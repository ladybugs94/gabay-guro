package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BadgeDetailsModel extends AndroidModel {

    @SerializedName("name")
    public String name;

    @SerializedName("image")
    public String image;

    @SerializedName("badge")
    public String badge;

    @SerializedName("progress")
    public float progress;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public BadgeDetailsModel convertFromJson(String json) {
        return convertFromJson(json, BadgeDetailsModel.class);
    }
}
