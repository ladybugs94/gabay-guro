package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class EngagementModel extends AndroidModel {

    @SerializedName("comment_id")
    public int commentId;
    @SerializedName("course_id")
    public int courseId;
    @SerializedName("code")
    public String code;
    @SerializedName("content")
    public String content;
    @SerializedName("date_created")
    public DateCreated dateCreated;
    @SerializedName("last_modified")
    public LastModified lastModified;
    @SerializedName("user")
    public User user;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public EngagementModel convertFromJson(String json) {
        return convertFromJson(json, EngagementModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModified {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class User {
        @SerializedName("data")
        public UserModel data;
    }
}
