package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UserModel extends AndroidModel {

    @SerializedName("user_id")
    public int userId;

    @SerializedName("name")
    public String name;

    @SerializedName("fb_id")
    public String fbID;

    @SerializedName("access_token")
    public String accessToken;

    @SerializedName("google_id")
    public String googleID;

    @SerializedName("username")
    public String username;

    @SerializedName("contact_number")
    public String contactNumber;

    @SerializedName("email")
    public String email;

    @SerializedName("date_created")
    public DateCreated dateCreated;

    @SerializedName("last_login")
    public LastLogin lastLogin;

    @SerializedName("avatar")
    public Avatar avatar;

    @SerializedName("contact_number_status")
    public String contactNumberStatus;

    @SerializedName("email_status")
    public String emailStatus;

    @SerializedName("bio")
    public String bio;

    @SerializedName("birthdate")
    public String birthdate;

    @SerializedName("age")
    public String age;

    @SerializedName("contact_number_network_provider")
    public String contactNumberNetworkProvider;

    @SerializedName("fname")
    public String fname;

    @SerializedName("lname")
    public String lname;

    @SerializedName("mname")
    public String mname;

    @SerializedName("suffix")
    public String suffix;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public UserModel convertFromJson(String json) {
        return convertFromJson(json, UserModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastLogin {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Avatar {
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String fullPath;

        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
