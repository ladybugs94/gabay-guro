package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BadgeModel extends AndroidModel {

    @SerializedName("badge_id")
    public int badgeId;
    @SerializedName("code")
    public String code;
    @SerializedName("name")
    public String name;
    @SerializedName("description")
    public String description;
    @SerializedName("reference_type")
    public String referenceType;
    @SerializedName("bronze_progress")
    public int bronzeProgress;
    @SerializedName("bronze_progress_percentage")
    public float bronzeProgressPercentage;
    @SerializedName("bronze_requirement")
    public int bronzeRequirement;
    @SerializedName("bronze_thumbnail")
    public BronzeThumbnail bronzeThumbnail;
    @SerializedName("silver_progress")
    public int silverProgress;
    @SerializedName("silver_progress_percentage")
    public float silverProgressPercentage;
    @SerializedName("silver_requirement")
    public int silverRequirement;
    @SerializedName("silver_thumbnail")
    public SilverThumbnail silverThumbnail;
    @SerializedName("gold_progress")
    public int goldProgress;
    @SerializedName("gold_progress_percentage")
    public float goldProgressPercentage;
    @SerializedName("gold_requirement")
    public int goldRequirement;
    @SerializedName("gold_thumbnail")
    public GoldThumbnail goldThumbnail;
    @SerializedName("platinum_progress")
    public int platinumProgress;
    @SerializedName("platinum_progress_percentage")
    public float platinumProgressPercentage;
    @SerializedName("platinum_requirement")
    public int platinumRequirement;
    @SerializedName("platinum_thumbnail")
    public PlatinumThumbnail platinumThumbnail;
    @SerializedName("isFinish")
    public boolean isFinish = false;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public BadgeModel convertFromJson(String json) {
        return convertFromJson(json, BadgeModel.class);
    }

    public static class BronzeThumbnail {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class SilverThumbnail {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class GoldThumbnail {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class PlatinumThumbnail {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
