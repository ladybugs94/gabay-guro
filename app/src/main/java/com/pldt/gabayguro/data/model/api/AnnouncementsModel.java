package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AnnouncementsModel extends AndroidModel {


    @SerializedName("announcement_id")
    public int announcementId;

    @SerializedName("title")
    public String title;

    @SerializedName("content")
    public String content;

    @SerializedName("web_url")
    public String webUrl;

    @SerializedName("date_created")
    public DateCreated dateCreated;

    @SerializedName("last_modified")
    public LastModified lastModified;

    @SerializedName("thumbnail")
    public Thumbnail thumbnail;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public AnnouncementsModel convertFromJson(String json) {
        return convertFromJson(json, AnnouncementsModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModified {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Thumbnail {
        @SerializedName("path")
        public String path;

        @SerializedName("filename")
        public String filename;

        @SerializedName("directory")
        public String directory;

        @SerializedName("full_path")
        public String fullPath;

        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
