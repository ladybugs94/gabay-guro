package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MaterialModel extends AndroidModel {

    @SerializedName("material_id")
    public int materialId;
    @SerializedName("code")
    public String code;
    @SerializedName("title")
    public String title;
    @SerializedName("date_created")
    public DateCreated dateCreated;
    @SerializedName("last_modified")
    public LastModified lastModified;
    @SerializedName("type")
    public String type;
    @SerializedName("file")
    public File file;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public MaterialModel convertFromJson(String json) {
        return convertFromJson(json, MaterialModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModified {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class File {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
