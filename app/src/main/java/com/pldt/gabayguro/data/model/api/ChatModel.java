package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Evanson on 2019-12-17.
 */

public class ChatModel extends AndroidModel {

    @SerializedName("channel")
    public String channel;
    @SerializedName("title")
    public String title;
    @SerializedName("type")
    public String type;
    @SerializedName("date_created")
    public DateCreated dateCreated;
    @SerializedName("last_message")
    public LastMessage lastMessage;
    @SerializedName("thumbnail")
    public Thumbnail thumbnail;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ChatModel convertFromJson(String json) {
        return convertFromJson(json, ChatModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }


    public static class Thumbnail {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
    public static class LastMessage {
        @SerializedName("data")
        public LatestMessageModel data;
    }
}
