package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class TransactionModel extends AndroidModel {

    @SerializedName("history_id")
    public int historyId;
    @SerializedName("code")
    public String code;
    @SerializedName("type")
    public String type;
    @SerializedName("remarks")
    public String remarks;
    @SerializedName("reference_id")
    public String referenceId;
    @SerializedName("reference_type")
    public String referenceType;
    @SerializedName("reference_type_display")
    public String referenceTypeDisplay;
    @SerializedName("amount_display")
    public String amountDisplay;
    @SerializedName("amount")
    public String amount;
    @SerializedName("date_created")
    public DateCreated dateCreated;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public TransactionModel convertFromJson(String json) {
        return convertFromJson(json, TransactionModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }
}
