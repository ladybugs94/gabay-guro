package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BindingModel extends AndroidModel {

    @SerializedName("facebook")
    public int facebook;

    @SerializedName("google")
    public int google;

    @SerializedName("office365")
    public int office365;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public BindingModel convertFromJson(String json) {
        return convertFromJson(json, BindingModel.class);
    }
}
