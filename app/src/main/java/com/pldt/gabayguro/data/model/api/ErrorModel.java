package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

import java.util.List;

/**
 * Created by Evanson on 2019-12-11.
 */

public class ErrorModel extends AndroidModel {


    @SerializedName("fname")
    public List<String> fname;

    @SerializedName("lname")
    public List<String> lname;

    @SerializedName("email")
    public List<String> email;

    @SerializedName("birthdate")
    public List<String> birthdate;

    @SerializedName("contact_number")
    public List<String> contactNumber;

    @SerializedName("password")
    public List<String> password;

    @SerializedName("currentPassword")
    public List<String> currentPassword;

    @SerializedName("password_confirmation")
    public List<String> passwordConfirmation;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ErrorModel convertFromJson(String json) {
        return convertFromJson(json, ErrorModel.class);
    }

}