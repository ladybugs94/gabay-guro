package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ResourcesModel extends AndroidModel {


    @SerializedName("product_id")
    public int productId;
    @SerializedName("category_code")
    public String categoryCode;
    @SerializedName("category_name")
    public String categoryName;
    @SerializedName("product_code")
    public String productCode;
    @SerializedName("product_name")
    public String productName;
    @SerializedName("price_display")
    public String priceDisplay;
    @SerializedName("price")
    public String price;
    @SerializedName("content")
    public String content;
    @SerializedName("date_created")
    public DateCreated dateCreated;
    @SerializedName("last_modified")
    public LastModified lastModified;
    @SerializedName("thumbnail")
    public Thumbnail thumbnail;
    @SerializedName("merchant")
    public Merchant merchant;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ResourcesModel convertFromJson(String json) {
        return convertFromJson(json, ResourcesModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModified {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Thumbnail {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class Merchant {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("merchant_id")
            public int merchantId;
            @SerializedName("code")
            public String code;
            @SerializedName("name")
            public String name;
            @SerializedName("date_created")
            public DateCreatedX dateCreated;
            @SerializedName("last_modified")
            public LastModifiedX lastModified;
            @SerializedName("thumbnail")
            public ThumbnailX thumbnail;

            public static class DateCreatedX {
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public String timestamp;
            }

            public static class LastModifiedX {
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public String timestamp;
            }

            public static class ThumbnailX {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }
}
