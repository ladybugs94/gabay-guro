package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class QuickLinkModel extends AndroidModel {

    @SerializedName("name")
    public String name;

    @SerializedName("img")
    public int img;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public QuickLinkModel convertFromJson(String json) {
        return convertFromJson(json, QuickLinkModel.class);
    }
}
