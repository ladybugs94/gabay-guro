package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ItemsModel extends AndroidModel {

    @SerializedName("item_id")
    public int itemId;
    @SerializedName("product_code")
    public String productCode;
    @SerializedName("product_name")
    public String productName;
    @SerializedName("price_display")
    public String priceDisplay;
    @SerializedName("price")
    public String price;
    @SerializedName("qty_display")
    public String qtyDisplay;
    @SerializedName("qty")
    public int qty;
    @SerializedName("subtotal_display")
    public String subtotalDisplay;
    @SerializedName("subtotal")
    public String subtotal;
    @SerializedName("product")
    public Product product;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ItemsModel convertFromJson(String json) {
        return convertFromJson(json, ItemsModel.class);
    }

    public static class Product {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("product_id")
            public int productId;
            @SerializedName("category_code")
            public String categoryCode;
            @SerializedName("category_name")
            public String categoryName;
            @SerializedName("product_code")
            public String productCode;
            @SerializedName("product_name")
            public String productName;
            @SerializedName("price_display")
            public String priceDisplay;
            @SerializedName("price")
            public String price;
            @SerializedName("content")
            public String content;
            @SerializedName("date_created")
            public DateCreated dateCreated;
            @SerializedName("last_modified")
            public LastModified lastModified;
            @SerializedName("thumbnail")
            public Thumbnail thumbnail;

            public static class DateCreated {
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public String timestamp;
            }

            public static class LastModified {
                @SerializedName("date_db")
                public String dateDb;
                @SerializedName("month_year")
                public String monthYear;
                @SerializedName("time_passed")
                public String timePassed;
                @SerializedName("timestamp")
                public String timestamp;
            }

            public static class Thumbnail {
                @SerializedName("path")
                public String path;
                @SerializedName("filename")
                public String filename;
                @SerializedName("directory")
                public String directory;
                @SerializedName("full_path")
                public String fullPath;
                @SerializedName("thumb_path")
                public String thumbPath;
            }
        }
    }
}
