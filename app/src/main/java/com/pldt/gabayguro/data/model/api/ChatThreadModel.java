package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

public class ChatThreadModel extends AndroidModel {


    @SerializedName("conversation_id")
    public int conversationId;

    @SerializedName("user_id")
    public int userId;

    @SerializedName("content")
    public String content;

    @SerializedName("type")
    public String type;

    @SerializedName("date_created")
    public DateCreated dateCreated;

    @SerializedName("attachment")
    public Attachment attachment;

    @SerializedName("sender")
    public Sender sender;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ChatThreadModel convertFromJson(String json) {
        return convertFromJson(json, ChatThreadModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Attachment {
        @SerializedName("filename")
        public String filename;
        @SerializedName("path")
        public String path;
        @SerializedName("directory")
        public String directory;
        @SerializedName("size")
        public String size;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class Sender {
        @SerializedName("data")
        public UserModel data;
    }
}


