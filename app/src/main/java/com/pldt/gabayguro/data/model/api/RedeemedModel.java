package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RedeemedModel extends AndroidModel {

    @SerializedName("transaction_id")
    public int transactionId;
    @SerializedName("code")
    public String code;
    @SerializedName("total_display")
    public String totalDisplay;
    @SerializedName("total")
    public String total;
    @SerializedName("total_qty_display")
    public String totalQtyDisplay;
    @SerializedName("total_qty")
    public int totalQty;
    @SerializedName("status")
    public String status;
    @SerializedName("status_display")
    public String statusDisplay;
    @SerializedName("payment_status")
    public String paymentStatus;
    @SerializedName("payment_status_display")
    public String paymentStatusDisplay;
    @SerializedName("date_created")
    public DateCreated dateCreated;
    @SerializedName("last_modified")
    public LastModified lastModified;
    @SerializedName("item")
    public Item item;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public RedeemedModel convertFromJson(String json) {
        return convertFromJson(json, RedeemedModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModified {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Item {
        @SerializedName("data")
        public List<ItemsModel> data;
    }
}
