package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ContributorModel extends AndroidModel {


    @SerializedName("contributor_id")
    public int contributorId;
    @SerializedName("name")
    public String name;
    @SerializedName("username")
    public String username;
    @SerializedName("email")
    public String email;
    @SerializedName("date_created")
    public DateCreated dateCreated;
    @SerializedName("last_login")
    public LastLogin lastLogin;
    @SerializedName("avatar")
    public Avatar avatar;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public ContributorModel convertFromJson(String json) {
        return convertFromJson(json, ContributorModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastLogin {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public Object timestamp;
    }

    public static class Avatar {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }
}
