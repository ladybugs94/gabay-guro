package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

import java.util.List;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class CoursesModel extends AndroidModel {


    @SerializedName("course_id")
    public int courseId;
    @SerializedName("code")
    public String code;
    @SerializedName("title")
    public String title;
    @SerializedName("overview")
    public String overview;
    @SerializedName("date_created")
    public DateCreated dateCreated;
    @SerializedName("last_modified")
    public LastModified lastModified;
    @SerializedName("thumbnail")
    public Thumbnail thumbnail;
    @SerializedName("material")
    public Material material;
    @SerializedName("contributor")
    public Contributor contributor;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public CoursesModel convertFromJson(String json) {
        return convertFromJson(json, CoursesModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class LastModified {
        @SerializedName("date_db")
        public String dateDb;
        @SerializedName("month_year")
        public String monthYear;
        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class Thumbnail {
        @SerializedName("path")
        public String path;
        @SerializedName("filename")
        public String filename;
        @SerializedName("directory")
        public String directory;
        @SerializedName("full_path")
        public String fullPath;
        @SerializedName("thumb_path")
        public String thumbPath;
    }

    public static class Material {
        @SerializedName("data")
        public List<MaterialModel> data;
    }

    public static class Contributor {
        @SerializedName("data")
        public ContributorModel data;
    }
}
