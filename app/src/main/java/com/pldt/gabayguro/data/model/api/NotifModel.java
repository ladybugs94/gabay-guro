package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NotifModel extends AndroidModel {

    @SerializedName("notification_id")
    public String notificationId;

    @SerializedName("event")
    public String event;

    @SerializedName("reference_id")
    public String referenceId;

    @SerializedName("type")
    public String type;

    @SerializedName("title")
    public String title;

    @SerializedName("content")
    public String content;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("is_read")
    public String isRead;

    @SerializedName("date_created")
    public DateCreated dateCreated;

    @SerializedName("read_at")
    public ReadAt readAt;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public NotifModel convertFromJson(String json) {
        return convertFromJson(json, NotifModel.class);
    }

    public static class DateCreated {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class ReadAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public String timestamp;
    }
}
