package com.pldt.gabayguro.data.model.api;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.vendor.android.base.AndroidModel;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MyBadgeModel extends AndroidModel {

    @SerializedName("user_badge_id")
    public int userBadgeId;

    @SerializedName("badge_id")
    public int badgeId;

    @SerializedName("current_badge")
    public String currentBadge;

    @SerializedName("acquired_bronze_status")
    public boolean acquiredBronzeStatus;

    @SerializedName("acquired_bronze_at")
    public AcquiredBronzeAt acquiredBronzeAt;

    @SerializedName("acquired_silver_status")
    public boolean acquiredSilverStatus;

    @SerializedName("acquired_silver_at")
    public AcquiredSilverAt acquiredSilverAt;

    @SerializedName("acquired_gold_status")
    public boolean acquiredGoldStatus;

    @SerializedName("acquired_gold_at")
    public AcquiredGoldAt acquiredGoldAt;

    @SerializedName("acquired_platinum_status")
    public boolean acquiredPlatinumStatus;

    @SerializedName("acquired_platinum_at")
    public AcquiredPlatinumAt acquiredPlatinumAt;

    @SerializedName("badge")
    public Badge badge;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public MyBadgeModel convertFromJson(String json) {
        return convertFromJson(json, MyBadgeModel.class);
    }

    public static class AcquiredBronzeAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;
        @SerializedName("timestamp")
        public String timestamp;
    }

    public static class AcquiredSilverAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public Object timestamp;
    }

    public static class AcquiredGoldAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public Object timestamp;
    }

    public static class AcquiredPlatinumAt {
        @SerializedName("date_db")
        public String dateDb;

        @SerializedName("month_year")
        public String monthYear;

        @SerializedName("time_passed")
        public String timePassed;

        @SerializedName("timestamp")
        public Object timestamp;
    }

    public static class Badge {
        @SerializedName("data")
        public BadgeModel data;
    }
}
