package com.pldt.gabayguro.vendor.android.java;

import android.Manifest;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.android.base.BaseActivity;

import java.util.Date;

public class DownloadFileManager {

    private Context context;
    private DownloadManager downloadManager;
    private BroadcastReceiver onComplete;
    private final int STORAGE_PERMISSION = 111;
    private long downloadReferenceId;
    private String urlFile;
    private DownloadCallback downloadCallback;
    private String fileName;

    public static DownloadFileManager newInstance(Context context, String fileName, DownloadCallback downloadCallback) {
        DownloadFileManager fragment = new DownloadFileManager();
        fragment.context = context;
        fragment.urlFile = "/";
        fragment.downloadCallback = downloadCallback;
        fragment.fileName = fileName;
        fragment.downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        return fragment;
    }

    public  void downloadWithPermissionGranted(String urlFile) {
        if(PermissionChecker.checkPermissions(context, Manifest.permission.WRITE_EXTERNAL_STORAGE, STORAGE_PERMISSION)){
            downloadFile(urlFile);
        }else{
            this.urlFile = urlFile;
        }
    }

    public Context getContext() {
        return context;
    }

    private void downloadFile(String urlFile){
        this.urlFile = urlFile;
        onComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (downloadReferenceId == -1)
                    return;

                Intent fileIntent = new Intent(Intent.ACTION_VIEW);
                Uri mostRecentDownload = downloadManager.getUriForDownloadedFile(downloadReferenceId);

                String mimeType = downloadManager.getMimeTypeForDownloadedFile(downloadReferenceId);
                fileIntent.setDataAndType(mostRecentDownload, mimeType);
                fileIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                defaultNotificationBuilder(fileIntent);
                if(downloadCallback != null){
                    downloadCallback.onFileDownloadCompleted(downloadReferenceId);
                }

                downloadReferenceId = -1;
            }
        };

        getContext().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        Uri downloadUri = Uri.parse(urlFile);
        String downloadName = fileName;
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("Downloading " + downloadName);
        request.setDescription("Downloading " + downloadName);
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/GabayGuro/"  + downloadName);
        request.addRequestHeader("Authorization", "Bearer" + UserData.getString(UserData.AUTHORIZATION));
        downloadReferenceId = downloadManager.enqueue(request);
    }

    private void defaultNotificationBuilder(Intent intent) {

        PendingIntent pendingIntent = PendingIntent.getActivity
                (getContext(), 1 /* Request code */, intent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getContext())
                .setSmallIcon(R.drawable.gabay_guro_logo_fav)
                .setColor(ActivityCompat.getColor(getContext(), R.color.colorPrimary))
                .setContentTitle(getContext().getString(R.string.app_name))
                .setContentText("Download completed. File is at Downloads/MentorMe/" + fileName)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify( (int) downloadReferenceId /* ID of notification */, notificationBuilder.build());
    }

    public static String generateRandomName(String fileType){
        return new Date().getTime() + fileType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public interface DownloadCallback{
        void onFileDownloadCompleted(long id);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == STORAGE_PERMISSION){
            if(((BaseActivity) getContext()) .isAllPermissionResultGranted(grantResults)){
                downloadFile(fileName);
            }
        }
    }
}
