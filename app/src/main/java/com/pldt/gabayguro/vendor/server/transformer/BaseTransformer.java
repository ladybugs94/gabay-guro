package com.pldt.gabayguro.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;
import com.pldt.gabayguro.data.model.api.ErrorModel;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class BaseTransformer {

    @SerializedName("msg")
    public String msg = "Internal Server Error";

    @SerializedName("status")
    public Boolean status = false;

    @SerializedName("status_code")
    public String status_code = "RETROFIT_FAILED";

    @SerializedName("token")
    public String token = "";

    @SerializedName("new_token")
    public String new_token = "";

    @SerializedName("has_morepages")
    public Boolean has_morepages = false;

    @SerializedName("errors")
    public ErrorModel errors;

    @SerializedName("wallet_display")
    public String walletDisplay;

    @SerializedName("wallet")
    public String wallet;


    @SerializedName("hasRequirements")
    public boolean hasRequirements(){
        return checkEmpty(errors);
    }

    public boolean checkEmpty(Object obj){
        return obj !=  null;
    }
}
