package com.pldt.gabayguro.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.config.Url;
import com.pldt.gabayguro.data.model.api.BindingModel;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.request.APIResponse;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Auth {

    public static Auth getDefault(){
        return new Auth();
    }


    public void login(Context context, String username, String password) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, username)
                .addParameter(Keys.PASSWORD, password)
                .showDefaultProgressDialog("Logging in...")
                .execute();
    }

    public void forgot (Context context, String username) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getForgotPass(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ForgotResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.EMAIL, username)
                .showDefaultProgressDialog("Submitting ...")
                .execute();
    }

    public void logout(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getLogout(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LogoutResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Logging out...")
                .execute();
    }

    public void checkLogin(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getCheckLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CheckResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();

    }

    public void refreshToken(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getRefreshToken(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RefreshTokenResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .execute();

    }


    public APIRequest register(Context context) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRegister(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RegisterResponse(this));
            }
        };

        apiRequest
                .showDefaultProgressDialog("Registering...");

        return apiRequest;
    }

    public APIRequest updateProfile(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getUpdateProfile(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateProfileResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Updating...");
        return apiRequest;
    }

    public void update_pass(Context context, String currentPass, String password, String passConfirm) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getUpdatePassword(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdatePassResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PASSWORD, password)
                .addParameter(Keys.CURRENT_PASSWORD, currentPass)
                .addParameter(Keys.PASSWORD_CONFIRM, passConfirm)
                .showDefaultProgressDialog("Updating password...")
                .execute();

    }

    public void update_avatar(Context context, File file) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getUpdateAvatar(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new UpdateAvatarResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.FILE, file)
                .showDefaultProgressDialog("Updating Avatar...")
                .execute();

    }

    public void update_phone(Context context, String phone, String otp) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getEditPhone(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new EditPhoneResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PHONE_NUMBER, phone)
                .addParameter(Keys.OTP_CODE, otp)
                .showDefaultProgressDialog("Updating...")
                .execute();
    }

    public void request_otp(Context context, String phone) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRequestOTP(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new OTPResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PHONE_NUMBER, phone)
                .showDefaultProgressDialog("Requesting...")
                .execute();
    }

    public void myProfile(Context context, SwipeRefreshLayout swipeRefreshLayout){
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getMyProfile(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyProfileResponse(this));
            }
        };
        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public void fbLogin(Context context, String accessToken, String fbID) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getFBLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new FBResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.ACCESS_TOKEN, accessToken)
                .addParameter(Keys.FB_ID, fbID)
                .addParameter(Keys.INCLUDE, "info")
                .showDefaultProgressDialog("Logging in...")
                .execute();
    }

    public void office365Login(Context context, String accessToken) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getOffice365Login(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new Office365Response(this));
            }
        };

        apiRequest
                .addParameter(Keys.ACCESS_TOKEN, accessToken)
                .addParameter(Keys.INCLUDE, "info")
                .showDefaultProgressDialog("Logging in...")
                .execute();
    }

    public void googleLogin(Context context, String accessToken, String googleID) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getGoogleLogin(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new GoogleResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.ACCESS_TOKEN, accessToken)
                .addParameter(Keys.GOOGLE_ID, googleID)
                .addParameter(Keys.INCLUDE, "info")
                .showDefaultProgressDialog("Logging in...")
                .execute();
    }

    public void connectFB(Context context, String accessToken) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getConnectFacebook(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new FBResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.ACCESS_TOKEN, accessToken)
                .showDefaultProgressDialog("Binding Account...")
                .execute();
    }

    public void connectOffice365(Context context, String accessToken) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getConnectOffice365(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new Office365Response(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.ACCESS_TOKEN, accessToken)
                .showDefaultProgressDialog("Binding Account...")
                .execute();
    }

    public void connectGoogle(Context context, String accessToken) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserModel>>(context) {
            @Override
            public Call<SingleTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getConnectGoogle(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new GoogleResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.ACCESS_TOKEN, accessToken)
                .showDefaultProgressDialog("Binding Account...")
                .execute();
    }

    public void bindAccount(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<BindingModel>>(context) {
            @Override
            public Call<SingleTransformer<BindingModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBindedTransformer(Url.getBindStatus(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new BindResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true)
                .execute();
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<BindingModel>> requestBindedTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }


    public class LoginResponse extends APIResponse<SingleTransformer<UserModel>> {
        public LoginResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class LogoutResponse extends APIResponse<BaseTransformer> {
        public LogoutResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ForgotResponse extends APIResponse<BaseTransformer> {
        public ForgotResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RegisterResponse extends APIResponse<BaseTransformer> {
        public RegisterResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UpdatePassResponse extends APIResponse<SingleTransformer<UserModel>> {
        public UpdatePassResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UpdateAvatarResponse extends APIResponse<SingleTransformer<UserModel>> {
        public UpdateAvatarResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class UpdateProfileResponse extends APIResponse<SingleTransformer<UserModel>> {
        public UpdateProfileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class MyProfileResponse extends APIResponse<SingleTransformer<UserModel>> {
        public MyProfileResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CheckResponse extends APIResponse<SingleTransformer<UserModel>> {
        public CheckResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RefreshTokenResponse extends APIResponse<SingleTransformer<UserModel>> {
        public RefreshTokenResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class OTPResponse extends APIResponse<BaseTransformer> {
        public OTPResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class EditPhoneResponse extends APIResponse<SingleTransformer<UserModel>> {
        public EditPhoneResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class FBResponse extends APIResponse<SingleTransformer<UserModel>> {
        public FBResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class Office365Response extends APIResponse<SingleTransformer<UserModel>> {
        public Office365Response(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class GoogleResponse extends APIResponse<SingleTransformer<UserModel>> {
        public GoogleResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class BindResponse extends APIResponse<SingleTransformer<BindingModel>> {
        public BindResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}
