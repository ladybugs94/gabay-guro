package com.pldt.gabayguro.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.config.Url;
import com.pldt.gabayguro.data.model.api.BadgeModel;
import com.pldt.gabayguro.data.model.api.MyBadgeModel;
import com.pldt.gabayguro.data.model.api.BadgeModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.request.APIResponse;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Badges {

    public static Badges getDefault(){
        return new Badges();
    }

    public APIRequest allBadges(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<BadgeModel>>(context) {
            @Override
            public Call<CollectionTransformer<BadgeModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getAllBadge(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllBadgeResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest myBadges(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<MyBadgeModel>>(context) {
            @Override
            public Call<CollectionTransformer<MyBadgeModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestMyBadgeTransformer(Url.getMyBadge(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyBadgesResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "badge")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public interface RequestService {

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<BadgeModel>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<MyBadgeModel>> requestMyBadgeTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class AllBadgeResponse extends APIResponse<CollectionTransformer<BadgeModel>> {
        public AllBadgeResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class MyBadgesResponse extends APIResponse<CollectionTransformer<MyBadgeModel>> {
        public MyBadgesResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}
