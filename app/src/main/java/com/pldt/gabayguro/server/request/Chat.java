package com.pldt.gabayguro.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.config.Url;
import com.pldt.gabayguro.data.model.api.ChatModel;
import com.pldt.gabayguro.data.model.api.ChatThreadModel;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.request.APIResponse;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;



public class Chat {

    public static Chat getDefault(){
        return new Chat();
    }


    public APIRequest myChats(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ChatModel>>(context) {
            @Override
            public Call<CollectionTransformer<ChatModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformerChat(Url.geyMyChats(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new MyChatResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "last_message.sender")
                .setPerPage(10)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }


    public void createChat(Context context, int id, String content, File file) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformerChatThread(Url.getCreateChat(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CreateMessageUserResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.USER_ID, id)
                .addParameter(Keys.CONTENT, content)
                .addParameter(Keys.FILE, file)
                .addParameter(Keys.INCLUDE, "sender")
                .execute();

    }


    public APIRequest createMessage(Context context,int chat_id, String content, File file) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<SingleTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformerChatThread(Url.getCreateMessage(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CreateMessageResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.CONTENT, content)
                .addParameter(Keys.CONVERSATION_ID, chat_id)
                .addParameter(Keys.FILE, file)
                .addParameter(Keys.INCLUDE, "sender")
                .execute();

        return apiRequest;
    }


    public APIRequest chatThread(Context context, int chat_id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ChatThreadModel>>(context) {
            @Override
            public Call<CollectionTransformer<ChatThreadModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformerChatThread(Url.getChatThread(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ChatThreadResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.CONVERSATION_ID,chat_id)
                .addParameter(Keys.INCLUDE, "sender")
                .setPerPage(20);

        return apiRequest;
    }

    public APIRequest removeMessage(Context context, int msg_id) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRemove(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RemoveMessageResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.MSG_ID, msg_id)
                .execute();

        return apiRequest;
    }

    public APIRequest removeMessageAll(Context context, int msg_id) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRemoveAll(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RemoveMessageResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.MSG_ID, msg_id)
                .execute();

        return apiRequest;
    }

    public APIRequest searchUser(Context context, SwipeRefreshLayout swipeRefreshLayout, String keyword) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<UserModel>>(context) {
            @Override
            public Call<CollectionTransformer<UserModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestUserTransformer(Url.getSearchUser(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SearchUserResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.KEYWORD, keyword)
                .setPerPage(10)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ChatModel>> requestSingleTransformerChat(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ChatThreadModel>> requestSingleTransformerChatThread(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ChatModel>> requestCollectionTransformerChat(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ChatThreadModel>> requestCollectionTransformerChatThread(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<UserModel>> requestUserTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);


        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class MyChatResponse extends APIResponse<CollectionTransformer<ChatModel>> {
        public MyChatResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CreateMessageResponse extends APIResponse<SingleTransformer<ChatThreadModel>> {
        public CreateMessageResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class CreateMessageUserResponse extends APIResponse<SingleTransformer<ChatThreadModel>> {
        public CreateMessageUserResponse(APIRequest apiRequest) { super(apiRequest); }
    }

    public class ChatThreadResponse extends APIResponse<CollectionTransformer<ChatThreadModel>> {
        public ChatThreadResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class SearchUserResponse extends APIResponse<CollectionTransformer<UserModel>> {
        public SearchUserResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RemoveMessageResponse extends APIResponse<BaseTransformer> {
        public RemoveMessageResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}
