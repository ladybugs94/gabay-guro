package com.pldt.gabayguro.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.config.Url;
import com.pldt.gabayguro.data.model.api.AnnouncementsModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.request.APIResponse;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Announcements {

    public static Announcements getDefault(){
        return new Announcements();
    }

    public APIRequest allAnnouncements(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<AnnouncementsModel>>(context) {
            @Override
            public Call<CollectionTransformer<AnnouncementsModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getAllAnnouncements(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllAnnouncementsResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public void announcements(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<AnnouncementsModel>>(context) {
            @Override
            public Call<SingleTransformer<AnnouncementsModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getAnnouncements(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AnnouncementsResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.ANNOUNCEMENT_ID, id).execute();
    }

    public interface RequestService {

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<AnnouncementsModel>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<AnnouncementsModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class AllAnnouncementsResponse extends APIResponse<CollectionTransformer<AnnouncementsModel>> {
        public AllAnnouncementsResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AnnouncementsResponse extends APIResponse<SingleTransformer<AnnouncementsModel>> {
        public AnnouncementsResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

}
