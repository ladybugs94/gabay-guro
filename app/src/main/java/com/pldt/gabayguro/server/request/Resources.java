package com.pldt.gabayguro.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.config.Url;
import com.pldt.gabayguro.data.model.api.CategoryModel;
import com.pldt.gabayguro.data.model.api.RedeemedModel;
import com.pldt.gabayguro.data.model.api.ResourcesModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.request.APIResponse;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Resources {

    public static Resources getDefault(){
        return new Resources();
    }

    public APIRequest allResources(Context context, SwipeRefreshLayout swipeRefreshLayout, String category) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<ResourcesModel>>(context) {
            @Override
            public Call<CollectionTransformer<ResourcesModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestResourcesTransformer(Url.getAllResources(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ResourcesResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.CATEGORY_CODE, category)
                .addParameter(Keys.INCLUDE, "merchant")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest allRedeemed(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<RedeemedModel>>(context) {
            @Override
            public Call<CollectionTransformer<RedeemedModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAllRedeemedTransformer(Url.getAllRedeem(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllRedeemResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "item.product")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest redeemedDetails(Context context, SwipeRefreshLayout swipeRefreshLayout, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<RedeemedModel>>(context) {
            @Override
            public Call<SingleTransformer<RedeemedModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRedeemedTransformer(Url.getAllRedeem(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RedeemedDetailsResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.TRANSACTION_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public void details(Context context, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<ResourcesModel>>(context) {
            @Override
            public Call<SingleTransformer<ResourcesModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getResources(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new DetailsResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PRODUCT_ID, id)
                .addParameter(Keys.INCLUDE, "merchant").execute();
    }

    public void redeem(Context context, int id) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestBaseTransformer(Url.getRedeem(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RedeemResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.PRODUCT_ID, id)
                .showDefaultProgressDialog("Redeeming...")
                .execute();
    }


    public APIRequest categories(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<CategoryModel>>(context) {
            @Override
            public Call<CollectionTransformer<CategoryModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCategoryTransformer(Url.getCategory(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CategoryResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public interface RequestService {

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<ResourcesModel>> requestResourcesTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<ResourcesModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<RedeemedModel>> requestRedeemedTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<RedeemedModel>> requestAllRedeemedTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<CategoryModel>> requestCategoryTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class ResourcesResponse extends APIResponse<CollectionTransformer<ResourcesModel>> {
        public ResourcesResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class DetailsResponse extends APIResponse<SingleTransformer<ResourcesModel>> {
        public DetailsResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RedeemResponse extends APIResponse<BaseTransformer> {
        public RedeemResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CategoryResponse extends APIResponse<CollectionTransformer<CategoryModel>> {
        public CategoryResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AllRedeemResponse extends APIResponse<CollectionTransformer<RedeemedModel>> {
        public AllRedeemResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
    public class RedeemedDetailsResponse extends APIResponse<SingleTransformer<RedeemedModel>> {
        public RedeemedDetailsResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }


}
