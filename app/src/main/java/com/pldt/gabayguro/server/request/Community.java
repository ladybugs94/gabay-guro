package com.pldt.gabayguro.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.config.Url;
import com.pldt.gabayguro.data.model.api.CoursesModel;
import com.pldt.gabayguro.data.model.api.EngagementModel;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.request.APIResponse;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Community {

    public static Community getDefault(){
        return new Community();
    }



    public APIRequest allCourses(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<CoursesModel>>(context) {
            @Override
            public Call<CollectionTransformer<CoursesModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCollectionTransformer(Url.getAllCourses(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllCoursesResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "material, contributor")
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest courseDetails(Context context, SwipeRefreshLayout swipeRefreshLayout, int id) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<CoursesModel>>(context) {
            @Override
            public Call<SingleTransformer<CoursesModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestSingleTransformer(Url.getCourseDetails(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CourseResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "material, contributor")
                .addParameter(Keys.COURSE_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public APIRequest allComment(Context context, SwipeRefreshLayout swipeRefreshLayout, int id) {
        APIRequest apiRequest = new APIRequest<CollectionTransformer<EngagementModel>>(context) {
            @Override
            public Call<CollectionTransformer<EngagementModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAllCommentTransformer(Url.getAllComment(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new AllCommentResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "user")
                .addParameter(Keys.COURSE_ID, id)
                .setSwipeRefreshLayout(swipeRefreshLayout)
                .showSwipeRefreshLayout(true);

        return apiRequest;
    }

    public void createComment(Context context, int id, String comment) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<EngagementModel>>(context) {
            @Override
            public Call<SingleTransformer<EngagementModel>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestCommentTransformer(Url.getCreateComment(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new CommentResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.COURSE_ID, id)
                .addParameter(Keys.COMMENT, comment)
                .showDefaultProgressDialog("Submitting...")
                .execute();
    }





    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<CoursesModel>> requestSingleTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<CoursesModel>> requestCollectionTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<EngagementModel>> requestCommentTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<EngagementModel>> requestAllCommentTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestBaseTransformer(@Path(value = "p", encoded = true) String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> part);

    }

    public class AllCoursesResponse extends APIResponse<CollectionTransformer<CoursesModel>> {
        public AllCoursesResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CourseResponse extends APIResponse<SingleTransformer<CoursesModel>> {
        public CourseResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class AllCommentResponse extends APIResponse<CollectionTransformer<EngagementModel>> {
        public AllCommentResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class CommentResponse extends APIResponse<SingleTransformer<EngagementModel>> {
        public CommentResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }


}
