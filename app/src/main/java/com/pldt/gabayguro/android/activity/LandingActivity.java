package com.pldt.gabayguro.android.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.fragment.landing.LoginFragment;
import com.pldt.gabayguro.android.fragment.landing.SplashFragment;
import com.pldt.gabayguro.android.fragment.register.SignUpFragment;
import com.pldt.gabayguro.android.route.RouteActivity;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LandingActivity extends RouteActivity implements FacebookCallback<LoginResult>,
        GraphRequest.GraphJSONObjectCallback, GoogleApiClient.OnConnectionFailedListener{
    public static final String TAG = LandingActivity.class.getName();

    private FBCallback fbCallback;
    private ProgressDialog progressDialog;
    private CallbackManager callbackManager;
    private GmailCallback gmailCallback;
    private GoogleApiClient googleApiClient;
    private static final int REQ_CODE = 123;

    @State String accessToken;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {
        initFacebook();
        google();
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.pldt.gabayguro", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {
        }

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            UserData.insert(UserData.REGID, newToken);
            Log.e("FCM", "Instance ID: " + newToken);

        });
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "login":
                openLoginFragment();
                break;
            default:
                openSplashFragment();
                break;
        }
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }
    public void openSplashFragment(){ switchFragment(SplashFragment.newInstance()); }

    public void attemptFBLogin(FBCallback fbCallback){
        this.fbCallback = fbCallback;
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    public void initFacebook(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.setIsDebugEnabled(true);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    public String getAccessToken(){
        if(accessToken == null){
            return "";
        }
        return accessToken;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        accessToken = AccessToken.getCurrentAccessToken().getToken();
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), this);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "email, gender");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode==REQ_CODE)){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else{
            if(callbackManager!=null){
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onCancel() {
        if(progressDialog != null){
            progressDialog.cancel();
        }
        if(fbCallback != null){
            fbCallback.failed();
        }
    }

    @Override
    public void onError(FacebookException error) {
        if(progressDialog != null){
            progressDialog.cancel();
        }
        if(fbCallback != null){
            fbCallback.failed();
        }
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        Profile profile = Profile.getCurrentProfile();
        UserModel userItem = new UserModel();

        if ( profile != null ) {
            userItem.fbID = profile.getId();
            userItem.name = profile.getName();
            if(object != null){
                try {
                    userItem.email = object.getString("email");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if(fbCallback != null){
                fbCallback.success(userItem);
            }
            Log.e("Login", ">>>FB Id: " + userItem.fbID);
            Log.e("Login", ">>>email: " + userItem.email);
            Log.e("Login", ">>>name: " + userItem.name);
        }

        if(progressDialog != null){
            progressDialog.cancel();
        }
    }

    private void google(){
        String serverClientID = "857022343794-mrleddfvprm5hji1lhmm5feq3c5p9p9u.apps.googleusercontent.com";
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientID)
                .requestEmail().build();

        googleApiClient =new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();
    }

    private void sign(){
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent,REQ_CODE);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            String token = account.getIdToken();
            String  id = account.getId();
            String name = account.getDisplayName();
            String email = account.getEmail();

            UserModel userItem = new UserModel();
            userItem.accessToken = token;
            userItem.googleID = id;
            userItem.name = name;
            userItem.email = email;

            if(gmailCallback != null){
                gmailCallback.successGmail(userItem);
            }

            Log.e("LoginGM", ">>>GM token: " + userItem.accessToken);
            Log.e("LoginGM", ">>>GM id: " + userItem.googleID);
            Log.e("LoginGM", ">>>email: " + userItem.name);
            Log.e("LoginGM", ">>>name: " + userItem.email);

        } catch (ApiException e) {

            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());

        }
    }

    public void attemptGMLogin(GmailCallback gmailCallback){
        this.gmailCallback = gmailCallback;
        sign();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("FAILED", ">>>Failed: " + connectionResult);
    }

    public interface FBCallback{
        void success(UserModel userModel);
        void failed();
    }

    public interface GmailCallback{
        void successGmail(UserModel userModel);
    }
}
