package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.CategoryModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class CategoryRecyclerViewAdapter extends BaseRecylerViewAdapter<CategoryRecyclerViewAdapter.ViewHolder, CategoryModel> {

    private ClickListener clickListener;

    public CategoryRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_category));
    }

    public String getSelectedItems(){
        String total = "";
        for (CategoryModel specialtyModel : getData()){
            if (specialtyModel.isSelected){
                total = total + specialtyModel.code;

            }
        }

        return total;
    }

    public void setSelected(int id){
        for(CategoryModel item : getData()){
            item.isSelected = item.categoryId == id;
        }
        notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.categoryTXT.setText(holder.getItem().name);
        holder.categoryTXT.setSelected(holder.getItem().isSelected);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.categoryTXT)         TextView categoryTXT;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(CategoryRecyclerViewAdapter.this);
        }
        public CategoryModel getItem() {
            return (CategoryModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
                case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onCategoryClicked((CategoryModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onCategoryClicked(CategoryModel categoryModel);
    }
} 
