package com.pldt.gabayguro.android.activity;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.fragment.DefaultFragment;
import com.pldt.gabayguro.android.fragment.register.ForgotPasswordFragment;
import com.pldt.gabayguro.android.fragment.register.SignUpFragment;
import com.pldt.gabayguro.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RegisterActivity extends RouteActivity {
    public static final String TAG = RegisterActivity.class.getName();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_register;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "signup":
                openSignUpFragment();
                break;
            case "forgot":
                openForgotPassword();
                break;
            default:
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openSignUpFragment(){ switchFragment(SignUpFragment.newInstance()); }
    public void openForgotPassword(){ switchFragment(ForgotPasswordFragment.newInstance()); }



}
