package com.pldt.gabayguro.android.fragment.community;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.CommunityActivity;
import com.pldt.gabayguro.android.adapter.DefaultRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.EngagementRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.MaterialsRecyclerViewAdapter;
import com.pldt.gabayguro.android.dialog.ViewImageDialog;
import com.pldt.gabayguro.data.model.api.CoursesModel;
import com.pldt.gabayguro.data.model.api.EngagementModel;
import com.pldt.gabayguro.data.model.api.MaterialModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.server.request.Community;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class CourseDetailsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, EngagementRecyclerViewAdapter.ClickListener, MaterialsRecyclerViewAdapter.ClickListener {
    public static final String TAG = CourseDetailsFragment.class.getName();

    public static CourseDetailsFragment newInstance(int id) {
        CourseDetailsFragment fragment = new CourseDetailsFragment();
        fragment.courseID = id;
        return fragment;
    }

    private CommunityActivity communityActivity;
    private MaterialsRecyclerViewAdapter materialsRecyclerViewAdapter;
    private EngagementRecyclerViewAdapter engagementRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private APIRequest apiRequest;

    @BindView(R.id.nameTXT)             TextView nameTXT;
    @BindView(R.id.descTXT)             TextView descTXT;
    @BindView(R.id.placeholder1)        TextView placeholder1;
    @BindView(R.id.placeholder2)        TextView placeholder2;
    @BindView(R.id.profileIMG)          ImageView profileIMG;
    @BindView(R.id.profileIV)          ImageView profileIV;
    @BindView(R.id.materialRV)          RecyclerView materialRV;
    @BindView(R.id.engagementRV)        RecyclerView engagementRV;
    @BindView(R.id.coursesSRL)          SwipeRefreshLayout coursesSRL;

    @State int courseID;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_course_details;
    }

    @Override
    public void onViewReady() {
        communityActivity = (CommunityActivity) getContext();
        communityActivity.setTitle("");
        coursesSRL.setOnRefreshListener(this);
        setUpListView();
        setUpEngagements();
        if (!UserData.getUserModel().avatar.fullPath.isEmpty()){
            Picasso.with(getContext()).load(UserData.getUserModel().avatar.fullPath).fit().centerCrop().into(profileIV);
        }
    }

    private void setUpListView(){
        materialsRecyclerViewAdapter = new MaterialsRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        materialRV.setLayoutManager(linearLayoutManager);
        materialRV.setAdapter(materialsRecyclerViewAdapter);
        materialsRecyclerViewAdapter.setClickListener(this);
    }

    private void setUpEngagements(){
        engagementRecyclerViewAdapter = new EngagementRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        engagementRV.setLayoutManager(linearLayoutManager);
        engagementRV.setAdapter(engagementRecyclerViewAdapter);
        engagementRecyclerViewAdapter.setClickListener(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Subscribe
    public void onResponse(Community.CourseResponse response){
        SingleTransformer<CoursesModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status){
            nameTXT.setText(singleTransformer.data.title);
            descTXT.setText(singleTransformer.data.overview);
            Picasso.with(getContext()).load(singleTransformer.data.thumbnail.fullPath).fit().centerCrop().into(profileIMG);
            materialsRecyclerViewAdapter.setNewData(singleTransformer.data.material.data);
            if (singleTransformer.data.material.data.size() != 0) {
                placeholder1.setVisibility(View.GONE);
                materialRV.setVisibility(View.VISIBLE);
            }else{
                placeholder1.setVisibility(View.VISIBLE);
                materialRV.setVisibility(View.GONE);
            }
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Community.AllCommentResponse response){
        CollectionTransformer<EngagementModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status){
            if (response.isNext()){
                engagementRecyclerViewAdapter.addNewData(transformer.data);
            }else{
                engagementRecyclerViewAdapter.setNewData(transformer.data);
            }
            if (transformer.data.size() != 0) {
                placeholder2.setVisibility(View.GONE);
                engagementRV.setVisibility(View.VISIBLE);
            }else{
                placeholder2.setVisibility(View.VISIBLE);
                engagementRV.setVisibility(View.GONE);
            }
        }else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public void refreshList(){
        apiRequest = Community.getDefault().courseDetails(getContext(), coursesSRL, courseID);
        apiRequest.first();

        apiRequest = Community.getDefault().allComment(getContext(), coursesSRL, courseID);
        apiRequest.setPerPage(3);
        apiRequest.first();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @OnClick(R.id.commentET)
    void onComment(){
        communityActivity.openAllCommentsFragment(courseID);
    }

    @OnClick(R.id.viewAllBTN)
    void onViewComment(){
        communityActivity.openAllCommentsFragment(courseID);
    }

    @Override
    public void onItemClick(EngagementModel engagementModel) {

    }

    @Override
    public void onAvatarClick(EngagementModel engagementModel) {
        if (engagementModel.user.data.userId != UserData.getUserModel().userId) {
            communityActivity.startMessageActivity("conversation", 0, engagementModel.user.data.userId, engagementModel.user.data.avatar.fullPath, engagementModel.user.data.name);
        }
    }

    public void openfile(String url){
        Uri uri = Uri.parse(url);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (url.contains(".doc") || url.contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if(url.contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if(url.contains(".ppt") || url.contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if(url.contains(".xls") || url.contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if(url.contains(".zip") || url.contains(".rar")) {
            // Zip Rar file
            intent.setDataAndType(uri, "application/zip");
        } else if(url.contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if(url.contains(".wav") || url.contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if(url.contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if(url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
            // JPG file
//            intent.setDataAndType(uri, "image/jpeg");
            ViewImageDialog.newInstance(url, url).show(getChildFragmentManager(), TAG);
        } else if(url.contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if(url.contains(".3gp") || url.contains(".mpg") || url.contains(".mpeg") || url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            intent.setDataAndType(uri, "*/*");
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    @Override
    public void onItemClick(MaterialModel coursesModel) {
        openfile(coursesModel.file.fullPath);
    }
}
