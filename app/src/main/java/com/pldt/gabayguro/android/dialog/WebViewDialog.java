package com.pldt.gabayguro.android.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.vendor.android.base.BaseDialog;
import com.pldt.gabayguro.vendor.android.java.Formatter;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;

import butterknife.BindView;
import icepick.State;

public class WebViewDialog extends BaseDialog implements View.OnClickListener,PopupMenu.OnMenuItemClickListener{
	public static final String TAG = WebViewDialog.class.getName();

	private  Callback callback;

	@State	String url;
	@State String title;
	@State String newURL;

	@BindView(R.id.webView)			WebView webView;
//	@BindView(R.id.toolbar) 		Toolbar toolbar;
	@BindView(R.id.titleBarTxt) 	TextView titleBarTxt;
	@BindView(R.id.otherOptionBTN) 	View otherOptionBTN;
	@BindView(R.id.progressBar) 	ProgressBar progressBar;
	@BindView(R.id.titleBarURLTxt) 	TextView titleBarURLTxt;
	@BindView(R.id.closeBTN) 		View closeBTN;

	public static WebViewDialog newInstance(String title, String url) {
		WebViewDialog webViewDialog = new WebViewDialog();
		webViewDialog.title = title;
		webViewDialog.url = url;
		return webViewDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_web_view;
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onViewReady() {

        closeBTN.setOnClickListener(this);
		otherOptionBTN.setOnClickListener(this);

		if(title == null){
			title = getString(R.string.app_name);
		}
//		toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
//		toolbar.setNavigationOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if (webView.canGoBack()) {
//					webView.goBack();
//				} else {
//					dismiss();
//				}
//			}
//		});
		progressBar.setMax(100);
		progressBar.setProgress(0);

		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setSupportZoom(true);
		webView.loadUrl(url);
		webView.setWebChromeClient(new WebChromeClient(){
			@Override
			public void onProgressChanged(WebView view, int newProgress) {

				if(progressBar != null){
					if(newProgress < 100){
						progressBar.setVisibility(View.VISIBLE);
						otherOptionBTN.setVisibility(View.GONE);
						progressBar.setProgress(newProgress);
					}else{
						progressBar.setVisibility(View.GONE);
						otherOptionBTN.setVisibility(View.VISIBLE);
						titleBarTxt.setText(view.getTitle());
						titleBarURLTxt.setText(Formatter.getDomainFromURL(view.getOriginalUrl()));
						newURL = view.getOriginalUrl();
					}
				}
				super.onProgressChanged(view, newProgress);
			}
		});
		webView.setWebViewClient(new WebViewClient() {

//            @SuppressWarnings("deprecation")
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView webView, String url){
//                return shouldOverrideUrlLoading(url);
//            }
//
//            @TargetApi(Build.VERSION_CODES.N)
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest request){
//                Uri uri = request.getUrl();
//                return shouldOverrideUrlLoading(uri.toString());
//            }
//
////            private boolean shouldOverrideUrlLoading(final String url){
////                return true;
////            }

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				Log.e("WebViewClient", "Url->" + url);
				view.loadUrl(url);
				newURL = url;
				return true;
			}

		});
        Log.e("WebViewClient", "Url->" + url);

		getDialog().setOnCancelListener(new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (webView.canGoBack()) {
					webView.goBack();
				} else {
					dialog.dismiss();
				}
			}
		});

		getDialog().setOnKeyListener(new Dialog.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface arg0, int keyCode,
								 KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					if (webView.canGoBack()) {
						webView.goBack();
					} else {
						getDialog().dismiss();
					}
				}
				return true;
			}
		});


	}

	private void otherOption(View v){
		PopupMenu popup = new PopupMenu(getActivity(), v);
		popup.getMenuInflater().inflate(R.menu.webview_other_option, popup.getMenu());
		popup.setOnMenuItemClickListener(this);
		popup.show();
	}

	public void copyToClipBoard(String label, String value){
		ClipboardManager clipboardManager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clipData = ClipData.newPlainText(label, value);
		clipboardManager.setPrimaryClip(clipData);
		ToastMessage.show(getActivity(), label + " Copied to clipboard.", ToastMessage.Status.SUCCESS);
	}

	private void openBrowser(String link){
		Intent webIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse(link));
		try {
			startActivity(webIntent);
		} catch (ActivityNotFoundException ex) {
//			startActivity(webIntent);
		}
	}


	@Override
	public void onDismiss(DialogInterface dialog) {
		if(webView != null){
			webView.stopLoading();
		}
		super.onDismiss(dialog);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.closeBTN:
				dismiss();
				break;
			case R.id.otherOptionBTN:
				otherOption(v);
				break;
		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()){
			case R.id.copyBTN:
			    if (callback != null){
                    copyToClipBoard("URL", newURL);
                    dismiss();
                }

				break;
			case R.id.openBTN:
				openBrowser(newURL);
				break;
		}
		return false;
	}

	public  interface  Callback{
	    void onSuccess(String url);
    }

}
