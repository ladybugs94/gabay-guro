package com.pldt.gabayguro.android.fragment.profile;

import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.android.adapter.BadgeDetailsRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.BadgeDetailsModel;
import com.pldt.gabayguro.data.model.api.BadgeModel;
import com.pldt.gabayguro.data.model.api.MyBadgeModel;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BadgeDetailsFragment extends BaseFragment {
    public static final String TAG = BadgeDetailsFragment.class.getName();

    private ProfileActivity profileActivity;
    private BadgeDetailsRecyclerViewAdapter badgeDetailsRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.defaultRV)       RecyclerView defaultRV;
    @BindView(R.id.descTXT)         TextView descTXT;

    public static BadgeDetailsFragment newInstance(BadgeModel badgeModel) {
        BadgeDetailsFragment fragment = new BadgeDetailsFragment();
        fragment.badgeModel = badgeModel;
        return fragment;
    }

    private BadgeModel badgeModel;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_badge_details;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        setUpListView();
        profileActivity.setTitle(badgeModel.name);
        descTXT.setText(badgeModel.description);
    }

    private void setUpListView(){
        badgeDetailsRecyclerViewAdapter = new BadgeDetailsRecyclerViewAdapter(getContext());
        badgeDetailsRecyclerViewAdapter.setNewData(getDefaultData());
        linearLayoutManager = new LinearLayoutManager(getContext());
        defaultRV.setLayoutManager(linearLayoutManager);
        defaultRV.setAdapter(badgeDetailsRecyclerViewAdapter);
    }

    private List<BadgeDetailsModel> getDefaultData(){
        List<BadgeDetailsModel> androidModels = new ArrayList<>();
        BadgeDetailsModel defaultItem;

        if (badgeModel.platinumRequirement>0){
            defaultItem = new BadgeDetailsModel();
            defaultItem.id = 1;
            defaultItem.name = "Requirements: " + badgeModel.platinumRequirement;
            defaultItem.badge = "Platinum";
            defaultItem.image = badgeModel.platinumThumbnail.fullPath;
            defaultItem.progress = badgeModel.platinumProgressPercentage;
            androidModels.add(defaultItem);

        }

        if (badgeModel.goldRequirement>0){
            defaultItem = new BadgeDetailsModel();
            defaultItem.id = 2;
            defaultItem.name = "Requirements: " + badgeModel.goldRequirement;
            defaultItem.badge = "Gold";
            defaultItem.image = badgeModel.goldThumbnail.fullPath;
            defaultItem.progress = badgeModel.goldProgressPercentage;
            androidModels.add(defaultItem);
        }
        if (badgeModel.silverRequirement>0){
            defaultItem = new BadgeDetailsModel();
            defaultItem.id = 3;
            defaultItem.name = "Requirements: " + badgeModel.silverRequirement;
            defaultItem.badge = "Silver";
            defaultItem.image = badgeModel.silverThumbnail.fullPath;
            defaultItem.progress = badgeModel.silverProgressPercentage;
            androidModels.add(defaultItem);
        }
        if (badgeModel.bronzeRequirement>0){
            defaultItem = new BadgeDetailsModel();
            defaultItem.id = 4;
            defaultItem.name = "Requirements: " + badgeModel.bronzeRequirement;
            defaultItem.badge = "Bronze";
            defaultItem.image = badgeModel.bronzeThumbnail.fullPath;
            defaultItem.progress = badgeModel.bronzeProgressPercentage;
            androidModels.add(defaultItem);
        }

        return androidModels;
    }
}
