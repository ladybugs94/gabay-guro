package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.EngagementModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class EngagementRecyclerViewAdapter extends BaseRecylerViewAdapter<EngagementRecyclerViewAdapter.ViewHolder, EngagementModel>{

    private ClickListener clickListener;

    public EngagementRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_engagements));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
        holder.profileIV.setTag(holder.getItem());
        holder.profileIV.setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().user.data.name + " • ");
        holder.descTXT.setText(holder.getItem().content);
        holder.dateTXT.setText(holder.getItem().dateCreated.timePassed);
        if (!holder.getItem().user.data.avatar.fullPath.isEmpty()){
            Picasso.with(getContext()).load(holder.getItem().user.data.avatar.fullPath).fit().centerCrop().error(R.drawable.gabay_guro_logo_fav).placeholder(R.drawable.gabay_guro_logo_fav).into(holder.profileIV);
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.descTXT)     TextView descTXT;
        @BindView(R.id.dateTXT)     TextView dateTXT;
        @BindView(R.id.adapterCON)  View adapterCON;
        @BindView(R.id.profileIV)   ImageView profileIV;

        public ViewHolder(View view) {
            super(view);
        }

        public EngagementModel getItem() {
            return (EngagementModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((EngagementModel) v.getTag());
                }
                break;
            case R.id.profileIV:
                if (clickListener != null){
                    clickListener.onAvatarClick((EngagementModel) v.getTag());
                }
                break;
        }
    }

    public interface ClickListener{
        void onItemClick(EngagementModel engagementModel);
        void onAvatarClick(EngagementModel engagementModel);
    }
} 
