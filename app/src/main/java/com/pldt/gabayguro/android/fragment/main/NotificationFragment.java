package com.pldt.gabayguro.android.fragment.main;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.MainActivity;
import com.pldt.gabayguro.android.adapter.NotifRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.NotifModel;
import com.pldt.gabayguro.server.request.Chat;
import com.pldt.gabayguro.server.request.Notification;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class NotificationFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener
        , NotifRecyclerViewAdapter.ClickListener
        , EndlessRecyclerViewScrollListener.Callback {
    public static final String TAG = NotificationFragment.class.getName();

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }

    private MainActivity mainActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private NotifRecyclerViewAdapter messageRecyclerViewAdapter;
    private APIRequest apiRequest;

    @BindView(R.id.chatRV)                      RecyclerView chatRV;
    @BindView(R.id.chatSRL)                     SwipeRefreshLayout chatSRL;
    @BindView(R.id.shimmerFL)                   ShimmerFrameLayout shimmerFL;
    @BindView(R.id.noMessageTXT)                TextView noMessageTXT;
    @BindView(R.id.nestedSV)                    NestedScrollView nestedSV;
    @BindView(R.id.progressBar2)                ProgressBar progressBar2;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_notification;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        mainActivity.setTitle("Notifications");
        setUpRView();
        chatSRL.setOnRefreshListener(this);
    }

    private void setUpRView(){
        messageRecyclerViewAdapter = new NotifRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        chatRV.setLayoutManager(linearLayoutManager);
        chatRV.setAdapter(messageRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        chatRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        messageRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Notification.AllNotifResponse colletionResponse) {
        CollectionTransformer<NotifModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            shimmerFL.setVisibility(View.GONE);
            nestedSV.setVisibility(View.VISIBLE);
            if (colletionResponse.isNext()){
                messageRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                endlessRecyclerViewScrollListener.reset();
                messageRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }

            progressBar2.setVisibility(View.GONE);
            if (messageRecyclerViewAdapter.getItemCount() == 0){
                noMessageTXT.setVisibility(View.VISIBLE);
            } else {
                noMessageTXT.setVisibility(View.GONE);
            }
//            groupChatLL.setVisibility(messageRecyclerViewAdapter.getData().size() > 0 ? View.GONE : View.VISIBLE);
        }

    }



    public void refreshList(){
        apiRequest = Notification.getDefault().allNotif(getContext(), chatSRL);
        apiRequest.first();

    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(NotifModel sampleModel) {

    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        progressBar2.setVisibility(View.VISIBLE);
        apiRequest.setPage(page+1);
        apiRequest.nextPage();
    }
}
