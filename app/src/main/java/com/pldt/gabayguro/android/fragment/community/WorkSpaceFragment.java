package com.pldt.gabayguro.android.fragment.community;

import android.util.Log;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.CommunityActivity;
import com.pldt.gabayguro.android.dialog.WebViewDialog;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class WorkSpaceFragment extends BaseFragment {
    public static final String TAG = WorkSpaceFragment.class.getName();

    public static WorkSpaceFragment newInstance() {
        WorkSpaceFragment fragment = new WorkSpaceFragment();
        return fragment;
    }

    private CommunityActivity communityActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_workspace;
    }

    @Override
    public void onViewReady() {
        communityActivity = (CommunityActivity) getContext();
        communityActivity.setTitle("Workspace");
    }

    @OnClick(R.id.depedBTN)
    void depedBTN(){
        WebViewDialog.newInstance("", "https://commons.deped.gov.ph/").show(getChildFragmentManager(), TAG);
    }

    @OnClick(R.id.lrBTN)
    void lrBTN(){
        WebViewDialog.newInstance("", "https://lrmds.deped.gov.ph/").show(getChildFragmentManager(), TAG);
    }

    @OnClick(R.id.frontLearnersBTN)
    void frontlearnersBTN(){
        WebViewDialog.newInstance("", "https://frontlearners.com/blended/index.php").show(getChildFragmentManager(), TAG);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
