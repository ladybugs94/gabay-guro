package com.pldt.gabayguro.android.fragment.community;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.CommunityActivity;
import com.pldt.gabayguro.android.adapter.EngagementRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.MaterialsRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.CoursesModel;
import com.pldt.gabayguro.data.model.api.EngagementModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Community;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AllCommentsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, EndlessRecyclerViewScrollListener.Callback, EngagementRecyclerViewAdapter.ClickListener {
    public static final String TAG = AllCommentsFragment.class.getName();

    public static AllCommentsFragment newInstance(int id) {
        AllCommentsFragment fragment = new AllCommentsFragment();
        fragment.courseID = id;
        return fragment;
    }

    private CommunityActivity communityActivity;
    private EngagementRecyclerViewAdapter engagementRecyclerViewAdapter;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private APIRequest apiRequest;

    @BindView(R.id.placeholder2)        TextView placeholder2;
    @BindView(R.id.commentET)           EditText commentET;
    @BindView(R.id.engagementRV)        RecyclerView engagementRV;
    @BindView(R.id.coursesSRL)          SwipeRefreshLayout coursesSRL;
    @BindView(R.id.shimmerFL)           ShimmerFrameLayout shimmerFL;

    @State int courseID;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_all_comments;
    }

    @Override
    public void onViewReady() {
        communityActivity = (CommunityActivity) getContext();
        communityActivity.setTitle("Engagements");
        coursesSRL.setOnRefreshListener(this);
        setUpEngagements();
    }

    private void setUpEngagements(){
        engagementRecyclerViewAdapter = new EngagementRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        engagementRV.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        engagementRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        engagementRV.setAdapter(engagementRecyclerViewAdapter);
        engagementRecyclerViewAdapter.setClickListener(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Subscribe
    public void onResponse(Community.AllCommentResponse response){
        CollectionTransformer<EngagementModel> transformer = response.getData(CollectionTransformer.class);
        if (transformer.status){
            shimmerFL.setVisibility(View.GONE);
            if (response.isNext()){
                engagementRecyclerViewAdapter.addNewData(transformer.data);
            }else{
                engagementRecyclerViewAdapter.setNewData(transformer.data);
                endlessRecyclerViewScrollListener.reset();
            }
            if (transformer.data.size() != 0) {
                placeholder2.setVisibility(View.GONE);
                engagementRV.setVisibility(View.VISIBLE);
            }else{
                placeholder2.setVisibility(View.VISIBLE);
                engagementRV.setVisibility(View.GONE);
            }
        }else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Community.CommentResponse response){
        SingleTransformer<EngagementModel> transformer = response.getData(SingleTransformer.class);
        if (transformer.status){
           refreshList();
           commentET.setText("");
        }else{
            ToastMessage.show(getContext(), transformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public void refreshList(){
        apiRequest = Community.getDefault().allComment(getContext(), coursesSRL, courseID);
        apiRequest.setPerPage(20);
        apiRequest.first();
    }

    @OnClick(R.id.sendBTN)
    void onSendBTN(){
        Community.getDefault().createComment(getContext(), courseID, commentET.getText().toString().trim());
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(EngagementModel engagementModel) {

    }

    @Override
    public void onAvatarClick(EngagementModel engagementModel) {
        if (engagementModel.user.data.userId != UserData.getUserModel().userId) {
            communityActivity.startMessageActivity("conversation", 0, engagementModel.user.data.userId, engagementModel.user.data.avatar.fullPath, engagementModel.user.data.name);
        }

    }
}
