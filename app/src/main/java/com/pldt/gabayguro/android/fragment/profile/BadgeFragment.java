package com.pldt.gabayguro.android.fragment.profile;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.android.adapter.BadgeRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.BadgeModel;
import com.pldt.gabayguro.data.model.api.BadgeModel;
import com.pldt.gabayguro.server.request.Badges;
import com.pldt.gabayguro.server.request.Notification;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BadgeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener
        , BadgeRecyclerViewAdapter.ClickListener
        , EndlessRecyclerViewScrollListener.Callback {
    public static final String TAG = BadgeFragment.class.getName();

    public static BadgeFragment newInstance() {
        BadgeFragment fragment = new BadgeFragment();
        return fragment;
    }

    private ProfileActivity profileActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private BadgeRecyclerViewAdapter badgeRecyclerViewAdapter;
    private APIRequest apiRequest;

    @BindView(R.id.chatRV)                      RecyclerView chatRV;
    @BindView(R.id.chatSRL)                     SwipeRefreshLayout chatSRL;
    @BindView(R.id.shimmerFL)                   ShimmerFrameLayout shimmerFL;
    @BindView(R.id.noMessageTXT)                TextView noMessageTXT;
    @BindView(R.id.nestedSV)                    NestedScrollView nestedSV;
    @BindView(R.id.progressBar2)                ProgressBar progressBar2;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_notification;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        profileActivity.setTitle("All Badges");
        setUpRView();
        chatSRL.setOnRefreshListener(this);
    }

    private void setUpRView(){
        badgeRecyclerViewAdapter = new BadgeRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        chatRV.setLayoutManager(linearLayoutManager);
        chatRV.setAdapter(badgeRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        chatRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        badgeRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Badges.AllBadgeResponse colletionResponse) {
        CollectionTransformer<BadgeModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            if (colletionResponse.isNext()){
                badgeRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                badgeRecyclerViewAdapter.setNewData(collectionTransformer.data);
                endlessRecyclerViewScrollListener.reset();
            }
            shimmerFL.setVisibility(View.GONE);
            nestedSV.setVisibility(View.VISIBLE);
            progressBar2.setVisibility(View.GONE);
            if (badgeRecyclerViewAdapter.getItemCount() == 0){
                noMessageTXT.setVisibility(View.VISIBLE);
            } else {
                noMessageTXT.setVisibility(View.GONE);
            }
        }

    }



    public void refreshList(){
        apiRequest = Badges.getDefault().allBadges(getContext(), chatSRL);
        apiRequest.setPerPage(10);
        apiRequest.first();

    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        progressBar2.setVisibility(View.VISIBLE);
        apiRequest.setPage(page+1);
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(BadgeModel sampleModel) {
        profileActivity.openBadgeDetailsFragment(sampleModel);
    }
}
