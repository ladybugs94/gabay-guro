package com.pldt.gabayguro.android.fragment.resources;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ResourcesActivity;
import com.pldt.gabayguro.android.adapter.RedeemedRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.RedeemedModel;
import com.pldt.gabayguro.server.request.Chat;
import com.pldt.gabayguro.server.request.Resources;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RedeemedFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener
        , RedeemedRecyclerViewAdapter.ClickListener
        , EndlessRecyclerViewScrollListener.Callback {
    public static final String TAG = RedeemedFragment.class.getName();

    public static RedeemedFragment newInstance() {
        RedeemedFragment fragment = new RedeemedFragment();
        return fragment;
    }

    private ResourcesActivity mainActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private RedeemedRecyclerViewAdapter redeemedRecyclerViewAdapter;
    private APIRequest apiRequest;

    @BindView(R.id.chatRV)                      RecyclerView chatRV;
    @BindView(R.id.chatSRL)                     SwipeRefreshLayout chatSRL;
    @BindView(R.id.shimmerFL)                   ShimmerFrameLayout shimmerFL;
    @BindView(R.id.noMessageTXT)                TextView noMessageTXT;
    @BindView(R.id.nestedSV)                    NestedScrollView nestedSV;
    @BindView(R.id.progressBar2)                ProgressBar progressBar2;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_redeemed;
    }

    @Override
    public void onViewReady() {
        mainActivity = (ResourcesActivity) getContext();
        mainActivity.setTitle("Redeemed Items");
        setUpRView();
        chatSRL.setOnRefreshListener(this);
    }

    private void setUpRView(){
        redeemedRecyclerViewAdapter = new RedeemedRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        chatRV.setLayoutManager(linearLayoutManager);
        chatRV.setAdapter(redeemedRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        chatRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        redeemedRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Resources.AllRedeemResponse colletionResponse) {
        CollectionTransformer<RedeemedModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            shimmerFL.setVisibility(View.GONE);
            nestedSV.setVisibility(View.VISIBLE);
            if (colletionResponse.isNext()){
                redeemedRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                endlessRecyclerViewScrollListener.reset();
                redeemedRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }

            progressBar2.setVisibility(View.GONE);
            if (redeemedRecyclerViewAdapter.getItemCount() == 0){
                noMessageTXT.setVisibility(View.VISIBLE);
            } else {
                noMessageTXT.setVisibility(View.GONE);
            }
//            groupChatLL.setVisibility(redeemedRecyclerViewAdapter.getData().size() > 0 ? View.GONE : View.VISIBLE);
        }

    }

    public void refreshList(){
        apiRequest = Resources.getDefault().allRedeemed(getContext(), chatSRL);
        apiRequest.first();

    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        progressBar2.setVisibility(View.VISIBLE);
        apiRequest.setPage(page+1);
        apiRequest.nextPage();
    }

    @Override
    public void onItemClick(RedeemedModel resourcesModel) {

    }
}
