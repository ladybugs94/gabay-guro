package com.pldt.gabayguro.android.fragment.coming_soon;

import android.util.Log;
import android.widget.TextView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ComingSoonActivity;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ComingSoon3Fragment extends BaseFragment {
    public static final String TAG = ComingSoon3Fragment.class.getName();

    public static ComingSoon3Fragment newInstance() {
        ComingSoon3Fragment fragment = new ComingSoon3Fragment();
        return fragment;
    }

    private ComingSoonActivity comingSoonActivity;


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_coming_three;
    }

    @Override
    public void onViewReady() {
        comingSoonActivity = (ComingSoonActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @OnClick(R.id.homeBTN)
    void onclicked(){
        comingSoonActivity.startMainActivity("home");
    }

}
