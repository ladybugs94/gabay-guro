package com.pldt.gabayguro.android.fragment.profile;

import android.content.DialogInterface;
import android.util.Log;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.facebook.login.LoginManager;
import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalException;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SettingsFragment extends BaseFragment {
    public static final String TAG = SettingsFragment.class.getName();

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    private final static String[] SCOPES = {"Files.Read"};
    final static String AUTHORITY = "https://login.microsoftonline.com/organizations";
    private ISingleAccountPublicClientApplication mSingleAccountApp;

    @BindView(R.id.logoutBTN)                   TextView logoutBTN;

    private ProfileActivity mainActivity;


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onViewReady() {
        mainActivity = (ProfileActivity) getContext();
        initOffice365();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setTitle("Settings");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LogoutResponse logoutResponse){
        BaseTransformer baseTransformer = logoutResponse.getData(BaseTransformer.class);
        if(baseTransformer.status){
            mainActivity.startLandingActivity("");
            LoginManager.getInstance().logOut();
            ToastMessage.show(mainActivity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.clearData();
        }else{
            ToastMessage.show(mainActivity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", logoutResponse.getData(BaseTransformer.class).msg);
    }

    public void initOffice365(){
        PublicClientApplication.createSingleAccountPublicClientApplication(mainActivity,
                R.raw.auth_config_single_account, new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                    }
                    @Override
                    public void onError(MsalException exception) {
                        //
                        Log.e("ERROR ", exception.getMessage());
                    }
                });
    }

    @OnClick(R.id.logoutBTN)
    void logoutBTNClicked(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        if (mSingleAccountApp!=null){
                            mSingleAccountApp.signOut(new ISingleAccountPublicClientApplication.SignOutCallback() {
                                @Override
                                public void onSignOut() {

                                }
                                @Override
                                public void onError(@NonNull MsalException exception){
                                }
                            });
                        }

                            Auth.getDefault().logout(getContext());
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Are you sure to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    @OnClick(R.id.changePassBTN)
    void updatePassBTN(){
        mainActivity.openUpdatePassFragment();
    }

    @OnClick(R.id.bindBTN)
    void bindBTN(){
        mainActivity.openBindFragment();
    }


}
