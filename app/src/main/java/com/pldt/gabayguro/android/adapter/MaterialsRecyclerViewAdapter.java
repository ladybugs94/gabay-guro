package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.MaterialModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MaterialsRecyclerViewAdapter extends BaseRecylerViewAdapter<MaterialsRecyclerViewAdapter.ViewHolder, MaterialModel>{

    private ClickListener clickListener;

    public MaterialsRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_material));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().title);
        holder.descTXT.setText(holder.getItem().file.filename);
        if (holder.getItem().type.equalsIgnoreCase("application/pdf")){
            Picasso.with(getContext()).load(R.drawable.doc).fit().centerCrop().placeholder(R.drawable.gabay_guro_logo_fav).into(holder.profileIV);
        }else{
            Picasso.with(getContext()).load(R.drawable.image).fit().centerCrop().placeholder(R.drawable.gabay_guro_logo_fav).into(holder.profileIV);
        }

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.descTXT)     TextView descTXT;
        @BindView(R.id.adapterCON)  View adapterCON;
        @BindView(R.id.profileIV)   ImageView profileIV;

        public ViewHolder(View view) {
            super(view);
        }

        public MaterialModel getItem() {
            return (MaterialModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((MaterialModel) v.getTag());
                }
                break;
        }
    }

    public interface ClickListener{
        void onItemClick(MaterialModel coursesModel);
    }
} 
