package com.pldt.gabayguro.android.fragment.coming_soon;

import android.content.DialogInterface;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalException;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ComingSoonActivity;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ComingSoon2Fragment extends BaseFragment {
    public static final String TAG = ComingSoon2Fragment.class.getName();

    public static ComingSoon2Fragment newInstance() {
        ComingSoon2Fragment fragment = new ComingSoon2Fragment();
        return fragment;
    }

    private ComingSoonActivity comingSoonActivity;
    private final static String[] SCOPES = {"Files.Read"};
    private ISingleAccountPublicClientApplication mSingleAccountApp;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_coming_two;
    }

    @Override
    public void onViewReady() {
        comingSoonActivity = (ComingSoonActivity) getContext();
        initOffice365();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LogoutResponse logoutResponse){
        BaseTransformer baseTransformer = logoutResponse.getData(BaseTransformer.class);
        if(baseTransformer.status){
            comingSoonActivity.startLandingActivity("");
            ToastMessage.show(comingSoonActivity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.clearData();
        }else{
            ToastMessage.show(comingSoonActivity,baseTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", logoutResponse.getData(BaseTransformer.class).msg);
    }


    @OnClick(R.id.logoutBTN)
    void logoutBTNClicked(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        if (mSingleAccountApp!=null){
                            mSingleAccountApp.signOut(new ISingleAccountPublicClientApplication.SignOutCallback() {
                                @Override
                                public void onSignOut() {

                                }
                                @Override
                                public void onError(@NonNull MsalException exception){
                                }
                            });
                        }
                        Auth.getDefault().logout(getContext());
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Are you sure to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void initOffice365(){
        PublicClientApplication.createSingleAccountPublicClientApplication(comingSoonActivity,
                R.raw.auth_config_single_account, new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                    }
                    @Override
                    public void onError(MsalException exception) {
                        //
                        Log.e("ERROR ", exception.getMessage());
                    }
                });
    }

}
