package com.pldt.gabayguro.android.fragment.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class OTPFragment extends BaseFragment {
    public static final String TAG = OTPFragment.class.getName();

    public static OTPFragment newInstance() {
        OTPFragment fragment = new OTPFragment();
        return fragment;
    }

    private ProfileActivity profileActivity;

    @BindView(R.id.verifyBTN)           TextView verifyBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_otp;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        profileActivity.setTitle("");
        
        switch (UserData.getUserModel().contactNumberNetworkProvider){
            case "smart":
            case "tnt":
            case "sun":
                verifyBTN.setVisibility(View.VISIBLE);
                break;
            default:
                verifyBTN.setVisibility(View.GONE);
                break;
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @OnClick(R.id.verifyBTN)
    void verifyBTN(){
        profileActivity.openOTPOneFragment();
    }
}
