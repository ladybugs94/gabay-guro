package com.pldt.gabayguro.android.fragment.main;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.MainActivity;
import com.pldt.gabayguro.android.adapter.AnnouncementRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.CommunityRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.QuickLinksRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.QuickLinksRecyclerViewAdapter;
import com.pldt.gabayguro.android.dialog.WebViewDialog;
import com.pldt.gabayguro.data.model.api.AnnouncementsModel;
import com.pldt.gabayguro.data.model.api.QuickLinkModel;
import com.pldt.gabayguro.data.model.api.QuickLinkModel;
import com.pldt.gabayguro.data.model.api.SampleModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Announcements;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class HomeFragment extends BaseFragment implements QuickLinksRecyclerViewAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener, AnnouncementRecyclerViewAdapter.ClickListener {
    public static final String TAG = HomeFragment.class.getName();

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    private MainActivity mainActivity;
    private QuickLinksRecyclerViewAdapter quickLinksRecyclerViewAdapter;
    private CommunityRecyclerViewAdapter communityRecyclerViewAdapter;
    private AnnouncementRecyclerViewAdapter announcementRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private APIRequest apiRequest;

    @BindView(R.id.quickLinkRV)                 RecyclerView quickLinkRV;
    @BindView(R.id.communityRV)                 RecyclerView communityRV;
    @BindView(R.id.announcementRV)              RecyclerView announcementRV;
    @BindView(R.id.homeSRL)                     SwipeRefreshLayout homeSRL;
    @BindView(R.id.shimmerFL)                   ShimmerFrameLayout shimmerFL;
    @BindView(R.id.noMessageTXT)                TextView noMessageTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        setUpListView();
        setupCommunityList();
        setupAnnouncementsList();
        homeSRL.setOnRefreshListener(this);
    }

    private void setUpListView(){
        quickLinksRecyclerViewAdapter = new QuickLinksRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        quickLinkRV.setLayoutManager(linearLayoutManager);
        quickLinkRV.setAdapter(quickLinksRecyclerViewAdapter);
        quickLinksRecyclerViewAdapter.setNewData(getDefaultData());
        quickLinksRecyclerViewAdapter.setClickListener(this);
    }

    private void setupCommunityList(){
        communityRecyclerViewAdapter = new CommunityRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        communityRV.setLayoutManager(linearLayoutManager);
        communityRV.setAdapter(communityRecyclerViewAdapter);
        communityRecyclerViewAdapter.setNewData(getCommunityData());
        communityRV.setNestedScrollingEnabled(false);
    }

    private void setupAnnouncementsList(){
        announcementRecyclerViewAdapter = new AnnouncementRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        announcementRV.setLayoutManager(linearLayoutManager);
        announcementRV.setAdapter(announcementRecyclerViewAdapter);
        announcementRV.setNestedScrollingEnabled(false);
        announcementRecyclerViewAdapter.setClickListener(this);
    }

    private List<QuickLinkModel> getDefaultData(){
        List<QuickLinkModel> androidModels = new ArrayList<>();
        QuickLinkModel defaultItem;

        defaultItem = new QuickLinkModel();
        defaultItem.id = 1;
        defaultItem.name = "Community";
        defaultItem.img = R.drawable.ql_community;
        androidModels.add(defaultItem);

        defaultItem = new QuickLinkModel();
        defaultItem.id = 2;
        defaultItem.name = "LR Portal";
        defaultItem.img = R.drawable.ql_lrmds;
        androidModels.add(defaultItem);

        defaultItem = new QuickLinkModel();
        defaultItem.id = 3;
        defaultItem.name = "Paybills";
        defaultItem.img = R.drawable.ql_paybills;
        androidModels.add(defaultItem);

        defaultItem = new QuickLinkModel();
        defaultItem.id = 4;
        defaultItem.name = "Records";
        defaultItem.img = R.drawable.ql_record;
        androidModels.add(defaultItem);

        defaultItem = new QuickLinkModel();
        defaultItem.id = 5;
        defaultItem.name = "Workspace";
        defaultItem.img = R.drawable.ql_workspace;
        androidModels.add(defaultItem);


        return androidModels;
    }

    private List<SampleModel> getCommunityData(){
        List<SampleModel> androidModels = new ArrayList<>();
        SampleModel defaultItem;
        for(int i = 0; i < 3; i++){
            defaultItem = new SampleModel();
            defaultItem.id = i;
            defaultItem.name = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setSelectedItem("home");
        mainActivity.setTitle("Home");
        mainActivity.homeActive();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Announcements.AllAnnouncementsResponse colletionResponse) {
        CollectionTransformer<AnnouncementsModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            shimmerFL.setVisibility(View.GONE);
            if (colletionResponse.isNext()){
                announcementRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {

                announcementRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }

            if (announcementRecyclerViewAdapter.getItemCount() == 0){
                noMessageTXT.setVisibility(View.VISIBLE);
            } else {
                noMessageTXT.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public void onItemClick(QuickLinkModel sampleModel) {
        switch (sampleModel.id){
            case 1:
                if (UserData.getUserModel().contactNumberStatus.equalsIgnoreCase("verified")){
                    mainActivity.openCommunityFragment();
                }else{
                    mainActivity.startProfileActivity("verify", "main");
                }

                break;
            case 2:
                WebViewDialog.newInstance("LR PORTAL" ,"https://lrmds.deped.gov.ph/login").show(getChildFragmentManager(), TAG);
                break;
            case 3:
            case 4:
                mainActivity.startComingSoonActivity("three");
                break;
            case 5:
                mainActivity.startCoursesActivity("workspace", 0);
                break;
        }
    }

    public void refreshList(){
        apiRequest = Announcements.getDefault().allAnnouncements(getContext(), homeSRL);
        apiRequest.setPerPage(3);
        apiRequest.first();
    }

    @OnClick(R.id.viewAnnounceBTN)
    void onClicked(){
        mainActivity.startAnnouncementsActivity("announce", 0);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(AnnouncementsModel sampleModel) {
        if (sampleModel.webUrl.isEmpty()){
            mainActivity.startAnnouncementsActivity("details", sampleModel.announcementId);
        }else{
            WebViewDialog.newInstance(sampleModel.title, sampleModel.webUrl).show(getChildFragmentManager(), TAG);
        }
    }
}
