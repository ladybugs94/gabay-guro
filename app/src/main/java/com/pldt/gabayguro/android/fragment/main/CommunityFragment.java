package com.pldt.gabayguro.android.fragment.main;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.MainActivity;
import com.pldt.gabayguro.android.adapter.CoursesRecyclerViewAdapter;
import com.pldt.gabayguro.android.dialog.WebViewDialog;
import com.pldt.gabayguro.data.model.api.CoursesModel;
import com.pldt.gabayguro.server.request.Community;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class CommunityFragment extends BaseFragment implements CoursesRecyclerViewAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener{
    public static final String TAG = CommunityFragment.class.getName();

    public static CommunityFragment newInstance() {
        CommunityFragment fragment = new CommunityFragment();
        return fragment;
    }

    private MainActivity mainActivity;
    private CoursesRecyclerViewAdapter communityRecyclerViewAdapter;

    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager otherLinearLayoutManager;
    private APIRequest apiRequest;

    @BindView(R.id.communityRV)                 RecyclerView communityRV;
    @BindView(R.id.otherRV)                     RecyclerView otherRV;
    @BindView(R.id.placeholder1)                TextView placeholder1;
    @BindView(R.id.placeholder2)                TextView placeholder2;
    @BindView(R.id.courseCountBTN)              TextView courseCountBTN;
    @BindView(R.id.otherCountBTN)               TextView otherCountBTN;
    @BindView(R.id.shimmerFL) ShimmerFrameLayout shimmerFL;
    @BindView(R.id.shimmerFLs) ShimmerFrameLayout shimmerFL2;
    @BindView(R.id.communitySRL)                SwipeRefreshLayout communitySRL;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_community;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        setupCommunityList();
        communitySRL.setOnRefreshListener(this);
    }

    private void setupCommunityList(){
        communityRecyclerViewAdapter = new CoursesRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        otherLinearLayoutManager = new LinearLayoutManager(getContext());
        communityRV.setLayoutManager(linearLayoutManager);
        communityRV.setAdapter(communityRecyclerViewAdapter);
        communityRV.setNestedScrollingEnabled(false);
        otherRV.setLayoutManager(otherLinearLayoutManager);
        otherRV.setAdapter(communityRecyclerViewAdapter);
        otherRV.setNestedScrollingEnabled(false);
        communityRecyclerViewAdapter.setClickListener(this);
    }



    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setTitle("Community");
        mainActivity.communityActive();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Community.AllCoursesResponse response){
        CollectionTransformer<CoursesModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            communityRecyclerViewAdapter.setNewData(collectionTransformer.data);
            courseCountBTN.setText(String.valueOf(collectionTransformer.data.size()));
            otherCountBTN.setText(String.valueOf(collectionTransformer.data.size()));
            shimmerFL.setVisibility(View.GONE);
            shimmerFL2.setVisibility(View.GONE);
            if (collectionTransformer.data.size() != 0) {
                placeholder2.setVisibility(View.GONE);
                placeholder1.setVisibility(View.GONE);
                otherRV.setVisibility(View.VISIBLE);
                communityRV.setVisibility(View.VISIBLE);
            }else{
                placeholder2.setVisibility(View.VISIBLE);
                placeholder1.setVisibility(View.VISIBLE);
                otherRV.setVisibility(View.GONE);
                communityRV.setVisibility(View.GONE);
            }
        }else{
            ToastMessage.show(getContext(), collectionTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @OnClick(R.id.frontLearnersBTN)
    void setupFrontLearners(){
        WebViewDialog.newInstance("FRONTLEARNERS","https://frontlearners.com/blended/index.php").show(getChildFragmentManager(), WebViewDialog.TAG);
    }

    @OnClick(R.id.mvpBTN)
    void setupMVP(){
        mainActivity.startComingSoonActivity("three");
    }

    @OnClick(R.id.resourcesBTN)
    void setupresourcesBTN(){
        mainActivity.startResourcesActivity("resources");
    }

    @OnClick(R.id.walletBTN)
    void onWalletCON(){
        String packageName = "com.paymaya";
        Intent intent = getContext().getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    @OnClick(R.id.courseCountBTN)
    void setupcourseCountBTN(){
        mainActivity.startCoursesActivity("all_courses", 0);
    }

    @OnClick(R.id.otherCountBTN)
    void setupotherCountBTN(){
        mainActivity.startCoursesActivity("other_courses", 0);
    }

    public  void refreshList(){
        apiRequest = Community.getDefault().allCourses(getContext(), communitySRL);
        apiRequest.setPerPage(3);
        apiRequest.first();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(CoursesModel coursesModel) {
        mainActivity.startCoursesActivity("detail_courses", coursesModel.courseId);
    }
}
