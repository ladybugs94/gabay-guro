package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;

import com.akexorcist.roundcornerprogressbar.IconRoundCornerProgressBar;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.BadgeModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BadgeRecyclerViewAdapter extends BaseRecylerViewAdapter<BadgeRecyclerViewAdapter.ViewHolder, BadgeModel>{

    private ClickListener clickListener;

    public BadgeRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_badges));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.nameTXT.setText(holder.getItem().name);
        holder.adapterCON.setTag(getItem(position));
        holder.adapterCON.setOnClickListener(this);
        boolean stop = false;

        if(holder.getItem().bronzeRequirement > 0){
            if (holder.getItem().bronzeProgressPercentage == 100){
                holder.progress_bar.setProgress(holder.getItem().bronzeProgressPercentage);
                holder.progressTXT.setText("(" + holder.getItem().bronzeProgressPercentage + "%)");
                if (!holder.getItem().bronzeThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().bronzeThumbnail.fullPath).into(holder.imageIV);
                    holder.badgeLabelTXT.setText("Bronze");
                }
            }else {
                holder.progress_bar.setProgress(holder.getItem().bronzeProgressPercentage);
                holder.progressTXT.setText("(" + holder.getItem().bronzeProgressPercentage + "%)");
                if (!holder.getItem().bronzeThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().bronzeThumbnail.fullPath).into(holder.imageIV);
                    holder.badgeLabelTXT.setText("Bronze");
                }
                holder.imageIV.setAlpha(0.5f);
                stop = true;
            }
        }

        if(!stop && holder.getItem().silverRequirement > 0 ) {
            if (holder.getItem().silverProgressPercentage == 100) {
                holder.progress_bar.setProgress(holder.getItem().silverProgressPercentage);
                holder.progressTXT.setText("(" + holder.getItem().silverProgressPercentage + "%)");
                if (!holder.getItem().silverThumbnail.fullPath.isEmpty()) {
                    Picasso.with(getContext()).load(holder.getItem().silverThumbnail.fullPath).into(holder.imageIV);
                    holder.badgeLabelTXT.setText("Silver");
                }
            } else {
                holder.progress_bar.setProgress(holder.getItem().silverProgressPercentage);
                holder.progressTXT.setText("(" + holder.getItem().silverProgressPercentage + "%)");
                if (!holder.getItem().silverThumbnail.fullPath.isEmpty()) {
                    Picasso.with(getContext()).load(holder.getItem().silverThumbnail.fullPath).into(holder.imageIV);
                    holder.badgeLabelTXT.setText("Silver");
                }
                holder.imageIV.setAlpha(0.5f);
                stop = true;
            }
        }


        if(!stop && holder.getItem().goldRequirement > 0 ){
            if (holder.getItem().goldProgressPercentage == 100){
                holder.progress_bar.setProgress(holder.getItem().goldProgressPercentage);
                holder.progressTXT.setText("(" + holder.getItem().goldProgressPercentage + "%)");
                if (!holder.getItem().goldThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().goldThumbnail.fullPath).into(holder.imageIV);
                    holder.badgeLabelTXT.setText("Gold");
                }
            }else {
                holder.progress_bar.setProgress(holder.getItem().goldProgressPercentage);
                holder.progressTXT.setText("(" + holder.getItem().goldProgressPercentage + "%)");
                if (!holder.getItem().goldThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().goldThumbnail.fullPath).into(holder.imageIV);
                    holder.badgeLabelTXT.setText("Gold");
                }
                holder.imageIV.setAlpha(0.5f);
                stop = true;
            }
        }

        if(!stop &&  holder.getItem().platinumRequirement > 0){
            if (holder.getItem().platinumProgressPercentage == 100){
                holder.progress_bar.setProgress(holder.getItem().platinumProgressPercentage);
                holder.progressTXT.setText("(" + holder.getItem().platinumProgressPercentage + "%)");
                if (!holder.getItem().platinumThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().platinumThumbnail.fullPath).into(holder.imageIV);
                    holder.badgeLabelTXT.setText("Platinum");
                }
            }else {
                holder.progress_bar.setProgress(holder.getItem().platinumProgressPercentage);
                holder.progressTXT.setText("(" + holder.getItem().platinumProgressPercentage + "%)");
                if (!holder.getItem().platinumThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().platinumThumbnail.fullPath).into(holder.imageIV);
                    holder.badgeLabelTXT.setText("Platinum");
                }
                holder.imageIV.setAlpha(0.5f);
                stop = true;
            }
        }


    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.progressTXT)     TextView progressTXT;
        @BindView(R.id.badgeLabelTXT)     TextView badgeLabelTXT;
        @BindView(R.id.adapterCON)     View adapterCON;
        @BindView(R.id.imageIV)     ImageView imageIV;
        @BindView(R.id.progress_bar) RoundCornerProgressBar progress_bar;

        public ViewHolder(View view) {
            super(view);
        }

        public BadgeModel getItem() {
            return (BadgeModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((BadgeModel) v.getTag());
                }
                break;
        }
    }

    public interface ClickListener{
        void onItemClick(BadgeModel sampleModel);
    }
} 
