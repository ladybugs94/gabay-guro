package com.pldt.gabayguro.android.fragment.messages;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.MessageActivity;
import com.pldt.gabayguro.android.adapter.UserRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.server.request.Chat;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SearchUserFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener
        , UserRecyclerViewAdapter.ClickListener
        , EndlessRecyclerViewScrollListener.Callback {
    public static final String TAG = SearchUserFragment.class.getName();

    public static SearchUserFragment newInstance() {
        SearchUserFragment fragment = new SearchUserFragment();
        return fragment;
    }

    private MessageActivity messageActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private UserRecyclerViewAdapter userRecyclerViewAdapter;
    private APIRequest apiRequest;

    @BindView(R.id.chatRV)                      RecyclerView chatRV;
    @BindView(R.id.chatSRL)                     SwipeRefreshLayout chatSRL;
    @BindView(R.id.shimmerFL)                   ShimmerFrameLayout shimmerFL;
    @BindView(R.id.noMessageTXT)                TextView noMessageTXT;
    @BindView(R.id.searchBTN)                   EditText searchBTN;
    @BindView(R.id.nestedSV)                    NestedScrollView nestedSV;
    @BindView(R.id.progressBar2)                ProgressBar progressBar2;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_user_list;
    }

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity) getContext();
        messageActivity.setTitle("Users");
        setUpRView();
        chatSRL.setOnRefreshListener(this);

        searchBTN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                apiRequest = Chat.getDefault().searchUser(getContext(), chatSRL, searchBTN.getText().toString().trim());
                apiRequest.first();
            }
        });
    }

    private void setUpRView(){
        userRecyclerViewAdapter = new UserRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        chatRV.setLayoutManager(linearLayoutManager);
        chatRV.setAdapter(userRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        chatRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        userRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Chat.SearchUserResponse colletionResponse) {
        CollectionTransformer<UserModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            shimmerFL.setVisibility(View.GONE);
            nestedSV.setVisibility(View.VISIBLE);
            if (colletionResponse.isNext()){
                userRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                endlessRecyclerViewScrollListener.reset();
                userRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }

            progressBar2.setVisibility(View.GONE);
            if (userRecyclerViewAdapter.getItemCount() == 0){
                noMessageTXT.setVisibility(View.VISIBLE);
            } else {
                noMessageTXT.setVisibility(View.GONE);
            }
        }

    }

    public void refreshList(){
        apiRequest = Chat.getDefault().searchUser(getContext(), chatSRL, "");
        apiRequest.first();

    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(UserModel sampleModel) {
        messageActivity.startMessageActivity("conversation", 0, sampleModel.userId, sampleModel.avatar.fullPath, sampleModel.name);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        progressBar2.setVisibility(View.VISIBLE);
        apiRequest.setPage(page+1);
        apiRequest.nextPage();
    }
}
