package com.pldt.gabayguro.android.activity;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.fragment.resources.RedeemedFragment;
import com.pldt.gabayguro.android.fragment.resources.ResourceDetailsFragment;
import com.pldt.gabayguro.android.fragment.resources.ResourcesFragment;
import com.pldt.gabayguro.android.fragment.wallet.WalletFragment;
import com.pldt.gabayguro.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ResourcesActivity extends RouteActivity {
    public static final String TAG = ResourcesActivity.class.getName();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "redeemed":
                openRedemedFragment();
                break;
            case "resources":
            default:
                openResourcesFragment();
                break;
        }
    }

    public void openResourcesFragment(){
        switchFragment(ResourcesFragment.newInstance());
    }
    public void openRedemedFragment(){
        switchFragment(RedeemedFragment.newInstance());
    }
    public void openResourcesDetailsFragment(int id){
        switchFragment(ResourceDetailsFragment.newInstance(id));
    }
}
