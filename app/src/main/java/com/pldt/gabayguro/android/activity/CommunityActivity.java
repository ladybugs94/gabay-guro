package com.pldt.gabayguro.android.activity;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.fragment.DefaultFragment;
import com.pldt.gabayguro.android.fragment.community.AllCommentsFragment;
import com.pldt.gabayguro.android.fragment.community.AllCoursesFragment;
import com.pldt.gabayguro.android.fragment.community.CourseDetailsFragment;
import com.pldt.gabayguro.android.fragment.community.OtherCoursesFragment;
import com.pldt.gabayguro.android.fragment.community.WorkSpaceFragment;
import com.pldt.gabayguro.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class CommunityActivity extends RouteActivity {
    public static final String TAG = CommunityActivity.class.getName();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "all_courses":
                openAllCoursesFragment();
                break;
            case "other_courses":
                openOtherCoursesFragment();
                break;
            case "workspace":
                openWorkSpaceFragment();
                break;
            case "detail_courses":
                openDetailsCoursesFragment(getFragmentBundle().getInt("id"));
                break;
            default:
                openAllCoursesFragment();
                break;
        }
    }

    public void openAllCoursesFragment(){ switchFragment(AllCoursesFragment.newInstance()); }
    public void openOtherCoursesFragment(){ switchFragment(OtherCoursesFragment.newInstance()); }
    public void openWorkSpaceFragment(){ switchFragment(WorkSpaceFragment.newInstance()); }
    public void openDetailsCoursesFragment(int id){ switchFragment(CourseDetailsFragment.newInstance(id)); }
    public void openAllCommentsFragment(int id){ switchFragment(AllCommentsFragment.newInstance(id)); }
}
