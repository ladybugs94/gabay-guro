package com.pldt.gabayguro.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.BadgeDetailsModel;
import com.pldt.gabayguro.data.model.api.BadgeModel;
import com.pldt.gabayguro.data.model.api.QuickLinkModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;
import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BadgeDetailsRecyclerViewAdapter extends BaseRecylerViewAdapter<BadgeDetailsRecyclerViewAdapter.ViewHolder, BadgeDetailsModel>{

    private ClickListener clickListener;

    public BadgeDetailsRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_badges));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.nameTXT.setText(holder.getItem().name);
        holder.progress_bar.setProgress(holder.getItem().progress);
        holder.progressTXT.setText("(" + holder.getItem().progress + "%)");
        if (!holder.getItem().image.isEmpty()){
            Picasso.with(getContext()).load(holder.getItem().image).into(holder.imageIV);
        }
        holder.badgeLabelTXT.setText(holder.getItem().badge);
        if (holder.getItem().progress!=100){
            holder.imageIV.setAlpha(0.5f);
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.progressTXT)     TextView progressTXT;
        @BindView(R.id.badgeLabelTXT)     TextView badgeLabelTXT;
        @BindView(R.id.imageIV)     ImageView imageIV;
        @BindView(R.id.progress_bar) RoundCornerProgressBar progress_bar;

        public ViewHolder(View view) {
            super(view);
        }

        public BadgeDetailsModel getItem() {
            return (BadgeDetailsModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(BadgeDetailsModel sampleModel);
    }
} 
