package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.ChatThreadModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class InboxRecyclerViewAdapter extends BaseRecylerViewAdapter<InboxRecyclerViewAdapter.ViewHolder, ChatThreadModel> {

    private ClickListener clickListener;
    private int myId;

    private static final int MESSAGE_IN = 1;
    private static final int MESSAGE_OUT = 2;
    private static final int ANNOUNCEMENT = 3;

    public InboxRecyclerViewAdapter(Context context) {
        super(context);
        myId = UserData.getUserModel().userId;
    }

    public void newMessage(ChatThreadModel messageModel){
        getData().add(0, messageModel);
        notifyItemInserted(0);
//        notifyItemRangeChanged(0, getData().size());
    }



    public void updateMessage(ChatThreadModel messageModel){
            int pos = getData().indexOf(messageModel);
            if (pos >= 0){
                getData().set(pos, messageModel);
                notifyItemChanged(pos);
            }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case MESSAGE_OUT:
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_message_out));
            case MESSAGE_IN:
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_message_in));
            case ANNOUNCEMENT:
                return new ViewHolder(getDefaultView(parent, R.layout.adapter_message_announcement));
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        switch(getData().get(position).type){
            case "announcement":
                return ANNOUNCEMENT;
            default:
                if(getData().get(position).userId == myId){
                    return MESSAGE_OUT;
                }else{
                    return MESSAGE_IN;
                }
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));

        holder.messageTXT.setText(holder.getItem().content);

        holder.adapterCON.setTag(position);
        holder.imageIV.setTag(holder.getItem());
        holder.fileCON.setTag(holder.getItem());
        holder.adapterCON.setOnLongClickListener(this);
        holder.adapterCON.setOnClickListener(this);
        holder.imageIV.setOnClickListener(this);
        holder.fileCON.setOnClickListener(this);

        holder.nameTXT.setText(holder.getItem().sender.data.name);
        holder.dateTXT.setText(holder.getItem().dateCreated.timePassed);

        if (holder.getItem().sender.data.avatar.fullPath.isEmpty()){
            Picasso.with(getContext())
                    .load(R.drawable.nav_user)
                    .placeholder(R.drawable.nav_user)
                    .error(R.drawable.nav_user)
                    .into(holder.profileCIV);
        }else {
            Picasso.with(getContext())
                    .load(holder.getItem().sender.data.avatar.fullPath)
                    .placeholder(R.drawable.nav_user)
                    .error(R.drawable.nav_user)
                    .into(holder.profileCIV);
        }

//        holder.onlineBTN.setBackground(holder.getItem().author.data.is_online ? ActivityCompat.getDrawable(getContext(), R.drawable.bg_circle_online) : ActivityCompat.getDrawable(getContext(), R.drawable.bg_circle_offlinee));

        switch(holder.getItem().type){
            case "text":
                holder.imageIV.setVisibility(View.GONE);
                holder.fileCON.setVisibility(View.GONE);
                holder.messageTXT.setVisibility(View.VISIBLE);
                break;
            case "image":
                holder.imageIV.setVisibility(View.VISIBLE);
                holder.fileCON.setVisibility(View.GONE);
                holder.messageTXT.setVisibility(View.GONE);
                Picasso.with(getContext())
                        .load(holder.getItem().attachment.fullPath)
                        .error(R.drawable.gabay_guro_logo)
                        .centerCrop()
                        .fit()
                        .into(holder.imageIV);
                break;
            case "file":
                holder.imageIV.setVisibility(View.GONE);
                holder.fileCON.setVisibility(View.VISIBLE);
                holder.fileTXT.setText(holder.getItem().content);
                holder.messageTXT.setVisibility(View.GONE);
                holder.videoIV.setVisibility(View.GONE);
                holder.downloadIV.setVisibility(View.VISIBLE);
                break;
            case "video":
                holder.imageIV.setVisibility(View.GONE);
                holder.fileCON.setVisibility(View.VISIBLE);
                holder.fileTXT.setText(holder.getItem().content);
                holder.messageTXT.setVisibility(View.GONE);
                holder.videoIV.setVisibility(View.VISIBLE);
                holder.downloadIV.setVisibility(View.GONE);
                break;
        }



        if (holder.getItem().sender.data.userId != UserData.getUserModel().userId){
            holder.profileCIV.setTag(holder.getItem());
            holder.profileCIV.setOnClickListener(this);
        }

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)         TextView nameTXT;
        @BindView(R.id.dateTXT)         TextView dateTXT;
        @BindView(R.id.messageTXT)      TextView messageTXT;
        @BindView(R.id.profileCIV)      ImageView profileCIV;
        @BindView(R.id.imageIV)         ImageView imageIV;
        @BindView(R.id.downloadIV)         ImageView downloadIV;
        @BindView(R.id.videoIV)         ImageView videoIV;
        @BindView(R.id.fileCON)         LinearLayout fileCON;
        @BindView(R.id.fileTXT)          TextView fileTXT;
        @BindView(R.id.adapterCON)      View adapterCON;
        @BindView(R.id.onlineBTN)       View onlineBTN;

        public ViewHolder(View view) {
            super(view);
        }

        public ChatThreadModel getItem() {
            return (ChatThreadModel) super.getItem();
        }
    }

    private void otherOptionClick(final View view, int position) {
        final ChatThreadModel chatThreadModel = getItem(position);
        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.getMenuInflater().inflate(R.menu.pop_menu_chat_other_option, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.deleteBTN:
                        if (clickListener != null){
                            clickListener.onDeleteClick(chatThreadModel);
                        }
                        break;
                        case R.id.deleteAllBTN:
                        if (clickListener != null){
                            clickListener.onDeleteAllClick(chatThreadModel);
                        }
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
            case R.id.imageIV:
            case R.id.fileCON:
                int position  = (int) v.getTag();
                if (getData().get(position).userId == myId) {
                    otherOptionClick(v, position);
                }
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fileCON:
                if(clickListener != null){
                    clickListener.onFileClick((ChatThreadModel) v.getTag());
                }
                break;
            case R.id.imageIV:
                if(clickListener != null){
                    clickListener.onImageClick((ChatThreadModel) v.getTag());
                }
                break;
            case R.id.profileCIV:
                if(clickListener != null){
                    clickListener.onAvatarClick((ChatThreadModel) v.getTag());
                }
                break;
        }
    }

    public boolean isMe(){
        for (ChatThreadModel chatThreadModel : getData()){
            return chatThreadModel.sender.data.userId == myId;
        }
        return false;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(ChatThreadModel sampleModel);
        void onDeleteClick(ChatThreadModel sampleModel);
        void onDeleteAllClick(ChatThreadModel sampleModel);
        void onFileClick(ChatThreadModel sampleModel);
        void onImageClick(ChatThreadModel sampleModel);
        void onAvatarClick(ChatThreadModel sampleModel);
    }
} 
