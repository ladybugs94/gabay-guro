package com.pldt.gabayguro.android.activity;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.fragment.DefaultFragment;
import com.pldt.gabayguro.android.fragment.wallet.WalletFragment;
import com.pldt.gabayguro.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class WalletActivity extends RouteActivity {
    public static final String TAG = WalletActivity.class.getName();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "wallet":
                openWalletFragment();
                break;
            default:
                openWalletFragment();
                break;
        }
    }

    public void openWalletFragment(){
        switchFragment(WalletFragment.newInstance());
    }
}
