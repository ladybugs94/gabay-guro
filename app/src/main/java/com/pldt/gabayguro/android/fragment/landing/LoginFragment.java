package com.pldt.gabayguro.android.fragment.landing;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.IAccount;
import com.microsoft.identity.client.IAuthenticationResult;
import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalException;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.LandingActivity;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class LoginFragment extends BaseFragment implements  LandingActivity.FBCallback, LandingActivity.GmailCallback{
    public static final String TAG = LoginFragment.class.getName();

    private LandingActivity landingActivity;
    private FirebaseAnalytics firebaseAnalytics;

    private final static String[] SCOPES = {"Files.Read"};
    final static String AUTHORITY = "https://login.microsoftonline.com/organizations";
    private ISingleAccountPublicClientApplication mSingleAccountApp;

    @BindView(R.id.emailET)              EditText usernameET;
    @BindView(R.id.passET)              EditText passwordET;
    @BindView(R.id.loginBTN)                TextView loginBTN;
    @BindView(R.id.forgotBTN)               TextView forgotBTN;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        firebaseAnalytics = FirebaseAnalytics.getInstance(landingActivity);
        initOffice365();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        try{
            SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
            if (singleTransformer.status){
                UserData.insert(singleTransformer.data);
                UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);

                Bundle bundle = new Bundle();
                bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, singleTransformer.data.userId);
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, singleTransformer.data.name);
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

                landingActivity.startMainActivity("home");
            } else{
                ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
            }
            Log.e("Message", singleTransformer.token);

        } catch(Exception e){

        }
    }

    @Subscribe
    public void onResponse(Auth.FBResponse loginResponse){
        SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            UserData.insert(singleTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            landingActivity.startMainActivity("home");
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", singleTransformer.token);
    }

    @Subscribe
    public void onResponse(Auth.Office365Response loginResponse){
        SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            UserData.insert(singleTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            landingActivity.startMainActivity("home");
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", singleTransformer.token);
    }

    @Subscribe
    public void onResponse(Auth.GoogleResponse loginResponse){
        SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            UserData.insert(singleTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, singleTransformer.token);
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            landingActivity.startMainActivity("home");
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
        Log.e("Message", singleTransformer.token);
    }


    @OnClick(R.id.loginBTN)
    void signInBTNClicked(){ Auth.getDefault().login(getContext(), usernameET.getText().toString(), passwordET.getText().toString()); }

    @OnClick(R.id.signUpBTN)
    void registerBTNClicked(){
        landingActivity.startRegisterActivity("signup");
    }

    @OnClick(R.id.forgotBTN)
    void forgotPassBTNClicked(){
        landingActivity.startRegisterActivity("forgot");
    }

    @OnClick(R.id.fbBTN)
    void signInFBBTNClicked(){
        landingActivity.attemptFBLogin(this);
    }

    @OnClick(R.id.gmailBTN)
    void signInGoogleBTNClicked(){
        landingActivity.attemptGMLogin(this);
    }

    @OnClick(R.id.office365BTN)
    void signInoffice365BTNClicked(){
        mSingleAccountApp.signIn(landingActivity, null, SCOPES, getAuthInteractiveCallback());
    }


    @Override
    public void success(UserModel userModel) {
//        Auth.getDefault().fbLogin(getContext(),  landingActivity.getAccessToken(), userModel.fbID);
        Log.e("TokenS", ">>>" + landingActivity.getAccessToken());
    }

    @Override
    public void failed() {

    }

    @Override
    public void successGmail(UserModel userModel) {
//        Auth.getDefault().googleLogin(getContext(),  userModel.accessToken, userModel.googleID);
        Log.e("TokenS", ">>>" +userModel.accessToken);
    }

    public void initOffice365(){
        PublicClientApplication.createSingleAccountPublicClientApplication(landingActivity,
                R.raw.auth_config_single_account, new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                    }
                    @Override
                    public void onError(MsalException exception) {
                        //
                        Log.e("ERROR ", exception.getMessage());
                    }
                });
    }

    private AuthenticationCallback getAuthInteractiveCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                Log.e("Access Token: ", authenticationResult.getAccessToken());
                Auth.getDefault().office365Login(getContext(),  authenticationResult.getAccessToken());
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString());
            }
            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.");
            }
        };
    }

}
