package com.pldt.gabayguro.android.activity;


import android.view.View;
import android.widget.ImageView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.fragment.messages.SearchUserFragment;
import com.pldt.gabayguro.android.fragment.messages.ThreadFragment;
import com.pldt.gabayguro.android.fragment.profile.OTPTwoFragment;
import com.pldt.gabayguro.android.fragment.profile.OTPoneFragment;
import com.pldt.gabayguro.android.fragment.profile.SettingsFragment;
import com.pldt.gabayguro.android.fragment.profile.UpdatePasswordFragment;
import com.pldt.gabayguro.android.fragment.profile.UpdateProfileFragment;
import com.pldt.gabayguro.android.route.RouteActivity;
import com.pldt.gabayguro.server.request.Chat;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class MessageActivity extends RouteActivity {
    public static final String TAG = MessageActivity.class.getName();

    @BindView(R.id.activityIconBTN)             ImageView activityIconBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_message;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "conversation":
                openConversationFragment();
                break;
            case "user":
                openSearchFragment();
                activityIconBTN.setVisibility(View.GONE);
                break;
        }
    }

    public void openConversationFragment(){ switchFragment(ThreadFragment.newInstance()); }
    public void openSearchFragment(){ switchFragment(SearchUserFragment.newInstance()); }


    public void setAvatar(String url){
        if (!url.isEmpty()) {
            Picasso.with(getContext()).load(url).fit().centerCrop().placeholder(R.drawable.nav_user).into(activityIconBTN);
        }
    }
}
