package com.pldt.gabayguro.android.fragment.main;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.MainActivity;
import com.pldt.gabayguro.android.adapter.RedeemedRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.CommunityRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.MyBadgeRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.QuickLinksRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.RedeemedRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.MyBadgeModel;
import com.pldt.gabayguro.data.model.api.QuickLinkModel;
import com.pldt.gabayguro.data.model.api.RedeemedModel;
import com.pldt.gabayguro.data.model.api.SampleModel;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.server.request.Badges;
import com.pldt.gabayguro.server.request.Resources;
import com.pldt.gabayguro.server.request.Wallet;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ProfileFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = ProfileFragment.class.getName();

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    private MainActivity mainActivity;
    private MyBadgeRecyclerViewAdapter myBadgeRecyclerViewAdapter;
    private RedeemedRecyclerViewAdapter redeemedRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private APIRequest apiRequest;

    @BindView(R.id.transactionRV)               RecyclerView transactionRV;
    @BindView(R.id.badgeRV)                     RecyclerView badgeRV;
    @BindView(R.id.nameTXT)                     TextView nameTXT;
    @BindView(R.id.usernameTXT)                 TextView usernameTXT;
    @BindView(R.id.bioTXT)                      TextView bioTXT;
    @BindView(R.id.birthTXT)                    TextView birthTXT;
    @BindView(R.id.joinedTXT)                   TextView joinedTXT;
    @BindView(R.id.pointsTXT)                   TextView pointsTXT;
    @BindView(R.id.placeholder1)                   TextView placeholder1;
    @BindView(R.id.placeholder2)                   TextView placeholder2;
    @BindView(R.id.badgeCON)                   View badgeCON;
    @BindView(R.id.profileIMG)                  ImageView profileIMG;
    @BindView(R.id.profileSRL)                  SwipeRefreshLayout profileSRL;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        profileSRL.setOnRefreshListener(this);
        setupAnnouncementsList();
        setUpListView();
    }

    private void setupAnnouncementsList(){
        redeemedRecyclerViewAdapter = new RedeemedRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        transactionRV.setLayoutManager(linearLayoutManager);
        transactionRV.setAdapter(redeemedRecyclerViewAdapter);
        transactionRV.setNestedScrollingEnabled(false);
    }

    private void setUpListView(){
        myBadgeRecyclerViewAdapter = new MyBadgeRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        badgeRV.setLayoutManager(linearLayoutManager);
        badgeRV.setAdapter(myBadgeRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setTitle("Profile");
        mainActivity.profileActive();
        refreshList();
    }

    public void refreshList(){
        Auth.getDefault().myProfile(getContext(), profileSRL);
        Wallet.getDefault().walletPoints(getContext());
        Badges.getDefault().myBadges(getContext(), profileSRL).execute();
        apiRequest = Resources.getDefault().allRedeemed(getContext(), profileSRL);
        apiRequest.setPerPage(3);
        apiRequest.first();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.MyProfileResponse response){
        SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status){
            nameTXT.setText(singleTransformer.data.name);
            usernameTXT.setText("@" + singleTransformer.data.username);
            if (singleTransformer.data.bio.isEmpty()){
                bioTXT.setText("Bio");
            }else{
                bioTXT.setText(singleTransformer.data.bio);
            }
            if (!singleTransformer.data.birthdate.isEmpty()){
                birthTXT.setText("Born: " + singleTransformer.data.birthdate);
            }else{
                birthTXT.setText("Born: --");
            }

            joinedTXT.setText("Joined: " + singleTransformer.data.dateCreated.dateDb);
            if (!singleTransformer.data.avatar.fullPath.isEmpty()){
                Picasso.with(getContext()).load(singleTransformer.data.avatar.fullPath).fit().centerCrop().into(profileIMG);
            }
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Wallet.WalletResponse response){
        BaseTransformer singleTransformer = response.getData(BaseTransformer.class);
        if (singleTransformer.status){
            pointsTXT.setText("My Points: "+ singleTransformer.walletDisplay);
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Badges.MyBadgesResponse response){
        CollectionTransformer<MyBadgeModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            myBadgeRecyclerViewAdapter.setNewData(collectionTransformer.data);

            if (collectionTransformer.data.size() !=0){
                placeholder2.setVisibility(View.GONE);
                badgeCON.setVisibility(View.VISIBLE);
            }else{
                placeholder2.setVisibility(View.VISIBLE);
                badgeCON.setVisibility(View.GONE);
            }

        }else{
            ToastMessage.show(getContext(), collectionTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Resources.AllRedeemResponse response){
        CollectionTransformer<RedeemedModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            redeemedRecyclerViewAdapter.setNewData(collectionTransformer.data);

            if (collectionTransformer.data.size() !=0){
                placeholder1.setVisibility(View.GONE);
            }else{
                placeholder1.setVisibility(View.VISIBLE);
            }

        }else{
            ToastMessage.show(getContext(), collectionTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @OnClick(R.id.updateBTN)
    void onUpdateBTN(){
        mainActivity.startProfileActivity("update_profile", "profile");
    }

    @OnClick(R.id.viewAllBTN)
    void onViewAllBTN(){
        mainActivity.startProfileActivity("badge", "");
    }

    @OnClick(R.id.pointsTXT)
    void onPointsTXT(){
        mainActivity.startProfileActivity("points", "");
    }

    @OnClick(R.id.viewBTN)
    void onviewBTN(){
        mainActivity.startResourcesActivity("redeemed");
    }

    @OnClick(R.id.walletCON)
    void onWalletCON(){
//        mainActivity.startWalletActivity("wallet");
        String packageName = "com.paymaya";
        Intent intent = getContext().getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=" + packageName));
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
}
