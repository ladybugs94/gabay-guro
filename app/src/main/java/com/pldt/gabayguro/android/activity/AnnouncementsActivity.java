package com.pldt.gabayguro.android.activity;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.fragment.announcements.AnnouncementsDetailsFragment;
import com.pldt.gabayguro.android.fragment.announcements.AnnouncementsFragment;
import com.pldt.gabayguro.android.fragment.wallet.WalletFragment;
import com.pldt.gabayguro.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AnnouncementsActivity extends RouteActivity {
    public static final String TAG = AnnouncementsActivity.class.getName();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_profile;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "announce":
                openAnnounceFragment();
                break;
            case "details":
                openDetailsFragment(getFragmentBundle().getInt("id"));
                break;
            default:
                openAnnounceFragment();
                break;
        }
    }

    public void openAnnounceFragment(){
        switchFragment(AnnouncementsFragment.newInstance());
    }
    public void openDetailsFragment(int id){ switchFragment(AnnouncementsDetailsFragment.newInstance(id)); }
}
