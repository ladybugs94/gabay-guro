package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.RedeemedModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class RedeemedRecyclerViewAdapter extends BaseRecylerViewAdapter<RedeemedRecyclerViewAdapter.ViewHolder, RedeemedModel> {

    private ClickListener clickListener;

    public RedeemedRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_redeemed));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().item.data.get(0).productName);
        holder.descTXT.setText(holder.getItem().item.data.get(0).product.data.content);
        holder.priceTXT.setText(holder.getItem().item.data.get(0).priceDisplay);
        Picasso.with(getContext()).load(holder.getItem().item.data.get(0).product.data.thumbnail.fullPath).fit().centerCrop().into(holder.resourceIMG);
        switch (holder.getItem().statusDisplay) {
            case "ongoing":
                holder.statusTXT.setText("Ongoing");
                holder.statusTXT.setBackgroundResource(R.drawable.bg_toast_failed);
                break;
            case "complete":
                holder.statusTXT.setText("Completed");
                holder.statusTXT.setBackgroundResource(R.drawable.bg_toast_success);
                break;
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.descTXT)     TextView descTXT;
        @BindView(R.id.priceTXT)     TextView priceTXT;
        @BindView(R.id.statusTXT)     TextView statusTXT;
        @BindView(R.id.resourceIMG) ImageView resourceIMG;
        @BindView(R.id.adapterCON)  View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public RedeemedModel getItem() {
            return (RedeemedModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if(clickListener != null){
                    clickListener.onItemClick((RedeemedModel) v.getTag());
                }
                break;
        }
    }

    public interface ClickListener{
        void onItemClick(RedeemedModel resourcesModel);
    }
} 
