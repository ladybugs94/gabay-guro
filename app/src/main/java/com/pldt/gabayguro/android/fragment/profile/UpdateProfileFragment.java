package com.pldt.gabayguro.android.fragment.profile;

import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.android.dialog.CalendarDialog;
import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ImageManager;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;
import com.pldt.gabayguro.vendor.server.util.ErrorResponseManger;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Evanson on 2019-10-02.
 */

public class UpdateProfileFragment extends BaseFragment implements  BSImagePicker.OnSingleImageSelectedListener, ImageManager.Callback, CalendarDialog.DateTimePickerListener {
    public static final String TAG = UpdateProfileFragment.class.getName();


    @BindView(R.id.fnameET)             EditText fnameET;
    @BindView(R.id.lnameET)             EditText lnameET;
    @BindView(R.id.mnameET)             EditText mnameET;
    @BindView(R.id.bioET)               EditText bioET;
    @BindView(R.id.phoneTXT)            TextView phoneTXT;
    @BindView(R.id.changeBTN)            TextView changeBTN;
    @BindView(R.id.dobTXT)              TextView dobTXT;
    @BindView(R.id.emailET)             EditText emailET;
    @BindView(R.id.usernameET)          EditText usernameET;
    @BindView(R.id.profileIMG)          ImageView profileIMG;
    @BindView(R.id.spinner)             Spinner spinner;

    public static UpdateProfileFragment newInstance() {
        UpdateProfileFragment fragment = new UpdateProfileFragment();

        return fragment;
    }

    private ProfileActivity profileActivity;


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_profile;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        profileActivity.setTitle("Update Profile");
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(profileActivity,
                R.array.ticket_type, R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (UserData.getUserModel().contactNumberStatus.equalsIgnoreCase("unverified")){
            changeBTN.setText("Verify");
        }else{
            changeBTN.setText("Change");
        }
    }

    private String getType() {
        String value ;
        switch (spinner.getSelectedItem().toString()){
            case "Jr/Junior":
                value = "JR";
                break;
            case "Sr/Senior":
                value = "SR";
                break;
            case "I":
                value = "I";
                break;
            case "II":
                value = "II";
                break;
            case "III":
                value = "III";
                break;
            case "IV":
                value = "IV";
                break;
            case "V":
                value = "V";
                break;
            case "VI":
                value = "VI";
                break;
            case "VII":
                value = "VII";
                break;
            case "VIII":
                value = "VIII";
                break;
            case "IX":
                value = "IX";
                break;
            case "X":
                value = "X";
                break;
            default:
                value = "";
                break;
        }
        return value;
    }

    public void suffixDisplay(String suffix){
        switch (suffix){
            case "JR":
                spinner.setSelection(1);
                break;
            case "SR":
                spinner.setSelection(2);
                break;
            case "I":
                spinner.setSelection(3);
                break;
            case "II":
                spinner.setSelection(4);
                break;
            case "III":
                spinner.setSelection(5);
                break;
            case "IV":
                spinner.setSelection(6);
                break;
            case "V":
                spinner.setSelection(7);
                break;
            case "VI":
                spinner.setSelection(8);
                break;
            case "VII":
                spinner.setSelection(9);
                break;
            case "VIII":
                spinner.setSelection(10);
                break;
            case "IX":
                spinner.setSelection(11);
                break;
            case "X":
                spinner.setSelection(12);
                break;
            default:
                spinner.setSelection(0);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();showProfile();
    }

    @OnClick(R.id.profileIMG)
    void profileIMGClicked(){
        openFileChooser();
    }

    @OnClick(R.id.changeBTN)
    void changeBTNClicked(){
        profileActivity.openOTPOneFragment();
    }

    @OnClick(R.id.submitBTN)
    void submitBTN(){
       APIRequest apiRequest = Auth.getDefault().updateProfile(getContext());
       apiRequest.addParameter(Keys.FNAME, fnameET.getText().toString().trim())
               .addParameter(Keys.LNAME, lnameET.getText().toString().trim())
//               .addParameter(Keys.USERNAME, usernameET.getText().toString().trim())
               .addParameter(Keys.EMAIL, emailET.getText().toString().trim())
               .addParameter(Keys.SUFFIX, getType())
               .addParameter(Keys.BIO, bioET.getText().toString().trim())
               .addParameter(Keys.MNAME, mnameET.getText().toString().trim())
               .addParameter(Keys.BIRTHDATE, dobTXT.getText().toString().trim());
       apiRequest.execute();
    }

    @OnClick(R.id.dobTXT)
    void setDobTXT(){
        CalendarDialog.newInstance(this, dobTXT.getText().toString()).show(getChildFragmentManager(), TAG);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void showProfile(){
        Auth.getDefault().myProfile(getContext(), null);
    }

    @Subscribe
    public void onResponse(Auth.MyProfileResponse response){
        SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status){
            fnameET.setText(singleTransformer.data.fname);
            lnameET.setText(singleTransformer.data.lname);
            mnameET.setText(singleTransformer.data.mname);
            emailET.setText(singleTransformer.data.email);
            usernameET.setText(singleTransformer.data.username);
            phoneTXT.setText(singleTransformer.data.contactNumber);
            bioET.setText(singleTransformer.data.bio);
            dobTXT.setText(singleTransformer.data.birthdate);
            suffixDisplay(singleTransformer.data.suffix);
            if (!singleTransformer.data.avatar.fullPath.isEmpty()){
                Picasso.with(getContext()).load(singleTransformer.data.avatar.fullPath).into(profileIMG);
            }

        } else {
            ToastMessage.show(getContext(),singleTransformer.msg, ToastMessage.Status.FAILED);

        }
    }

    @Subscribe
    public void onResponse(Auth.UpdateProfileResponse registerResponse){
        SingleTransformer<UserModel> baseTransformer = registerResponse.getData(SingleTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.insert(baseTransformer.data);
            showProfile();
        }else{
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            if(baseTransformer.hasRequirements()){
                ErrorResponseManger.first(emailET, baseTransformer.errors.email);
                ErrorResponseManger.first(lnameET, baseTransformer.errors.lname);
                ErrorResponseManger.first(fnameET, baseTransformer.errors.fname);
                ErrorResponseManger.first(dobTXT, baseTransformer.errors.birthdate);
            }
        }
    }

    private void openFileChooser() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.pldt.gabayguro.fileprovider").build();
        pickerDialog.show(getChildFragmentManager(), "picker");

    }


    @Override
    public void onSingleImageSelected(Uri uri, String tag) {

        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
            int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()));
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
            ImageManager.getFileFromBitmap(getContext(), scaled, "image1", this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void success(File file) {
        Auth.getDefault().update_avatar(getContext(),file);
    }

    @Subscribe
    public void onResponse(Auth.UpdateAvatarResponse response){
        SingleTransformer<UserModel> singleTransformer = response.getData(SingleTransformer.class);
        if(singleTransformer.status){
            UserData.insert(singleTransformer.data);
            showProfile();
            ToastMessage.show(getContext(),singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            ToastMessage.show(getContext(),singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void forDisplay(String date) {
        dobTXT.setText(date);
    }
}