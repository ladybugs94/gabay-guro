package com.pldt.gabayguro.android.fragment.register;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.RegisterActivity;
import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.util.ErrorResponseManger;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SignUpFragment extends BaseFragment {
    public static final String TAG = SignUpFragment.class.getName();

    private RegisterActivity registerActivity;
    private static final String CONTACT_FORMAT = "+63 [000][000][0000]";


    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    private FirebaseAnalytics firebaseAnalytics;
    private APIRequest apiRequest;

    @BindView(R.id.signUpBTN)                       TextView signUpBTN;
    @BindView(R.id.fnameET)                         EditText fnameET;
    @BindView(R.id.lnameET)                         EditText lnameET;
    @BindView(R.id.emailET)                         EditText emailET;
    @BindView(R.id.phoneET)                         EditText phoneET;
    @BindView(R.id.passET)                          EditText passET;
    @BindView(R.id.usernameET)                          EditText usernameET;
    @BindView(R.id.confirmpassET)                   EditText confirmpassET;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_signup;
    }

    @Override
    public void onViewReady() {
        registerActivity = (RegisterActivity) getContext();
        firebaseAnalytics = FirebaseAnalytics.getInstance(registerActivity);
        onMaskedTextChangedListener(phoneET);
    }

    public void onMaskedTextChangedListener(EditText editText){

        MaskedTextChangedListener maskedTextChangedListener = new MaskedTextChangedListener(
                CONTACT_FORMAT,
                true,
                editText,
                null,
                null);

        editText.addTextChangedListener(maskedTextChangedListener);
        editText.setOnFocusChangeListener(maskedTextChangedListener);
    }

    @OnClick(R.id.signUpBTN)
    void onClick(){
        preReg();
    }

    @OnClick(R.id.loginBTN)
    void loginBTN(){
        registerActivity.startLandingActivity("");
    }

    @Override
    public void onResume() {
        super.onResume();
        registerActivity.setTitle("Sign Up");
    }

    public void preReg(){
        String fname = fnameET.getText().toString().trim();
        String lname = lnameET.getText().toString().trim();
        String phone = phoneET.getText().toString().trim();
        String user = usernameET.getText().toString().trim();
        String email = emailET.getText().toString().trim();
        String pass = passET.getText().toString().trim();
        String confirmpass = confirmpassET.getText().toString().trim();

        apiRequest = Auth.getDefault().register(getContext())
                .addParameter(Keys.FNAME, fname)
                .addParameter(Keys.LNAME, lname)
                .addParameter(Keys.PHONE_NUMBER, phone)
                .addParameter(Keys.USERNAME, user)
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.PASSWORD, pass)
                .addParameter(Keys.PASSWORD_CONFIRM, confirmpass);
        apiRequest.execute();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.RegisterResponse loginResponse){
        BaseTransformer baseTransformer = loginResponse.getData(BaseTransformer.class);
        if (baseTransformer.status){
            Bundle bundle = new Bundle();
            bundle.putString("FirstName", fnameET.getText().toString().trim());
            bundle.putString("LastName", lnameET.getText().toString().trim());
            bundle.putString("Email", emailET.getText().toString().trim());
            bundle.putString("Phone", phoneET.getText().toString().trim());
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
            registerActivity.startLandingActivity("");
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
        }else{
            if(baseTransformer.hasRequirements()){
                ErrorResponseManger.first(emailET, baseTransformer.errors.email);
                ErrorResponseManger.first(passET, baseTransformer.errors.password);
                ErrorResponseManger.first(confirmpassET, baseTransformer.errors.passwordConfirmation);
                ErrorResponseManger.first(lnameET, baseTransformer.errors.lname);
                ErrorResponseManger.first(fnameET, baseTransformer.errors.fname);
                ErrorResponseManger.first(phoneET, baseTransformer.errors.contactNumber);
            }
        }
    }


}
