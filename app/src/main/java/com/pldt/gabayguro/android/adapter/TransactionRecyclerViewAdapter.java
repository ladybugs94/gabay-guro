package com.pldt.gabayguro.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.TransactionModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;


public class TransactionRecyclerViewAdapter extends BaseRecylerViewAdapter<TransactionRecyclerViewAdapter.ViewHolder, TransactionModel> {

    private ClickListener clickListener;

    public TransactionRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_transaction));
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.titleTXT.setText(holder.getItem().amountDisplay);
        holder.dateTXT.setText(holder.getItem().dateCreated.timePassed);
        holder.codeTXT.setText(holder.getItem().referenceTypeDisplay);
        holder.contentTXT.setText("Remarks: " + holder.getItem().remarks);

        if (holder.getItem().type.equalsIgnoreCase("debit")){
            holder.titleTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.failed));
        }else{
            holder.titleTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)              View adapterCON;
        @BindView(R.id.titleTXT)                 TextView titleTXT;
        @BindView(R.id.contentTXT)              TextView contentTXT;
        @BindView(R.id.dateTXT)                 TextView dateTXT;
        @BindView(R.id.codeTXT)                 TextView codeTXT;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(TransactionRecyclerViewAdapter.this);

        }

        public TransactionModel getItem() {
            return (TransactionModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((TransactionModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(TransactionModel sampleModel);
    }
}
