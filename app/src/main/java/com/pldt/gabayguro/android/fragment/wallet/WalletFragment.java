package com.pldt.gabayguro.android.fragment.wallet;

import android.util.Log;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.WalletActivity;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class WalletFragment extends BaseFragment {
    public static final String TAG = WalletFragment.class.getName();

    public static WalletFragment newInstance() {
        WalletFragment fragment = new WalletFragment();
        return fragment;
    }

    private WalletActivity walletActivity;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_wallet;
    }

    @Override
    public void onViewReady() {
        walletActivity = (WalletActivity) getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        walletActivity.setTitle("Wallet");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
