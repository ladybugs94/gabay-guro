package com.pldt.gabayguro.android.fragment.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;
import com.pldt.gabayguro.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class UpdatePasswordFragment extends BaseFragment {
    public static final String TAG = UpdatePasswordFragment.class.getName();

    public static UpdatePasswordFragment newInstance() {
        UpdatePasswordFragment fragment = new UpdatePasswordFragment();
        return fragment;
    }

    private ProfileActivity profileActivity;

    @BindView(R.id.passwordET)          EditText passwordET;
    @BindView(R.id.currentPassET)       EditText currentPassET;
    @BindView(R.id.passConET)           EditText passConET;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_update_pass;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        profileActivity.setTitle("Change Password");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.UpdatePassResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
            UserData.insert(userTransformer.data);
            profileActivity.startMainActivity("home");
            ToastMessage.show(getContext(),userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            ToastMessage.show(getContext(),userTransformer.msg, ToastMessage.Status.FAILED);
            if(userTransformer.hasRequirements()){
                if (!ErrorResponseManger.first(userTransformer.errors.currentPassword).equals("")) {
                    currentPassET.setError(ErrorResponseManger.first(userTransformer.errors.currentPassword));
                }
                if (!ErrorResponseManger.first(userTransformer.errors.password).equals("")) {
                    passwordET.setError(ErrorResponseManger.first(userTransformer.errors.password));
                }
                if (!ErrorResponseManger.first(userTransformer.errors.passwordConfirmation).equals("")) {
                    passConET.setError(ErrorResponseManger.first(userTransformer.errors.passwordConfirmation));
                }
            }
        }
    }

    @OnClick(R.id.updateBTN)
    void updateBTN(){
        showLogoutAlert();
    }

    public void showLogoutAlert(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you sure you want to update password?");
        builder1.setTitle("Update Password");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Auth.getDefault().update_pass(getContext(), currentPassET.getText().toString(), passwordET.getText().toString(), passConET.getText().toString());
                    }
                });
        builder1.setNegativeButton(
                "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
