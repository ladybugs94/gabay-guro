package com.pldt.gabayguro.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.NotifModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;


public class NotifRecyclerViewAdapter extends BaseRecylerViewAdapter<NotifRecyclerViewAdapter.ViewHolder, NotifModel> {

    private ClickListener clickListener;

    public NotifRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_notification));
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.titleTXT.setText(holder.getItem().title);
        holder.dateTXT.setText(holder.getItem().dateCreated.timePassed);
        holder.contentTXT.setText(holder.getItem().content);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)              View adapterCON;
        @BindView(R.id.titleTXT)                 TextView titleTXT;
        @BindView(R.id.contentTXT)              TextView contentTXT;
        @BindView(R.id.dateTXT)                 TextView dateTXT;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(NotifRecyclerViewAdapter.this);

        }

        public NotifModel getItem() {
            return (NotifModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((NotifModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(NotifModel sampleModel);
    }
}
