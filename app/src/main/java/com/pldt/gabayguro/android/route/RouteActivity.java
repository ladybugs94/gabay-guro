package com.pldt.gabayguro.android.route;

import android.content.Intent;
import android.os.Bundle;

import com.pldt.gabayguro.android.activity.AnnouncementsActivity;
import com.pldt.gabayguro.android.activity.ComingSoonActivity;
import com.pldt.gabayguro.android.activity.CommunityActivity;
import com.pldt.gabayguro.android.activity.LandingActivity;
import com.pldt.gabayguro.android.activity.MainActivity;
import com.pldt.gabayguro.android.activity.MessageActivity;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.android.activity.RegisterActivity;
import com.pldt.gabayguro.android.activity.ResourcesActivity;
import com.pldt.gabayguro.android.activity.WalletActivity;
import com.pldt.gabayguro.vendor.android.base.BaseActivity;
import com.pldt.gabayguro.vendor.android.base.RouteManager;

/**
 * Created by Labyalo on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

    public void startMainActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
    }

    public void startRegisterActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(RegisterActivity.class)
                .addActivityTag("register")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startLandingActivity(String fragmentTAG){
        RouteManager.Route.with(this)
                .addActivityClass(LandingActivity.class)
                .addActivityTag("landing")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    public void startProfileActivity(String fragmentTAG, String from){
        Bundle bundle = new Bundle();
        bundle.putString("from", from);
        RouteManager.Route.with(this)
                .addActivityClass(ProfileActivity.class)
                .addActivityTag("profile")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startWalletActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(WalletActivity.class)
                .addActivityTag("wallet")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startComingSoonActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(ComingSoonActivity.class)
                .addActivityTag("coming")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startResourcesActivity(String fragmentTAG){
        Bundle bundle = new Bundle();
        RouteManager.Route.with(this)
                .addActivityClass(ResourcesActivity.class)
                .addActivityTag("resources")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startCoursesActivity(String fragmentTAG, int id){
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        RouteManager.Route.with(this)
                .addActivityClass(CommunityActivity.class)
                .addActivityTag("community")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startAnnouncementsActivity(String fragmentTAG, int id){
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        RouteManager.Route.with(this)
                .addActivityClass(AnnouncementsActivity.class)
                .addActivityTag("announcements")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startMessageActivity(String fragmentTAG, int id, int userID, String avatarURL, String chatName){
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        bundle.putInt("userID", userID);
        bundle.putString("avatarURL", avatarURL);
        bundle.putString("chatName", chatName);
        RouteManager.Route.with(this)
                .addActivityClass(MessageActivity.class)
                .addActivityTag("message")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }


//    @Subscribe
//    public void invalidToken(InvalidToken invalidToken){
//        Log.e("Token", "Expired");
//        EventBus.getDefault().unregister(this);
//        UserData.insert(new UserModel());
//        UserData.insert(UserData.TOKEN_EXPIRED, true);
//        startLandingActivity();
//    }
}
