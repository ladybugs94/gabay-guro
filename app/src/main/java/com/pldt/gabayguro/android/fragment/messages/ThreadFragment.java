package com.pldt.gabayguro.android.fragment.messages;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.google.gson.Gson;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.MessageActivity;
import com.pldt.gabayguro.android.adapter.InboxRecyclerViewAdapter;
import com.pldt.gabayguro.android.dialog.ViewImageDialog;
import com.pldt.gabayguro.data.model.api.ChatThreadModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Chat;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.DownloadFileManager;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.android.java.ImageManager;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.android.pusher.Cred;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;
import dk.nodes.filepicker.FilePickerActivity;
import dk.nodes.filepicker.FilePickerConstants;
import dk.nodes.filepicker.uriHelper.FilePickerUriHelper;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ThreadFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener, InboxRecyclerViewAdapter.ClickListener,
        EndlessRecyclerViewScrollListener.Callback, ImageManager.Callback, DownloadFileManager.DownloadCallback{
    public static final String TAG = ThreadFragment.class.getName();

    private MessageActivity messageActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private InboxRecyclerViewAdapter inboxRecyclerViewAdapter;
    private APIRequest apiRequest;
    private Pusher pusher;
    private static final int PICK_FILE_RESULT_CODE = 1;
    private DownloadFileManager downloadFileManager;

    private int chat_id;
    private int userid;
    private String avatarURL;
    private String chatName;

    public static ThreadFragment newInstance() {
        ThreadFragment fragment = new ThreadFragment();
        return fragment;
    }

    @BindView(R.id.defaultRV)                   RecyclerView defaultRV;
    @BindView(R.id.progressBar)                 ProgressBar progressBar;
    @BindView(R.id.messageET)                   EditText messageET;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_chat_thread;
    }

    @Override
    public void onViewReady() {
        messageActivity = (MessageActivity) getContext();
        setUpListView();
        chat_id = messageActivity.getFragmentBundle().getInt("id");
        userid = messageActivity.getFragmentBundle().getInt("userID");
        avatarURL = messageActivity.getFragmentBundle().getString("avatarURL");
        chatName = messageActivity.getFragmentBundle().getString("chatName");
        messageActivity.setTitle(chatName);
        messageActivity.setAvatar(avatarURL);
        setupPusher("chat."+chat_id);
        downloadFileManager = DownloadFileManager.newInstance(getContext(), null, this);
    }


    private void setUpListView() {
        inboxRecyclerViewAdapter = new InboxRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        defaultRV.setLayoutManager(linearLayoutManager);
        defaultRV.setAdapter(inboxRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        defaultRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        inboxRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }


    private void refreshList() {
        apiRequest = Chat.getDefault().chatThread(getContext(), chat_id);
        apiRequest.first();
    }

    public void attemptSend() {
        if (messageET.getText().toString().length() != 0) {
            if (chat_id == 0){
                Chat.getDefault().createChat(getContext(), userid, messageET.getText().toString(), null);
            }else{
                Chat.getDefault().createMessage(getContext(), chat_id, messageET.getText().toString(), null);
            }
        }
    }

    @Subscribe
    public void onResponse(Chat.CreateMessageResponse responseData) {
        SingleTransformer<ChatThreadModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if (singleTransformer.status) {
            inboxRecyclerViewAdapter.newMessage(singleTransformer.data);
            defaultRV.smoothScrollToPosition(0);
            messageET.setText("");
        } else {
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Chat.CreateMessageUserResponse responseData) {
        SingleTransformer<ChatThreadModel> singleTransformer = responseData.getData(SingleTransformer.class);
        if (singleTransformer.status) {
            apiRequest = Chat.getDefault().chatThread(getContext(), singleTransformer.data.conversationId);
            apiRequest.first();
            chat_id = singleTransformer.data.conversationId;
            messageET.setText("");
        } else {
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Chat.ChatThreadResponse collectionResponse) {
        CollectionTransformer<ChatThreadModel> collectionTransformer = collectionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (collectionResponse.isNext()) {
                inboxRecyclerViewAdapter.addNewData(collectionTransformer.data);
            } else {
                inboxRecyclerViewAdapter.setNewData(collectionTransformer.data);
                endlessRecyclerViewScrollListener.reset();
            }
            progressBar.setVisibility(View.GONE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onResponse(Chat.RemoveMessageResponse responseData) {
        BaseTransformer singleTransformer = responseData.getData(BaseTransformer.class);
        if(singleTransformer.status){
            refreshList();
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    private void openFilePicker(){
        Intent intent = new Intent(getContext(), FilePickerActivity.class);
        intent.putExtra(FilePickerConstants.FILE, true);
        intent.putExtra(FilePickerConstants.MULTIPLE_TYPES, new String[]{
                FilePickerConstants.MIME_IMAGE,
                FilePickerConstants.MIME_VIDEO,
                FilePickerConstants.MIME_PDF,
                FilePickerConstants.MIME_TEXT_PLAIN
        });

        startActivityForResult(intent, PICK_FILE_RESULT_CODE);
    }

    @Override
    public  void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case PICK_FILE_RESULT_CODE:
                if(resultCode == Activity.RESULT_OK){
                    defaultRV.smoothScrollToPosition(0);
                    File file = FilePickerUriHelper.getFile(getContext(), data);
                    if (chat_id == 0){
                        Chat.getDefault().createChat(getContext(), userid, file.getName(), file);
                    }else{
                        Chat.getDefault().createMessage(getContext(), chat_id, file.getName(), file);
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        downloadFileManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void openImageChooser() {
        BSImagePicker pickerDialog = new BSImagePicker.Builder("com.pldt.gabayguro").build();
        pickerDialog.show(getChildFragmentManager(), "picker");

    }

    private void setupPusher(String channelName){
        PusherOptions options = new PusherOptions();
        options.setCluster(Cred.CLUSTER);
        pusher = new Pusher(Cred.KEY, options);
        Channel channel = pusher.subscribe(channelName);

        channel.bind("new_message", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                processPusher(data,eventName);
            }
        });

        channel.bind("deleted_message", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                processPusher(data,eventName);
            }
        });

        pusher.connect();
    }

    private void processPusher(final String data, final String eventName){
        final ChatThreadModel chatThreadModel = new Gson().fromJson(data, ChatThreadModel.class);

        messageActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch(eventName){
                    case "deleted_message":
                    case "updated_message":
                        inboxRecyclerViewAdapter.updateMessage(chatThreadModel);

                        break;
                    default:
                        if(chatThreadModel.sender.data.userId != UserData.getUserModel().userId){
                            inboxRecyclerViewAdapter.newMessage(chatThreadModel);
                            Log.d("FULLPATH", chatThreadModel.attachment.fullPath);
                        }
                }

                if (defaultRV != null){
                    defaultRV.smoothScrollToPosition(0);
                }
            }
        });
        Log.e(TAG, "PUSHER TO" + eventName);
    }

    @OnClick(R.id.imageBTN)
    void imageBTNClicked(){
        openImageChooser();
    }

    @OnClick(R.id.fileBTN)
    void fileBTNClicked(){
        openFilePicker();
    }

    @OnClick(R.id.sendBTN)
    void sendBTNClicked(){
        attemptSend();
    }


    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ImageManager.getFileFromBitmap(getContext(), bitmap, "image1", this);
    }

    @Override
    public void success(File file) {
        if (chat_id == 0){
            Chat.getDefault().createChat(getContext(), userid, file.getName(), file);
        }else{
            Chat.getDefault().createMessage(getContext(), chat_id, file.getName(), file);
        }
    }

    @Override
    public void onItemClick(ChatThreadModel sampleModel) {
        ToastMessage.show(getContext(), sampleModel.content, ToastMessage.Status.SUCCESS);
    }

    @Override
    public void onDeleteClick(ChatThreadModel sampleModel) {
        Chat.getDefault().removeMessage(getContext(), sampleModel.id);
    }

    @Override
    public void onDeleteAllClick(ChatThreadModel sampleModel) {
        Chat.getDefault().removeMessageAll(getContext(), sampleModel.id);
    }

    @Override
    public void onFileClick(ChatThreadModel sampleModel) {
//        downloadFileManager.setFileName(sampleModel.content);
//        downloadFileManager.downloadWithPermissionGranted(sampleModel.attachment.fullPath);
//        Intent intent = new Intent();
//        intent.setDataAndType(Uri.parse(sampleModel.attachment.fullPath), "application/pdf");
//        startActivity(intent);
        openfile(sampleModel.attachment.fullPath);
    }

    public void openfile(String url){
        Uri uri = Uri.parse(url);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (url.contains(".doc") || url.contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if(url.contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if(url.contains(".ppt") || url.contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if(url.contains(".xls") || url.contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if(url.contains(".zip") || url.contains(".rar")) {
            // Zip Rar file
            intent.setDataAndType(uri, "application/zip");
        } else if(url.contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if(url.contains(".wav") || url.contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if(url.contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if(url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
            // JPG file
//            intent.setDataAndType(uri, "image/jpeg");
            ViewImageDialog.newInstance(url,url).show(getChildFragmentManager(), TAG);
        } else if(url.contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if(url.contains(".3gp") || url.contains(".mpg") || url.contains(".mpeg") || url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            intent.setDataAndType(uri, "*/*");
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    @Override
    public void onImageClick(ChatThreadModel sampleModel) {
        ViewImageDialog.newInstance(sampleModel.attachment.fullPath, sampleModel.content).show(getFragmentManager(), ViewImageDialog.TAG);
    }

    @Override
    public void onAvatarClick(ChatThreadModel sampleModel) {

    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.setPage(page+1);
        apiRequest.nextPage();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFileDownloadCompleted(long id) {
        ToastMessage.show(getContext(), "Download Successfully!", ToastMessage.Status.SUCCESS);
    }
}
