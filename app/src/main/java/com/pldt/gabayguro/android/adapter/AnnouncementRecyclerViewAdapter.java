package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.AnnouncementsModel;
import com.pldt.gabayguro.data.model.api.ChatModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AnnouncementRecyclerViewAdapter extends BaseRecylerViewAdapter<AnnouncementRecyclerViewAdapter.ViewHolder, AnnouncementsModel>{

    private ClickListener clickListener;

    public AnnouncementRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_announcements));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().title);
        holder.descTXT.setText(holder.getItem().content);
        holder.dateTXT.setText(holder.getItem().dateCreated.timePassed);

        Picasso.with(getContext()).load(holder.getItem().thumbnail.fullPath).fit().centerCrop().placeholder(R.drawable.gabay_guro_logo_fav).error(R.drawable.gabay_guro_logo_fav).into(holder.imageIV);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.descTXT)     TextView descTXT;
        @BindView(R.id.dateTXT)     TextView dateTXT;
        @BindView(R.id.imageIV)     ImageView imageIV;
        @BindView(R.id.adapterCON)  View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public AnnouncementsModel getItem() {
            return (AnnouncementsModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((AnnouncementsModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(AnnouncementsModel sampleModel);
    }
} 
