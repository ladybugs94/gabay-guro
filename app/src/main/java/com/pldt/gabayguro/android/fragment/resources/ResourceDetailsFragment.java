package com.pldt.gabayguro.android.fragment.resources;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ResourcesActivity;
import com.pldt.gabayguro.data.model.api.ResourcesModel;
import com.pldt.gabayguro.server.request.Announcements;
import com.pldt.gabayguro.server.request.Resources;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;


public class ResourceDetailsFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = ResourceDetailsFragment.class.getName();

    private int id;

    public static ResourceDetailsFragment newInstance(int id) {
        ResourceDetailsFragment fragment = new ResourceDetailsFragment();
        fragment.id = id;
        return fragment;
    }
    private ResourcesActivity resourcesActivity;

    @BindView(R.id.articleIV)               ImageView articleIV;
    @BindView(R.id.articleTitleTXT)         TextView articleTitleTXT;
    @BindView(R.id.articleDetails)          TextView articleDetails;
    @BindView(R.id.redeemBTN)          TextView redeemBTN;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_resource_details;
    }

    @Override
    public void onViewReady() {
        resourcesActivity = (ResourcesActivity) getContext();
        resourcesActivity.setTitle("Details");
        Resources.getDefault().details(getContext(), id);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Resources.DetailsResponse response) {
        SingleTransformer<ResourcesModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status) {

            articleTitleTXT.setText(singleTransformer.data.productName);
            articleDetails.setText(singleTransformer.data.content);

            Glide.with(getContext())
                    .load(singleTransformer.data.thumbnail.fullPath)
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.color.colorPrimary)
                            .error(R.color.colorPrimary)
                            .dontAnimate()
                            .skipMemoryCache(true)
                            .dontTransform()
                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                    .into(articleIV);

            redeemBTN.setText("REDEEM (" + singleTransformer.data.priceDisplay + " points)");
            //image

        } else {
            Toast.makeText(getContext(), singleTransformer.msg, Toast.LENGTH_SHORT).show();
        }

    }

    @Subscribe
    public void onResponse(Resources.RedeemResponse response) {
        BaseTransformer singleTransformer = response.getData(BaseTransformer.class);
        if (singleTransformer.status) {
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
            resourcesActivity.startResourcesActivity("redeemed");
        } else {
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }

    }

    @OnClick(R.id.redeemBTN)
    void onRedeem(){
        Resources.getDefault().redeem(getContext(), id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

}
