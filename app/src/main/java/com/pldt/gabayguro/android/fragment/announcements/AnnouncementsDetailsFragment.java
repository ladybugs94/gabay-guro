package com.pldt.gabayguro.android.fragment.announcements;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.AnnouncementsActivity;
import com.pldt.gabayguro.data.model.api.AnnouncementsModel;
import com.pldt.gabayguro.server.request.Announcements;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;


public class AnnouncementsDetailsFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = AnnouncementsDetailsFragment.class.getName();

    private int id;

    public static AnnouncementsDetailsFragment newInstance(int id) {
        AnnouncementsDetailsFragment fragment = new AnnouncementsDetailsFragment();
        fragment.id = id;
        return fragment;
    }
    private AnnouncementsActivity mainActivity;

    @BindView(R.id.articleIV)               ImageView articleIV;
    @BindView(R.id.articleTitleTXT)         TextView articleTitleTXT;
    @BindView(R.id.articleDetails)          TextView articleDetails;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_article_details;
    }

    @Override
    public void onViewReady() {
        mainActivity = (AnnouncementsActivity) getContext();
        mainActivity.setTitle("Details");
        Announcements.getDefault().announcements(getContext(), id);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Announcements.AnnouncementsResponse response) {
        SingleTransformer<AnnouncementsModel> singleTransformer = response.getData(SingleTransformer.class);
        if (singleTransformer.status) {

            articleTitleTXT.setText(singleTransformer.data.title);
            articleDetails.setText(singleTransformer.data.content);

            Glide.with(getContext())
                    .load(singleTransformer.data.thumbnail.fullPath)
                    .apply(new RequestOptions()
                            .centerCrop()
                            .placeholder(R.color.colorPrimary)
                            .error(R.color.colorPrimary)
                            .dontAnimate()
                            .skipMemoryCache(true)
                            .dontTransform()
                            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC))
                    .into(articleIV);

            //image

        } else {
            Toast.makeText(getContext(), singleTransformer.msg, Toast.LENGTH_SHORT).show();
        }


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

}
