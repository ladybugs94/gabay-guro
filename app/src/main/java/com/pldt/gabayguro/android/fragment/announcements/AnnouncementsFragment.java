package com.pldt.gabayguro.android.fragment.announcements;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.AnnouncementsActivity;
import com.pldt.gabayguro.android.adapter.AnnouncementRecyclerViewAdapter;
import com.pldt.gabayguro.android.dialog.WebViewDialog;
import com.pldt.gabayguro.data.model.api.AnnouncementsModel;
import com.pldt.gabayguro.server.request.Announcements;
import com.pldt.gabayguro.server.request.Chat;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class AnnouncementsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener
        , AnnouncementRecyclerViewAdapter.ClickListener
        , EndlessRecyclerViewScrollListener.Callback {
    public static final String TAG = AnnouncementsFragment.class.getName();

    public static AnnouncementsFragment newInstance() {
        AnnouncementsFragment fragment = new AnnouncementsFragment();
        return fragment;
    }

    private AnnouncementsActivity mainActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private AnnouncementRecyclerViewAdapter announcementRecyclerViewAdapter;
    private APIRequest apiRequest;

    @BindView(R.id.chatRV)                      RecyclerView chatRV;
    @BindView(R.id.chatSRL)                     SwipeRefreshLayout chatSRL;
    @BindView(R.id.shimmerFL)                   ShimmerFrameLayout shimmerFL;
    @BindView(R.id.noMessageTXT)                TextView noMessageTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_announcements;
    }

    @Override
    public void onViewReady() {
        mainActivity = (AnnouncementsActivity) getContext();
        mainActivity.setTitle("Announcements");
        setUpRView();
        chatSRL.setOnRefreshListener(this);
    }

    private void setUpRView(){
        announcementRecyclerViewAdapter = new AnnouncementRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        chatRV.setLayoutManager(linearLayoutManager);
        chatRV.setAdapter(announcementRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        chatRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        announcementRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Announcements.AllAnnouncementsResponse colletionResponse) {
        CollectionTransformer<AnnouncementsModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            shimmerFL.setVisibility(View.GONE);
            if (colletionResponse.isNext()){
                announcementRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                endlessRecyclerViewScrollListener.reset();
                announcementRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }

            if (announcementRecyclerViewAdapter.getItemCount() == 0){
                noMessageTXT.setVisibility(View.VISIBLE);
            } else {
                noMessageTXT.setVisibility(View.GONE);
            }
//            groupChatLL.setVisibility(announcementRecyclerViewAdapter.getData().size() > 0 ? View.GONE : View.VISIBLE);
        }

    }



    public void refreshList(){
        apiRequest = Announcements.getDefault().allAnnouncements(getContext(), chatSRL);
        apiRequest.setPerPage(10);
        apiRequest.first();

    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(AnnouncementsModel sampleModel) {
        if (sampleModel.webUrl.isEmpty()){
            mainActivity.openDetailsFragment(sampleModel.announcementId);
        }else{
            WebViewDialog.newInstance(sampleModel.title, sampleModel.webUrl).show(getChildFragmentManager(), TAG);
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.setPage(page+1);
        apiRequest.nextPage();
    }
}
