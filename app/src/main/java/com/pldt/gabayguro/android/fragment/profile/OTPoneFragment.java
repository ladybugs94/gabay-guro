package com.pldt.gabayguro.android.fragment.profile;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.util.ErrorResponseManger;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class OTPoneFragment extends BaseFragment {
    public static final String TAG = OTPoneFragment.class.getName();

    public static OTPoneFragment newInstance() {
        OTPoneFragment fragment = new OTPoneFragment();
        return fragment;
    }

    private ProfileActivity profileActivity;
    private FirebaseAnalytics firebaseAnalytics;
    private static final String CONTACT_FORMAT = "+63 [000] [000] [0000]";

    @BindView(R.id.phoneET)             EditText phoneET;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_otp_one;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        profileActivity.setTitle("");
        phoneET.setText(UserData.getUserModel().contactNumber);
        firebaseAnalytics = FirebaseAnalytics.getInstance(profileActivity);
        onMaskedTextChangedListener(phoneET);
    }

    public void onMaskedTextChangedListener(EditText editText){

        MaskedTextChangedListener maskedTextChangedListener = new MaskedTextChangedListener(
                CONTACT_FORMAT,
                true,
                editText,
                null,
                null);

        editText.addTextChangedListener(maskedTextChangedListener);
        editText.setOnFocusChangeListener(maskedTextChangedListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.OTPResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);

            Bundle bundle = new Bundle();
            bundle.putString("Phone", phoneET.getText().toString().trim());
            firebaseAnalytics.logEvent("OTP", bundle);

            profileActivity.openOTPtwoFragment(phoneET.getText().toString().trim());
        }else{
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
            if(baseTransformer.hasRequirements()){
                ErrorResponseManger.first(phoneET, baseTransformer.errors.contactNumber);
            }
        }
    }

    @OnClick(R.id.otpBTN)
    void setOTPBTN(){
        Auth.getDefault().request_otp(getContext(), phoneET.getText().toString().trim());
    }
}
