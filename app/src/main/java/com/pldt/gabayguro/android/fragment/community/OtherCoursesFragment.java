package com.pldt.gabayguro.android.fragment.community;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.CommunityActivity;
import com.pldt.gabayguro.android.adapter.CoursesRecyclerViewAdapter;
import com.pldt.gabayguro.config.Keys;
import com.pldt.gabayguro.data.model.api.CoursesModel;
import com.pldt.gabayguro.server.request.Community;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class OtherCoursesFragment extends BaseFragment implements EndlessRecyclerViewScrollListener.Callback, CoursesRecyclerViewAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = OtherCoursesFragment.class.getName();

    private CoursesRecyclerViewAdapter coursesRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private APIRequest apiRequest;
    private CommunityActivity communityActivity;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    @BindView(R.id.coursesRV)               RecyclerView coursesRV;
    @BindView(R.id.coursesSRL)              SwipeRefreshLayout coursesSRL;
    @BindView(R.id.searchET)                EditText searchET;

    public static OtherCoursesFragment newInstance() {
        OtherCoursesFragment fragment = new OtherCoursesFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_all_courses;
    }

    @Override
    public void onViewReady() {
        communityActivity = (CommunityActivity) getContext();
        communityActivity.setTitle("Recommended Courses");
        coursesSRL.setOnRefreshListener(this);
        setUpListView();
        searchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                apiRequest = Community.getDefault().allCourses(getContext(), coursesSRL);
                apiRequest.addParameter(Keys.KEYWORD, searchET.getText().toString());
                apiRequest.first();
            }
        });
    }

    private void setUpListView(){
        coursesRecyclerViewAdapter = new CoursesRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        coursesRV.setLayoutManager(linearLayoutManager);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        coursesRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        coursesRV.setAdapter(coursesRecyclerViewAdapter);
        coursesRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Subscribe
    public void onResponse(Community.AllCoursesResponse response){
        CollectionTransformer<CoursesModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            if (response.isNext()){
                coursesRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                coursesRecyclerViewAdapter.setNewData(collectionTransformer.data);
                endlessRecyclerViewScrollListener.reset();
            }
        }else{
            ToastMessage.show(getContext(), collectionTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public  void refreshList(){
        apiRequest = Community.getDefault().allCourses(getContext(), coursesSRL);
        apiRequest.first();
    }


    @Override
    public void onItemClick(CoursesModel coursesModel) {
        communityActivity.openDetailsCoursesFragment(coursesModel.courseId);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.nextPage();
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
}
