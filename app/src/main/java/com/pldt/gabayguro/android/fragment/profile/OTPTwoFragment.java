package com.pldt.gabayguro.android.fragment.profile;

import android.util.Log;
import android.widget.TextView;

//import com.mukesh.OnOtpCompletionListener;
//import com.mukesh.OtpView;
import com.chaos.view.PinView;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class OTPTwoFragment extends BaseFragment {
    public static final String TAG = OTPTwoFragment.class.getName();

    public static OTPTwoFragment newInstance(String phoneNumber) {
        OTPTwoFragment fragment = new OTPTwoFragment();
        fragment.phoneNumber = phoneNumber;
        return fragment;
    }

    @State String phoneNumber;
    @State String otpValue = "123456";

    private ProfileActivity profileActivity;

    @BindView(R.id.phoneTXT)             TextView phoneET;
    @BindView(R.id.otp_view)              PinView otp_view;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_otp_two;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        profileActivity.setTitle("");
        phoneET.setText("Enter the OTP sent to  " + phoneNumber);
//        otp_view.setOtpCompletionListener(new OnOtpCompletionListener() {
//            @Override
//            public void onOtpCompleted(String otp) {
//                otpValue = otp;
//            }
//        });
        otp_view.getText().toString();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.OTPResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            profileActivity.openOTPtwoFragment(phoneET.getText().toString().trim());
        }else{
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Subscribe
    public void onResponse(Auth.EditPhoneResponse response){
        SingleTransformer<UserModel> baseTransformer = response.getData(SingleTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
            UserData.insert(baseTransformer.data);
            if (profileActivity.getFragmentBundle().getString("from").equalsIgnoreCase("profile")){
                profileActivity.openUpdateProfileFragment();
            }else{
                profileActivity.startMainActivity("community");
            }

        }else{
            ToastMessage.show(getContext(), baseTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @OnClick(R.id.otpBTN)
    void setOTPBTN(){
        Auth.getDefault().request_otp(getContext(), phoneNumber);
    }

    @OnClick(R.id.submitBTN)
    void setsubmitBTN(){
        otpValue = otp_view.getText().toString();
        Auth.getDefault().update_phone(getContext(), phoneNumber, otpValue);
    }
}
