package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.AnnouncementsModel;
import com.pldt.gabayguro.data.model.api.QuickLinkModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class QuickLinksRecyclerViewAdapter extends BaseRecylerViewAdapter<QuickLinksRecyclerViewAdapter.ViewHolder, QuickLinkModel>{

    private ClickListener clickListener;

    public QuickLinksRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_quick_links));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.adapterCON.setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().name);
        holder.qlIMG.setImageResource(holder.getItem().img);
        if (holder.getItem().id ==1){
            holder.adapterCON.setBackgroundResource(R.drawable.bg_ripple_green_link);
            holder.nameTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        }else{
            holder.adapterCON.setBackgroundResource(R.drawable.bg_ripple_white_link);
            holder.nameTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.medium));
        }
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;
        @BindView(R.id.qlIMG)       ImageView qlIMG;
        @BindView(R.id.adapterCON)       View adapterCON;

        public ViewHolder(View view) {
            super(view);
        }

        public QuickLinkModel getItem() {
            return (QuickLinkModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((QuickLinkModel) v.getTag());
                }
                break;
        }
    }

    public interface ClickListener{
        void onItemClick(QuickLinkModel sampleModel);
    }
} 
