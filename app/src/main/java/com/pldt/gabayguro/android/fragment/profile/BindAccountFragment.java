package com.pldt.gabayguro.android.fragment.profile;

import android.util.Log;
import android.widget.TextView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.IAuthenticationResult;
import com.microsoft.identity.client.IPublicClientApplication;
import com.microsoft.identity.client.ISingleAccountPublicClientApplication;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalException;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.data.model.api.BindingModel;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class BindAccountFragment extends BaseFragment implements  ProfileActivity.FBCallback, ProfileActivity.GmailCallback, SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = BindAccountFragment.class.getName();

    private ProfileActivity profileActivity;
    private FirebaseAnalytics firebaseAnalytics;

    private final static String[] SCOPES = {"Files.Read"};
    final static String AUTHORITY = "https://login.microsoftonline.com/organizations";
    private ISingleAccountPublicClientApplication mSingleAccountApp;

    public static BindAccountFragment newInstance() {
        BindAccountFragment fragment = new BindAccountFragment();
        return fragment;
    }

    @BindView(R.id.bindSRL)             SwipeRefreshLayout bindSRL;
    @BindView(R.id.fbBTN)               TextView fbBTN;
    @BindView(R.id.gmailBTN)            TextView gmailBTN;
    @BindView(R.id.office365BTN)        TextView office365BTN;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_bind_account;
    }

    @Override
    public void onViewReady() {
        profileActivity = (ProfileActivity) getContext();
        firebaseAnalytics = FirebaseAnalytics.getInstance(profileActivity);
        initOffice365();
        bindSRL.setOnRefreshListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
        profileActivity.setTitle("Bind Account");
    }

    @Subscribe
    public void onResponse(Auth.FBResponse loginResponse){
        SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }

    }

    @Subscribe
    public void onResponse(Auth.Office365Response loginResponse){
        SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }

    }

    @Subscribe
    public void onResponse(Auth.GoogleResponse loginResponse){
        SingleTransformer<UserModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.SUCCESS);
        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }

    }

    @Subscribe
    public void onResponse(Auth.BindResponse loginResponse){
        SingleTransformer<BindingModel> singleTransformer = loginResponse.getData(SingleTransformer.class);
        if (singleTransformer.status){
            if (singleTransformer.data.facebook == 0){
                fbBTN.setText("Connect to Facebook");
            }else{
                fbBTN.setText("Account Connected");
                fbBTN.setEnabled(false);
            }

            if (singleTransformer.data.google == 0){
                gmailBTN.setText("Connect to Google");
            }else{
                gmailBTN.setText("Account Connected");
                gmailBTN.setEnabled(false);
            }

            if (singleTransformer.data.office365 == 0){
                office365BTN.setText("Connect to Office365");
            }else{
                office365BTN.setText("Account Connected");
                office365BTN.setEnabled(false);
            }

        }
        else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }

    }

    @OnClick(R.id.fbBTN)
    void signInFBBTNClicked(){
        profileActivity.attemptFBLogin(this);
    }

    @OnClick(R.id.gmailBTN)
    void signInGoogleBTNClicked(){
        profileActivity.attemptGMLogin(this);
    }

    @OnClick(R.id.office365BTN)
    void signInoffice365BTNClicked(){
        mSingleAccountApp.signIn(profileActivity, null, SCOPES, getAuthInteractiveCallback());
    }


    @Override
    public void success(UserModel userModel) {
        Auth.getDefault().connectFB(getContext(),  profileActivity.getAccessToken());
        Log.e("TokenS", ">>>" + profileActivity.getAccessToken());
    }

    @Override
    public void failed() {

    }

    @Override
    public void successGmail(UserModel userModel) {
        Auth.getDefault().connectGoogle(getContext(),  userModel.accessToken);
        Log.e("TokenS", ">>>" +userModel.accessToken);
    }

    public void initOffice365(){
        PublicClientApplication.createSingleAccountPublicClientApplication(profileActivity,
                R.raw.auth_config_single_account, new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                    }
                    @Override
                    public void onError(MsalException exception) {
                        //
                        Log.e("ERROR ", exception.getMessage());
                    }
                });
    }

    private AuthenticationCallback getAuthInteractiveCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                Log.e("Access Token: ", authenticationResult.getAccessToken());
                Auth.getDefault().connectOffice365(getContext(),  authenticationResult.getAccessToken());
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString());
            }
            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.");
            }
        };
    }

    public void refreshList(){
        Auth.getDefault().bindAccount(getContext(), bindSRL);
    }

    @Override
    public void onRefresh() {
        refreshList();
    }
}
