package com.pldt.gabayguro.android.fragment.register;

import android.widget.EditText;
import android.widget.TextView;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.RegisterActivity;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.util.ErrorResponseManger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgotPasswordFragment extends BaseFragment {
    public static final String TAG = ForgotPasswordFragment.class.getName();

    private RegisterActivity activity;

    @BindView(R.id.submitBTN)                   TextView submitBTN;
    @BindView(R.id.emailET)                     EditText emailET;

    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void onViewReady() {
        activity = (RegisterActivity) getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setTitle("Forgot Password");
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @OnClick(R.id.submitBTN)
    void submitBTNClicked(){
        Auth.getDefault().forgot(activity,emailET.getText().toString().trim());
    }



    @Subscribe
    public void onResponse(Auth.ForgotResponse response){
        BaseTransformer baseTransformer = response.getData(BaseTransformer.class);
        if (baseTransformer.status){
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.SUCCESS);
            activity.startLandingActivity("");
        } else {
            ToastMessage.show(activity,baseTransformer.msg, ToastMessage.Status.FAILED);
            if(baseTransformer.hasRequirements()){
                ErrorResponseManger.first(emailET, baseTransformer.errors.email);
            }
        }

    }




}
