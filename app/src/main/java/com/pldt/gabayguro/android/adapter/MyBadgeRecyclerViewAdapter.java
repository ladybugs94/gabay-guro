package com.pldt.gabayguro.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.MyBadgeModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MyBadgeRecyclerViewAdapter extends BaseRecylerViewAdapter<MyBadgeRecyclerViewAdapter.ViewHolder, MyBadgeModel>{

    private ClickListener clickListener;

    public MyBadgeRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_my_badge));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(getItem(position));
        holder.adapterCON.setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().badge.data.name);

        switch (holder.getItem().currentBadge.toLowerCase()){
            case "bronze":
                if (!holder.getItem().badge.data.bronzeThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().badge.data.bronzeThumbnail.fullPath).into(holder.imageIV);
                }
                break;
            case "silver":
                if (!holder.getItem().badge.data.silverThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().badge.data.silverThumbnail.fullPath).into(holder.imageIV);
                }
                break;
            case "gold":
                if (!holder.getItem().badge.data.goldThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().badge.data.goldThumbnail.fullPath).into(holder.imageIV);
                }
                break;
            case "platinum":
                if (!holder.getItem().badge.data.platinumThumbnail.fullPath.isEmpty()){
                    Picasso.with(getContext()).load(holder.getItem().badge.data.platinumThumbnail.fullPath).into(holder.imageIV);
                }
                break;
        }

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.badgeLabelTXT)     TextView nameTXT;
        @BindView(R.id.adapterCON)     View adapterCON;
        @BindView(R.id.imageIV)     ImageView imageIV;

        public ViewHolder(View view) {
            super(view);
        }

        public MyBadgeModel getItem() {
            return (MyBadgeModel) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((MyBadgeModel) v.getTag());
                }
                break;
        }
    }

    public interface ClickListener{
        void onItemClick(MyBadgeModel sampleModel);
    }
} 
