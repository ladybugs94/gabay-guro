package com.pldt.gabayguro.android.fragment.resources;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ResourcesActivity;
import com.pldt.gabayguro.android.adapter.CategoryRecyclerViewAdapter;
import com.pldt.gabayguro.android.adapter.ResourceRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.CategoryModel;
import com.pldt.gabayguro.data.model.api.ResourcesModel;
import com.pldt.gabayguro.server.request.Resources;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;


public class   ResourcesFragment extends BaseFragment implements ResourceRecyclerViewAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener,
        EndlessRecyclerViewScrollListener.Callback, CategoryRecyclerViewAdapter.ClickListener{
    public static final String TAG = ResourcesFragment.class.getName();

    private ResourcesActivity mainActivity;
    private ResourceRecyclerViewAdapter resourceRecyclerViewAdapter;
    private CategoryRecyclerViewAdapter categoryRecyclerViewAdapter;
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private APIRequest apiRequest;
    private APIRequest apiRequest2;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;


    @BindView(R.id.resourcesRV)                 RecyclerView resourcesRV;
    @BindView(R.id.categoryRV)                 RecyclerView categoryRV;
    @BindView(R.id.placeHolderTXT)              TextView placeHolderTXT;
    @BindView(R.id.resourcesSRL)                SwipeRefreshLayout resourcesSRL;

    public static ResourcesFragment newInstance() {
        ResourcesFragment fragment = new ResourcesFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_resources;
    }

    @Override
    public void onViewReady() {
        mainActivity = (ResourcesActivity) getContext();
        setUpTransactions();
        setUpCategory();
        resourcesSRL.setOnRefreshListener(this);
    }


    public void setUpTransactions(){
        resourceRecyclerViewAdapter = new ResourceRecyclerViewAdapter(getContext());
        gridLayoutManager = new GridLayoutManager(getContext(), 2);
        resourcesRV.setLayoutManager(gridLayoutManager);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager, this);
        resourcesRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        resourcesRV.setAdapter(resourceRecyclerViewAdapter);
        resourceRecyclerViewAdapter.setClickListener(this);
    }

    private void setUpCategory(){
        categoryRecyclerViewAdapter = new CategoryRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        categoryRV.setLayoutManager(linearLayoutManager);
        categoryRV.setAdapter(categoryRecyclerViewAdapter);
        categoryRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Resources.ResourcesResponse response) {
        CollectionTransformer<ResourcesModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (response.isNext()) {
                resourceRecyclerViewAdapter.addNewData(collectionTransformer.data);
            } else {
                endlessRecyclerViewScrollListener.reset();
                resourceRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
        }else{
            ToastMessage.show(getContext(),collectionTransformer.msg, ToastMessage.Status.FAILED);
        }

        if (resourceRecyclerViewAdapter.getData().size() == 0) {
            placeHolderTXT.setVisibility(View.VISIBLE);
            resourcesRV.setVisibility(View.GONE);
        }
        else{
            placeHolderTXT.setVisibility(View.GONE);
            resourcesRV.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onResponse(Resources.CategoryResponse response) {
        CollectionTransformer<CategoryModel> collectionTransformer = response.getData(CollectionTransformer.class);
        if (collectionTransformer.status) {
            if (response.isNext()) {
                categoryRecyclerViewAdapter.addNewData(collectionTransformer.data);
            } else {
                categoryRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
        }else{
            ToastMessage.show(getContext(),collectionTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setTitle("Resource Center");
        refresh();
    }


    public void refresh(){
        apiRequest =  Resources.getDefault().allResources(getContext(), resourcesSRL, "");
        apiRequest.setPerPage(10);
        apiRequest.first();
        apiRequest2 =  Resources.getDefault().categories(getContext(), resourcesSRL);
        apiRequest2.setPerPage(20);
        apiRequest2.first();
    }

    @Override
    public void onRefresh() {
        refresh();
    }

    @Override
    public void onItemClick(ResourcesModel resourcesModel) {
        mainActivity.openResourcesDetailsFragment(resourcesModel.productId);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.setPage(page+1);
        apiRequest.nextPage();
    }

    @Override
    public void onCategoryClicked(CategoryModel categoryModel) {
        categoryRecyclerViewAdapter.setSelected(categoryModel.categoryId);
        apiRequest =  Resources.getDefault().allResources(getContext(), resourcesSRL, categoryModel.code);
        apiRequest.setPerPage(10);
        apiRequest.first();
    }
}
