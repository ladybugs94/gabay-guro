package com.pldt.gabayguro.android.activity;

import android.content.res.ColorStateList;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;


import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.adapter.DrawerAdapter;
import com.pldt.gabayguro.android.fragment.DefaultFragment;
import com.pldt.gabayguro.android.fragment.main.CommunityFragment;
import com.pldt.gabayguro.android.fragment.main.HomeFragment;
import com.pldt.gabayguro.android.fragment.main.MessagingFragment;
import com.pldt.gabayguro.android.fragment.main.NotificationFragment;
import com.pldt.gabayguro.android.fragment.main.ProfileFragment;
import com.pldt.gabayguro.android.fragment.profile.SettingsFragment;
import com.pldt.gabayguro.android.route.RouteActivity;
import com.pldt.gabayguro.data.model.api.NavDrawerModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.android.java.Keyboard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class MainActivity extends RouteActivity implements DrawerAdapter.ClickListener {
    public static final String TAG = MainActivity.class.getName();


    private DrawerAdapter drawerAdapter;
    ColorStateList defaultColor;

    @BindView(R.id.homeCon)                     View homeCON;
    @BindView(R.id.messageCon)                  View messageCon;
    @BindView(R.id.communityCon)                View communityCon;
    @BindView(R.id.notifCon)                    View notifCon;
    @BindView(R.id.profileCon)                  View profileCon;
    @BindView(R.id.homeIMG)                     ImageView homeIMG;
    @BindView(R.id.messageIMG)                  ImageView messageIMG;
    @BindView(R.id.communityIMG)                ImageView communityIMG;
    @BindView(R.id.notifIMG)                    ImageView notifIMG;
    @BindView(R.id.profileIMG)                  ImageView profileIMG;
    @BindView(R.id.homeTXT)                     TextView homeTXT;
    @BindView(R.id.messageTXT)                  TextView messageTXT;
    @BindView(R.id.communityTXT)                TextView communityTXT;
    @BindView(R.id.notifTXT)                    TextView notifTXT;
    @BindView(R.id.profileTXT)                  TextView profileTXT;
    @BindView(R.id.drawer_layout)               DrawerLayout drawer_layout;
    @BindView(R.id.profileNavIMG)               ImageView profileNavIMG;
    @BindView(R.id.profileBTN)                  ImageView profileBTN;
    @BindView(R.id.nameTXT)                     TextView nameTXT;
    @BindView(R.id.descTXT)                     TextView descTXT;
    @BindView(R.id.drawerLV)                    ListView drawerLV;
    @BindView(R.id.titleCON)                    View titleCON;
    @BindView(R.id.otherTitleCON)               View otherTitleCON;

    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {
        SetDrawer();
        defaultColor = homeTXT.getTextColors();
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "home":
                openHomeFragment();
                break;
                case "community":
                openCommunityFragment();
                break;
            case "settings":
                openSettingsFragment();
                break;
                default:
                    openDefaultFragment();
                break;
        }
    }

    public void openDefaultFragment(){
        switchFragment(DefaultFragment.newInstance());
    }
    public void openHomeFragment(){ switchFragment(HomeFragment.newInstance()); }
    public void openMessageFragment(){ switchFragment(MessagingFragment.newInstance()); }
    public void openCommunityFragment(){ switchFragment(CommunityFragment.newInstance()); }
    public void openNotifFragment(){ switchFragment(NotificationFragment.newInstance()); }
    public void openProfileFragment(){ switchFragment(ProfileFragment.newInstance()); }
    public void openSettingsFragment(){ switchFragment(SettingsFragment.newInstance()); }


    public void setSelectedItem(String item) {
        drawerAdapter.setSelectedItem(item);
    }

    private void SetDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();

        drawerAdapter = new DrawerAdapter(getContext());
        drawerAdapter.setClickListener(this);
        drawerAdapter.setNewData(NavDrawer());

        drawerLV.setAdapter(drawerAdapter);
    }

    private List<NavDrawerModel> NavDrawer() {
        List<NavDrawerModel> navDrawerListModels = new ArrayList<>();

        NavDrawerModel navDrawerModel = new NavDrawerModel();
        navDrawerModel.id = 1;
        navDrawerModel.item = "Home";
        navDrawerListModels.add(navDrawerModel);

        navDrawerModel = new NavDrawerModel();
        navDrawerModel.id = 2;
        navDrawerModel.item = "Settings";
        navDrawerListModels.add(navDrawerModel);

        return navDrawerListModels;
    }

    public void drawer() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            drawer_layout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @OnClick(R.id.menuBTN)
    void menuBTNOnClicked(){
        drawer();
    }

    @OnClick(R.id.settingBTN)
    void settingBTNOnClicked(){
        startProfileActivity("settings", "");
    }

    @OnClick(R.id.qrBTN)
    void qrBTNOnClicked(){
        startComingSoonActivity("three");
    }


    @OnClick(R.id.profileNavIMG)
    void profileNavIMGOnClicked(){
        startProfileActivity("update_profile", "");
    }

    @OnClick(R.id.profileBTN)
    void profileBTNClicked(){
        startProfileActivity("update_profile", "");
    }

    @OnClick(R.id.homeCon)
    public void homeActives(){
        homeActive();
        openHomeFragment();
    }

    @OnClick(R.id.messageCon)
    public void messageActives(){
        messageActive();
        openMessageFragment();
    }
    @OnClick(R.id.communityCon)
    public void comActives(){
        if (UserData.getUserModel().contactNumberStatus.equalsIgnoreCase("verified")){
            communityActive();
            openCommunityFragment();
        }else{
            startProfileActivity("verify", "main");
        }

    }
    @OnClick(R.id.notifCon)
    public void notifActives(){
        notifActive();
        openNotifFragment();
    }
    @OnClick(R.id.profileCon)
    public void profileActives(){
        profileActive();
        openProfileFragment();
    }

    @Override
    public void onItemClick(NavDrawerModel navDrawerModel) {
        drawer_layout.closeDrawer(GravityCompat.START);
        switch (navDrawerModel.id) {
            case 1:
                openHomeFragment();
                Keyboard.hideKeyboard(this);
                break;
            case 2:
                openSettingsFragment();
                Keyboard.hideKeyboard(this);
                break;
        }
    }

    public void homeActive(){
        homeCON.setSelected(true);
        homeIMG.setImageResource(R.drawable.nav_home_alt);
        homeTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        messageCon.setSelected(false);
        messageIMG.setImageResource(R.drawable.nav_messaging);
        messageTXT.setTextColor(defaultColor);

        communityCon.setSelected(false);
        communityIMG.setImageResource(R.drawable.nav_community);
        communityTXT.setTextColor(defaultColor);

        notifCon.setSelected(false);
        notifIMG.setImageResource(R.drawable.nav_notification);
        notifTXT.setTextColor(defaultColor);

        profileCon.setSelected(false);
        profileIMG.setImageResource(R.drawable.nav_user);
        profileTXT.setTextColor(defaultColor);
        titleCON.setVisibility(View.GONE);
        otherTitleCON.setVisibility(View.VISIBLE);
    }

    public void messageActive(){
        messageCon.setSelected(true);
        messageIMG.setImageResource(R.drawable.nav_messaging_alt);
        messageTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        homeCON.setSelected(false);
        homeIMG.setImageResource(R.drawable.nav_home);
        homeTXT.setTextColor(defaultColor);

        communityCon.setSelected(false);
        communityIMG.setImageResource(R.drawable.nav_community);
        communityTXT.setTextColor(defaultColor);

        notifCon.setSelected(false);
        notifIMG.setImageResource(R.drawable.nav_notification);
        notifTXT.setTextColor(defaultColor);

        profileCon.setSelected(false);
        profileIMG.setImageResource(R.drawable.nav_user);
        profileTXT.setTextColor(defaultColor);

        titleCON.setVisibility(View.VISIBLE);
        otherTitleCON.setVisibility(View.GONE);
    }

    public void communityActive(){
        communityCon.setSelected(true);
        communityIMG.setImageResource(R.drawable.nav_community_alt);
        communityTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        homeCON.setSelected(false);
        homeIMG.setImageResource(R.drawable.nav_home);
        homeTXT.setTextColor(defaultColor);

        messageCon.setSelected(false);
        messageIMG.setImageResource(R.drawable.nav_messaging);
        messageTXT.setTextColor(defaultColor);

        notifCon.setSelected(false);
        notifIMG.setImageResource(R.drawable.nav_notification);
        notifTXT.setTextColor(defaultColor);

        profileCon.setSelected(false);
        profileIMG.setImageResource(R.drawable.nav_user);
        profileTXT.setTextColor(defaultColor);

        titleCON.setVisibility(View.VISIBLE);
        otherTitleCON.setVisibility(View.GONE);
    }

    public void notifActive(){
        notifCon.setSelected(true);
        notifIMG.setImageResource(R.drawable.nav_notification_alt);
        notifTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        homeCON.setSelected(false);
        homeIMG.setImageResource(R.drawable.nav_home);
        homeTXT.setTextColor(defaultColor);

        messageCon.setSelected(false);
        messageIMG.setImageResource(R.drawable.nav_messaging);
        messageTXT.setTextColor(defaultColor);

        communityCon.setSelected(false);
        communityIMG.setImageResource(R.drawable.nav_community);
        communityTXT.setTextColor(defaultColor);

        profileCon.setSelected(false);
        profileIMG.setImageResource(R.drawable.nav_user);
        profileTXT.setTextColor(defaultColor);

        titleCON.setVisibility(View.VISIBLE);
        otherTitleCON.setVisibility(View.GONE);
    }

    public void profileActive(){
        profileCon.setSelected(true);
        profileIMG.setImageResource(R.drawable.nav_user_alt);
        profileTXT.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        homeCON.setSelected(false);
        homeIMG.setImageResource(R.drawable.nav_home);
        homeTXT.setTextColor(defaultColor);

        messageCon.setSelected(false);
        messageIMG.setImageResource(R.drawable.nav_messaging);
        messageTXT.setTextColor(defaultColor);

        communityCon.setSelected(false);
        communityIMG.setImageResource(R.drawable.nav_community);
        communityTXT.setTextColor(defaultColor);

        notifCon.setSelected(false);
        notifIMG.setImageResource(R.drawable.nav_notification);
        notifTXT.setTextColor(defaultColor);

        titleCON.setVisibility(View.VISIBLE);
        otherTitleCON.setVisibility(View.GONE);
    }

}
