package com.pldt.gabayguro.android.activity;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.fragment.DefaultFragment;
import com.pldt.gabayguro.android.fragment.coming_soon.ComingSoon1Fragment;
import com.pldt.gabayguro.android.fragment.coming_soon.ComingSoon2Fragment;
import com.pldt.gabayguro.android.fragment.coming_soon.ComingSoon3Fragment;
import com.pldt.gabayguro.android.route.RouteActivity;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class ComingSoonActivity extends RouteActivity {
    public static final String TAG = ComingSoonActivity.class.getName();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "one":
                openComingSoon1Fragment();
                break;
            case "two":
                openComingSoon2Fragment();
                break;
            case "three":
                openComingSoon3Fragment();
                break;
            default:
                openComingSoon1Fragment();
                break;
        }
    }

    public void openComingSoon1Fragment(){
        switchFragment(ComingSoon1Fragment.newInstance());
    }
    public void openComingSoon2Fragment(){ switchFragment(ComingSoon2Fragment.newInstance()); }
    public void openComingSoon3Fragment(){ switchFragment(ComingSoon3Fragment.newInstance()); }

}
