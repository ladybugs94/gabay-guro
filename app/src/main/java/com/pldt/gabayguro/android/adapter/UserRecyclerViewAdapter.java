package com.pldt.gabayguro.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;


public class UserRecyclerViewAdapter extends BaseRecylerViewAdapter<UserRecyclerViewAdapter.ViewHolder, UserModel> {

    private ClickListener clickListener;

    public UserRecyclerViewAdapter(Context context) {
        super(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_user));
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.nameTXT.setText(holder.getItem().name);

        Glide.with(getContext())
                .load(holder.getItem().avatar.fullPath)
                .thumbnail(0.1f)
                .apply(new RequestOptions().placeholder(R.drawable.nav_user).error(R.drawable.nav_user))
                .into(holder.profileCIV);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)              View adapterCON;
        @BindView(R.id.nameTXT)                 TextView nameTXT;
        @BindView(R.id.profileCIV)              ImageView profileCIV;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(UserRecyclerViewAdapter.this);

        }

        public UserModel getItem() {
            return (UserModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((UserModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(UserModel sampleModel);
    }
}
