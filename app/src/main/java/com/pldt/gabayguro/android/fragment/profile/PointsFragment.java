package com.pldt.gabayguro.android.fragment.profile;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.ProfileActivity;
import com.pldt.gabayguro.android.adapter.TransactionRecyclerViewAdapter;
import com.pldt.gabayguro.data.model.api.TransactionModel;
import com.pldt.gabayguro.data.model.api.TransactionModel;
import com.pldt.gabayguro.server.request.Notification;
import com.pldt.gabayguro.server.request.Wallet;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.request.APIRequest;
import com.pldt.gabayguro.vendor.server.transformer.BaseTransformer;
import com.pldt.gabayguro.vendor.server.transformer.CollectionTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class PointsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener
        , TransactionRecyclerViewAdapter.ClickListener
        , EndlessRecyclerViewScrollListener.Callback {
    public static final String TAG = PointsFragment.class.getName();

    public static PointsFragment newInstance() {
        PointsFragment fragment = new PointsFragment();
        return fragment;
    }

    private ProfileActivity mainActivity;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private TransactionRecyclerViewAdapter transactionRecyclerViewAdapter;
    private APIRequest apiRequest;

    @BindView(R.id.transactionRV)                   RecyclerView transactionRV;
    @BindView(R.id.pointsSRL)                       SwipeRefreshLayout pointsSRL;
    @BindView(R.id.shimmerFL)                       ShimmerFrameLayout shimmerFL;
    @BindView(R.id.placeholderTXT)                  TextView placeholderTXT;
    @BindView(R.id.pointsTXT)                       TextView pointsTXT;

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_points;
    }

    @Override
    public void onViewReady() {
        mainActivity = (ProfileActivity) getContext();
        mainActivity.setTitle("My Points");
        setUpRView();
        pointsSRL.setOnRefreshListener(this);
    }

    private void setUpRView(){
        transactionRecyclerViewAdapter = new TransactionRecyclerViewAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        transactionRV.setLayoutManager(linearLayoutManager);
        transactionRV.setAdapter(transactionRecyclerViewAdapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        transactionRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        transactionRecyclerViewAdapter.setClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Wallet.TransactionResponse colletionResponse) {
        CollectionTransformer<TransactionModel> collectionTransformer = colletionResponse.getData(CollectionTransformer.class);
        if (collectionTransformer.status){
            shimmerFL.setVisibility(View.GONE);
            if (colletionResponse.isNext()){
                transactionRecyclerViewAdapter.addNewData(collectionTransformer.data);
            }else {
                endlessRecyclerViewScrollListener.reset();
                transactionRecyclerViewAdapter.setNewData(collectionTransformer.data);
            }
            if (transactionRecyclerViewAdapter.getItemCount() == 0){
                placeholderTXT.setVisibility(View.VISIBLE);
            } else {
                placeholderTXT.setVisibility(View.GONE);
            }
        }

    }

    @Subscribe
    public void onResponse(Wallet.WalletResponse response){
        BaseTransformer singleTransformer = response.getData(BaseTransformer.class);
        if (singleTransformer.status){
            pointsTXT.setText(singleTransformer.walletDisplay);
        }else{
            ToastMessage.show(getContext(), singleTransformer.msg, ToastMessage.Status.FAILED);
        }
    }

    public void refreshList(){
        apiRequest = Wallet.getDefault().transactionHistory(getContext(), pointsSRL);
        apiRequest.first();

        Wallet.getDefault().walletPoints(getContext());

    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(TransactionModel sampleModel) {

    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        apiRequest.setPage(page+1);
        apiRequest.nextPage();
    }
}
