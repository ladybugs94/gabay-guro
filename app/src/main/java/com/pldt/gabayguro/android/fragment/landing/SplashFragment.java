package com.pldt.gabayguro.android.fragment.landing;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;

import com.pldt.gabayguro.R;
import com.pldt.gabayguro.android.activity.LandingActivity;
import com.pldt.gabayguro.data.model.api.UserModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.server.request.Auth;
import com.pldt.gabayguro.vendor.android.base.BaseFragment;
import com.pldt.gabayguro.vendor.android.java.ToastMessage;
import com.pldt.gabayguro.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Labyalo on 8/12/2017.
 */

public class SplashFragment extends BaseFragment {
    public static final String TAG = SplashFragment.class.getName();

    private LandingActivity landingActivity;
    private Runnable runnable;
    private Handler handler;

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        landingActivity  = (LandingActivity) getContext();
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;}

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNetworkAvailable()){
            checkLogin();
        }else{
            openLoginFragment();
        }

    }

        public void openLoginFragment() {
        landingActivity = (LandingActivity) getActivity();
        runnable = new Runnable() {
            @Override
            public void run() {
                landingActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        landingActivity.startMainActivity("home");

                        if (UserData.isLogin()){
                            landingActivity.startMainActivity("home");
                        }else {
                            landingActivity.startMainActivity("home");
                        }
                    }
                });
            }
        };

        handler = new Handler();
            handler.postDelayed(runnable, 3000);

    }


    private void checkLogin() {
        Auth.getDefault().checkLogin(getContext());
    }

    private void refreshToken() {
        Auth.getDefault().refreshToken(getContext());
    }

    @Subscribe
    public void onResponse(Auth.CheckResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
            UserData.insert(userTransformer.data);
            landingActivity.startMainActivity("home");
            ToastMessage.show(getContext(),userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            refreshToken();
        }
    }

    @Subscribe
    public void onResponse(Auth.RefreshTokenResponse response){
        SingleTransformer<UserModel> userTransformer = response.getData(SingleTransformer.class);
        if(userTransformer.status){
            UserData.insert(userTransformer.data);
            UserData.insert(UserData.AUTHORIZATION, userTransformer.token);
            landingActivity.startMainActivity("home");
            ToastMessage.show(getContext(),userTransformer.msg, ToastMessage.Status.SUCCESS);
        }else {
            landingActivity.openLoginFragment();
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) landingActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
