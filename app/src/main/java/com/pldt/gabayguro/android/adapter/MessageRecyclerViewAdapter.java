package com.pldt.gabayguro.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pldt.gabayguro.R;
import com.pldt.gabayguro.data.model.api.ChatModel;
import com.pldt.gabayguro.data.preference.UserData;
import com.pldt.gabayguro.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;


public class MessageRecyclerViewAdapter extends BaseRecylerViewAdapter<MessageRecyclerViewAdapter.ViewHolder, ChatModel> {

    private ClickListener clickListener;

    public MessageRecyclerViewAdapter(Context context) {
        super(context);
    }


    public String getSelectedItems(){
        String total = "";
        for (ChatModel chatModel : getData()){
            total = total + chatModel.id + ",";
        }
        return total;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_message));
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.nameTXT.setText(holder.getItem().title);
        holder.timeTXT.setText(holder.getItem().dateCreated.timePassed);
//        holder.messageTXT.setText(holder.getItem().lastMessage.data.content);
//        holder.onlineBTN.setBackground(holder.getItem().is_online ? ActivityCompat.getDrawable(getContext(), R.drawable.bg_circle_online) : ActivityCompat.getDrawable(getContext(), R.drawable.bg_circle_offline));
        if(UserData.getUserModel().id == holder.getItem().lastMessage.data.sender.data.userId){
            holder.messageTXT.setText("You: " + holder.getItem().lastMessage.data.content);
        }else{
                holder.messageTXT.setText(holder.getItem().lastMessage.data.content);
        }

//        if(holder.getItem().lastMessage.data.isRead){
//            holder.nameTXT.setTextAppearance(getContext(), R.style.TextAppearance_Medium);
//            holder.messageTXT.setTextAppearance(getContext(), R.style.TextAppearance_Medium);
//
//        }else{
//            if(UserData.getUserModel().id == holder.getItem().lastMessage.data.senderUserId){
//                //if youre the sender
//                holder.nameTXT.setTextAppearance(getContext(), R.style.TextAppearance_Medium);
//                holder.messageTXT.setTextAppearance(getContext(), R.style.TextAppearance_Medium);
//
//
//            }else{
//                holder.nameTXT.setTextAppearance(getContext(), R.style.TextAppearance_Medium);
//                holder.messageTXT.setTextAppearance(getContext(), R.style.TextAppearance_Medium);
//            }
//
//        }

        Glide.with(getContext())
                .load(holder.getItem().thumbnail.fullPath)
                .thumbnail(0.1f)
                .apply(new RequestOptions().placeholder(R.drawable.nav_user).error(R.drawable.nav_user))
                .into(holder.profileCIV);

    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)              View adapterCON;
        @BindView(R.id.nameTXT)                 TextView nameTXT;
        @BindView(R.id.messageTXT)              TextView messageTXT;
        @BindView(R.id.timeTXT)                 TextView timeTXT;
        @BindView(R.id.profileCIV)              ImageView profileCIV;
        @BindView(R.id.onlineBTN)               View onlineBTN;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(MessageRecyclerViewAdapter.this);

        }

        public ChatModel getItem() {
            return (ChatModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((ChatModel) v.getTag());
                }
                break;
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(ChatModel sampleModel);
    }
}
