package com.pldt.gabayguro.config;

/**
 * Created by Labyalo on 8/7/2017.
 */

public class Keys {
    public static final String DEVICE_ID = "device_id";
    public static final String DEVICE_PLATFORM = "device_platform";
    public static final String DEVICE_REG_ID = "device_reg_id";
    public static final String DEVICE_BRAND = "device_brand";
    public static final String DEVICE_BRAND_MODEL = "device_brand_model";
    public static final String DEVICE_PLATFORM_VERSION  = "device_platform_version";

    public static final String PAGE = "page";
    public static final String PER_PAGE = "per_page";
    public static final String USER_ID = "user_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String PASSWORD_CONFIRM = "password_confirmation";
    public static final String CURRENT_PASSWORD = "current_password";
    public static final String FILE = "file";
    public static final String EMAIL = "email";
    public static final String FNAME = "fname";
    public static final String LNAME = "lname";
    public static final String MNAME = "mname";
    public static final String SUFFIX = "suffix";
    public static final String BIO = "bio";
    public static final String INCLUDE = "include";
    public static final String BIRTHDATE = "birthdate";
    public static final String OTP_CODE = "otp_code";
    public static final String PHONE_NUMBER = "contact_number";
    public static final String KEYWORD = "keyword";
    public static final String COURSE_ID = "course_id";
    public static final String COMMENT = "comment";

    public static final String CONVERSATION_ID = "conversation_id";
    public static final String CONTENT = "content";
    public static final String MSG_ID = "message_id";
    public static final String FB_ID = "fb_id";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String GOOGLE_ID = "google_id";
    public static final String ANNOUNCEMENT_ID = "announcement_id";
    public static final String CATEGORY_CODE = "category_code";
    public static final String PRODUCT_ID = "product_id";
    public static final String TRANSACTION_ID = "transaction_id";

}
