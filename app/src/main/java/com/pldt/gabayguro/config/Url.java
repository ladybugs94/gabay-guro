package com.pldt.gabayguro.config;

import com.pldt.gabayguro.vendor.android.java.security.SecurityLayer;

/**
 * Created by Labyalo on 8/3/2017.
 */

public class Url extends SecurityLayer {
    public static final String PRODUCTION_URL = decrypt("https://gabaygurowebappprod.azurewebsites.net");
    public static final String DEBUG_URL = decrypt("https://gabaygurowebappprod.azurewebsites.net");

//    https://gabayguro.highlysucceed.com
//    https://gabaygurowebappprod.azurewebsites.net

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;

    public static final String getLogin() {return "/api/auth/login.json";}
    public static final String getLogout() {return "/api/auth/logout.json";}
    public static final String getForgotPass() {return "/api/auth/forgot-password.json";}
    public static final String getRegister() {return "/api/auth/register.json";}
    public static final String getCheckLogin(){ return "/api/auth/check-login.json"; }
    public static final String getRefreshToken(){return "/api/auth/refresh-token.json";}
    public static final String getUpdatePassword(){return "/api/profile/edit-password.json";}
    public static final String getUpdateProfile(){return "/api/profile/edit-profile.json";}
    public static final String getUpdateAvatar(){return "/api/profile/edit-avatar.json";}
    public static final String getMyProfile(){return "/api/profile/show.json";}
    public static final String getFBLogin(){return "/api/auth/facebook.json";}
    public static final String getOffice365Login(){return "/api/auth/office365.json";}
    public static final String getGoogleLogin(){return "/api/auth/google.json";}

    //connect
    public static final String getConnectGoogle(){return "/api/profile/connect/google.json";}
    public static final String getConnectFacebook(){return "/api/profile/connect/facebook.json";}
    public static final String getConnectOffice365(){return "/api/profile/connect/office365.json";}
    public static final String getBindStatus(){return "/api/profile/connect/status.json";}

    //otp
    public static final String getRequestOTP(){return "/api/profile/request-otp.json";}
    public static final String getEditPhone(){return "/api/profile/edit-phone.json";}

    //courses
    public static final String getAllCourses(){return "/api/community/course/all.json";}
    public static final String getCourseDetails(){return "/api/community/course/show.json";}

    //engagement
    public static final String getCreateComment(){return "/api/community/course/engagement/create.json";}
    public static final String getAllComment(){return "/api/community/course/engagement/all.json";}

    //Chat
    public static final String geyMyChats(){return "/api/messaging/all.json";}
    public static final String getCreateChat(){return "/api/messaging/create.json";}
    public static final String getCreateMessage(){return "/api/messaging/send.json";}
    public static final String getChatThread(){return "/api/messaging/conversation.json";}
    public static final String getRemove(){return "/api/messaging/delete.json";}
    public static final String getRemoveAll(){return "/api/messaging/trash.json";}
    public static final String getSearchUser(){return "/api/messaging/search-user.json";}

    //notif
    public static final String getNotification(){return "/api/profile/notification.json";}

    //announcements
    public static final String getAnnouncements(){return "/api/general/announcement/show.json";}
    public static final String getAllAnnouncements(){return "/api/general/announcement/all.json";}

    //resources
    public static final String getCategory(){return "/api/ecommerce/category.json";}
    public static final String getAllResources(){return "/api/ecommerce/product.json";}
    public static final String getResources(){return "/api/ecommerce/show.json";}
    public static final String getRedeem(){return "/api/ecommerce/redeem.json";}
    public static final String getAllRedeem(){return "/api/profile/transaction/all.json";}
    public static final String getRedeemDetails(){return "/api/profile/transaction/show.json";}

    //wallet
    public static final String getTransactionHistory(){return "/api/profile/wallet/history.json";}
    public static final String getWalletPoints(){return "/api/profile/wallet/info.json";}

    //badge
    public static final String getMyBadge(){return "/api/profile/badge/owned.json";}
    public static final String getAllBadge(){return "/api/profile/badge/all.json";}
}
